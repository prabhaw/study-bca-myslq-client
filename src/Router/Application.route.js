import React from "react";
import { Switch } from "react-router-dom";
import PublicRoute from "./PublicRoute.route";
import PrivateRoute from "./PrivateRoute.route";
import HomePage from "../components/Pages/HomePage/HomePage.component";
import PageNotFound from "../components/common/Pagenotfound/Pagenotfound.component";
import Dashboard from "../components/Pages/Dashboard/Dashboard.component";
import Profile from "../components/Pages/Profile/Profile.component";
import ChangePassword from "../components/Pages/ChangePassword/ChangePassword.component";
import ForgetPassword from "../components/common/ForgetPassword/ForgetPassword.component";
import ResetPassword from "../components/common/ForgetPassword/ResetPassword.component";
import ChangePassEmail from "../components/Pages/ChangePasswordEmailSend/ChangePassEmail.component";
import AdminSignUp from "../components/Pages/AdminSignup/AdminSignup.component";
import ProtectedRoute from "./PrivateRoute.route";
import DashPageNotFound from "../components/common/DashPageNotFound/DashPageNotFound.component";
import Dashuser from "../components/Pages/DashUser/Dashuser.component";
import Publicuser from "./../components/Pages/publicuser/Publicuser.component";
import DashBlog from "../components/Dashboard/Blog/DashBlog.component";
import Articles from "../components/Pages/Articles/Articles.component";
import ArticlesSingle from "../components/Pages/ArticlesSingle/ArticlesSingle.component";
import AboutBca from "../components/Dashboard/AboutBca/AboutBca.component";
import AdminAboutBca from "./../components/Dashboard/AdminAboutBca/AdminAboutBca.component";
import AboutBcaPublic from "../components/Pages/AboutBca/AboutBcaPublic.component";
import AdminCollegeComponent from "../components/Dashboard/AdminCollege/AdminCollege.component";
import College from "../components/Pages/College/College.component";
import SingleCollege from "../components/Pages/College/SingleCollege.component";
import AdminSyllabus from "../components/Dashboard/AdminSyllabus/AdminSyllabus.component";
import Syllabus from "../components/Pages/Syllabus/Syllabus.component";
import AdminBookComponent from "../components/Dashboard/AdminBook/AdminBook.component";
import Books from "../components/Pages/Books/Books.component";
import AdminNotes from "../components/Dashboard/AdminNotes/AdminNotes.component";
import Notes from "../components/Pages/Notes/Notes.component";
import SingleNote from "./../components/Pages/Notes/SingleNote.component";
import AdminOldQuestion from "../components/Dashboard/AdminOldQuestion/AdminOldQuestion.component";
import OldQuestionComponent from "../components/Pages/OldQuestion/OldQuestion.component";
import OldQuestionSingle from "../components/Pages/OldQuestion/OldQuestionSingle.component";
import AdminSliderComponent from "../components/Dashboard/AdminSlider/AdminSlider.component";
import Policy from "../components/Pages/policy/Policy.component";
const ApplicationRoute = () => {
  return (
    <Switch>
      <PublicRoute exact path='/' component={HomePage} />
      <PublicRoute path='/forgot-password' component={ForgetPassword} />
      <PublicRoute path='/reset-password/:token' component={ResetPassword} />
      <PublicRoute path='/reset-password-end' component={ChangePassEmail} />
      <PublicRoute path='/admin' component={AdminSignUp} />
      <PublicRoute path='/professor' component={Publicuser} />
      <PublicRoute path='/articles' exact component={Articles} />
      <PublicRoute path='/aboutbca' exact component={AboutBcaPublic} />
      <PublicRoute path='/college' exact component={College} />
      <PublicRoute path='/syllabus' exact component={Syllabus} />
      <PublicRoute path='/books' exact component={Books} />
      <PublicRoute path='/notes' exact component={Notes} />
      <PublicRoute path='/questions' exact component={OldQuestionComponent} />
      <PublicRoute path='/privacy' exact component={Policy} />

      <PublicRoute path='/notes/:id' component={SingleNote} />
      <PublicRoute path='/questions/:id' component={OldQuestionSingle} />

      <PublicRoute path='/college/:id' exact component={SingleCollege} />

      <PublicRoute path='/articles/:id' component={ArticlesSingle} />

      {/* --------------------------protected ROute---------------------------------- */}

      <PrivateRoute exact path='/user/dashboard' component={Dashboard} />
      <PrivateRoute exact path='/user/profile' component={Profile} />
      <PrivateRoute exact path='/user' component={Dashuser} />
      <PrivateRoute exact path='/user/articles' component={DashBlog} />
      <PrivateRoute exact path='/user/aboutbca' component={AboutBca} />
      <PrivateRoute exact path='/user/syllabus' component={AdminSyllabus} />
      <PrivateRoute exact path='/user/book' component={AdminBookComponent} />
      <PrivateRoute exact path='/user/notes' component={AdminNotes} />
      <PrivateRoute
        exact
        path='/user/slider'
        component={AdminSliderComponent}
      />

      <PrivateRoute
        exact
        path='/user/oldquestion'
        component={AdminOldQuestion}
      />

      <PrivateRoute
        exact
        path='/user/admin-aboutbca'
        component={AdminAboutBca}
      />

      <PrivateRoute
        exact
        path='/user/setting/change-password'
        component={ChangePassword}
      />
      <PrivateRoute
        exact
        path='/user/college'
        component={AdminCollegeComponent}
      />

      {/* ----------------------------------------------------------------- */}

      <ProtectedRoute path='/user/*' component={DashPageNotFound} />
      <PublicRoute component={PageNotFound} />
    </Switch>
  );
};

export default ApplicationRoute;
