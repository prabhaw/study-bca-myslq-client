import React from "react";
import { Route } from "react-router-dom";
import Navbar from "../components/common/Navbar/Navbar.component";
import Footer from "../components/common/Footer/Footer.component";

const PublicRoute = ({ component: Component, ...props }) => {
  return (
    <Route
      {...props}
      render={(props) => (
        <>
          <Navbar />
          <div style={{ minHeight: "72vh" }}>
            <Component {...props} />
          </div>

          <Footer />
        </>
      )}
    />
  );
};

export default PublicRoute;
