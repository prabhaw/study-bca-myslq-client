import React from "react";
import { Route, Redirect } from "react-router-dom";
import NavDashboard from "../components/common/NavDashboard/NavDashboard.component";
import { Layout } from "antd";
import Sidebar from "../components/common/DashboardSidebar/DashboardSidebar.component";
import Footer from "../components/common/Footer/Footer.component";
const { Content } = Layout;
const ProtectedRoute = ({ component: Component, ...props }) => {
  return (
    <Route
      {...props}
      render={(props) =>
        localStorage.getItem("token") ? (
          <>
            <Layout>
              <Sidebar />
              <Layout>
                <NavDashboard />
                <Layout>
                  <Content style={{ background: "#F6F6F6", minHeight: "80vh" }}>
                    <Component {...props} />
                  </Content>
                  <Footer />
                </Layout>
              </Layout>
            </Layout>
          </>
        ) : (
          <Redirect to='/' />
        )
      }
    />
  );
};

export default ProtectedRoute;
