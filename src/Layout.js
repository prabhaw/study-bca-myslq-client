import React from "react";
import "./App.less";
import store from "./Redux/store";
import ApplicationRoute from "./Router/Application.route";
import { Provider } from "react-redux";
import ReactNotifications from "react-notifications-component";
import { BackTop } from "antd";

function App() {
  return (
    <Provider store={store}>
      <ReactNotifications />

      <ApplicationRoute />

      <BackTop />
    </Provider>
  );
}

export default App;
