import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import Layouts from "./Layout";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter as Router } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-notifications-component/dist/theme.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const routed_app = (
  <Router>
    <Layouts />
  </Router>
);
ReactDOM.render(routed_app, document.getElementById("root"));

serviceWorker.unregister();
