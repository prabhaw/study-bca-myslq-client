import React, { useEffect, useState } from "react";
import "./DashBlog.component.less";
import { Divider, Avatar, Pagination, Modal, Skeleton, Input } from "antd";
import { connect } from "react-redux";
import { UserOutlined } from "@ant-design/icons";
import BlogForm from "./BlogForm.component";
import BlogCard from "../../common/BlogCard/Blogcard.component";
import {
  fetch_dash_blog,
  add_blog_modal,
  add_blog,
  search_blog_admin,
} from "./../../../Redux/actions/blogs.action";
const IMG_URL = process.env.REACT_APP_USER_IMG_URL;

const { Search } = Input;
const DashBlog = (props) => {
  const {
    user,
    fetch_dash_blog,
    blogs,
    loading,
    total,
    pageNum,
    modal,
    add_blog_modal,
    add_blog,
    search_blog_admin,
  } = props;
  const [searchs, setSearch] = useState();
  useEffect(() => {
    fetch_dash_blog(1, 12);
  }, [fetch_dash_blog]);
  useEffect(() => {
    document.title = "Articles";
  }, []);
  const single_blog = blogs.map((item) => (
    <div key={item.id} className='col-12 col-md-10'>
      <BlogCard blog={item} />
    </div>
  ));
  const addBlog = (data) => {
    add_blog(data);
  };
  const changePage = (page, pageSize) => {
    if (searchs) {
      search_blog_admin(searchs, page, pageSize);
    } else {
      fetch_dash_blog(page, pageSize);
    }
  };

  const search = (value) => {
    setSearch(value);
    search_blog_admin(value, 1, 12);
  };
  return (
    <>
      <div className='dash-blog'>
        <div className='justify-content-center row search-section'>
          <div className='col-12 col-md-8'>
            <div className='row  '>
              <div className='col-12 '>
                <div id='c-c-main'>
                  <div className='tb'>
                    <div className='td' id='p-c-i'>
                      {user && user.profile_image ? (
                        <Avatar
                          size={51}
                          src={`${IMG_URL}/${user.profile_image}`}
                        />
                      ) : (
                        <Avatar size={51} icon={<UserOutlined />} />
                      )}
                    </div>
                    <div
                      className='td '
                      style={{ width: "100%", height: "100%" }}
                      id='c-inp'
                    >
                      <div className='form-label-group'>
                        <input
                          type='text'
                          placeholder=' Write About Something....'
                          className='form-control shadow-none'
                          readOnly={true}
                          onClick={() => {
                            add_blog_modal();
                          }}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {user.role === 1 ? (
              <div className='row '>
                <div className='col-12 '>
                  <Search
                    placeholder='input search text'
                    onSearch={search}
                    style={{ width: "100%", marginTop: "10px" }}
                    size='large'
                  />
                </div>
              </div>
            ) : null}
          </div>
        </div>
        <Divider />
        <div className='d-flex row justify-content-center'>
          {loading ? (
            <div className='col-12 col-md-10'>
              <Skeleton active paragraph={{ rows: 10 }} />
            </div>
          ) : (
            <>{single_blog}</>
          )}
        </div>
        <div className='justify-content-center d-flex'>
          <Pagination
            total={total}
            defaultCurrent={1}
            current={pageNum}
            onChange={changePage}
            hideOnSinglePage={true}
            pageSize={12}
            showSizeChanger={false}
          />
        </div>
      </div>

      <Modal
        visible={modal}
        centered={true}
        closable={true}
        footer={null}
        onCancel={() => {
          add_blog_modal();
        }}
        //onCancel={this.handleCancel}
        zIndex={1050}
        width={900}
      >
        <BlogForm
          btnWord='Add POST'
          action={addBlog}
          resetform={props.resetform}
          btnload={props.btn_load}
        />
      </Modal>
    </>
  );
};
const mapStateToProps = (state) => ({
  user: state.auth.user,
  blogs: state.blogs.blogs,
  loading: state.blogs.loading,
  total: state.blogs.blog_total,
  pageNum: state.blogs.pageNum,
  modal: state.blogs.blog_modal,
  resetform: state.blogs.formreset,
  btn_load: state.blogs.add_blog_btn,
});
const mapDispatchToProps = {
  fetch_dash_blog,
  add_blog_modal,
  add_blog,
  search_blog_admin,
};
export default connect(mapStateToProps, mapDispatchToProps)(DashBlog);
