import React, { useState, useEffect } from "react";
import { Form, Input, Button } from "antd";
import JoditEditor from "jodit-react";
import notifications from "../../../utils/notifications";
const { TextArea } = Input;
const BlogForm = (props) => {
  const [form] = Form.useForm();
  const [image, setImage] = useState("");
  const [description, setDescription] = useState("");
  const { update, blog } = props;

  useEffect(() => {
    if (update) {
      form.setFieldsValue({
        title: blog.title,
        short_description: blog.short_description,
      });
    }
  }, [form, update, blog]);

  useEffect(() => {
    if (props.resetform) {
      form.resetFields();
      setImage("");
      setDescription("");
    }
  }, [props.resetform, form]);

  useEffect(() => {
    setImage("");
  }, [props.resetimage]);
  const handleImage = (e) => {
    let { files, type } = e.target;
    const image = files;

    if (type === "file") {
      if (image[0]) {
        if (
          image[0].type === "image/jpeg" ||
          image[0].type === "image/png" ||
          image[0].type === "image/jpg"
        ) {
          if (image[0].size / 1024 / 1024 <= 2) {
            setImage(image);
          } else {
            notifications.showWarning("File is more then 2MB.");
          }
        } else {
          notifications.showWarning(
            "File format onley jpge or png is accepted."
          );
        }
      }
    }
  };

  const Image = image ? (
    <img
      src={URL.createObjectURL(image[0])}
      className='img-fluid'
      style={{ maxHeight: "400px", width: "100%" }}
      alt='College'
    />
  ) : null;

  const onFinish = (value) => {
    const formData = new FormData();
    formData.append("description", description);
    formData.append("blog_img", image[0]);
    for (var key in value) {
      formData.append(key, value[key]);
    }
    props.action(formData);
  };
  return (
    <>
      <div className='college-form'>
        <Form onFinish={onFinish} form={form} scrollToFirstError={true}>
          <div className='formm-group'>
            <label htmlFor='title'>
              <span style={{ color: "red" }}>*</span>
              <strong>Article Title</strong>
            </label>
            <Form.Item
              name='title'
              rules={[
                {
                  required: true,
                  message: "Please input your Title.",
                },
              ]}
            >
              <TextArea
                placeholder='Article Title Here....'
                size='large'
                autoSize={{ minRows: 2, maxRows: 2 }}
                maxLength='200'
              />
            </Form.Item>
          </div>
          <div className='formm-group'>
            <label htmlFor='short_description'>
              <span style={{ color: "red" }}>*</span>
              <strong>Short Description</strong>
            </label>
            <Form.Item
              name='short_description'
              rules={[
                {
                  required: true,
                  message: "Please input  Short Description",
                },
              ]}
            >
              <TextArea
                placeholder='Input Short Descriptiom of Article....'
                size='large'
                autoSize={{ minRows: 3, maxRows: 3 }}
                maxLength='500'
              />
            </Form.Item>
          </div>
          <div className='formm-group'>
            <label htmlFor='description'>
              <strong>Description</strong>
            </label>
            <JoditEditor
              value={blog && blog.description ? blog.description : ""}
              tabIndex={1}
              name='description'
              onChange={(value) => {
                setDescription(value);
              }}
            />
          </div>
          <div className='formm-group' style={{ margin: "20px 0px" }}>
            <label htmlFor='college_image'>
              <strong>Image For Article</strong>
            </label>
            {image ? <div style={{ marginBottom: "20px" }}>{Image}</div> : null}
            <div className='input-group'>
              <label className='input-group-btn my-0'>
                <span
                  className='btn btn-large btn-outline-primary rounded-0'
                  id='browse'
                >
                  Browse&hellip;
                  <input id='csv-input' type='file' onChange={handleImage} />
                </span>
              </label>
              <input
                type='text'
                className='form-control rounded-0'
                readOnly
                placeholder='Upload Image For Article'
                value={image ? image[0].name : ""}
              />
            </div>
          </div>

          <Form.Item>
            <div className=' d-flex flex-row-reverse '>
              <div className='col-12 col-md-2' style={{ padding: "0" }}>
                <Button
                  type='primary'
                  htmlType='submit'
                  loading={props.btnload}
                  block
                  size='large'
                >
                  {props.btnWord}
                </Button>
              </div>
            </div>
          </Form.Item>
        </Form>
      </div>
    </>
  );
};

export default BlogForm;
