import React, { useEffect } from "react";
import "./AdminNotes.component.less";
import { Divider, Avatar, Pagination, Modal, Skeleton } from "antd";
import { connect } from "react-redux";
import { UserOutlined } from "@ant-design/icons";
import AdminNoteCard from "./AdminNoteCard.component";
import AddNoteFormComponent from "./AddNoteForm.component";
import {
  note_modal,
  add_note,
  fetch_admin,
} from "./../../../Redux/actions/notes.action";
const IMG_URL = process.env.REACT_APP_USER_IMG_URL;

const AdminNotes = (props) => {
  const {
    user,
    note_modal,
    modal,
    add_note,
    note_btn,
    formreset,
    fetch_admin,
    loading,
    notes,
    total,
    pageNumber,
  } = props;
  useEffect(() => {
    document.title = "BCA-Notes";
    fetch_admin(1, 12);
  }, [fetch_admin]);
  const changePage = (page, pageSize) => {
    fetch_admin(page, pageSize);
  };
  const note = notes.map((item) => (
    <div className='col-12 col-md-11' key={item.id}>
      <AdminNoteCard note={item} />
    </div>
  ));
  return (
    <>
      <div className='dash-blog'>
        <div className='justify-content-center row search-section'>
          <div className='col-12 col-md-6'>
            <div className='row  '>
              <div className='col-12 '>
                <div id='c-c-main'>
                  <div className='tb'>
                    <div className='td' id='p-c-i'>
                      {user && user.profile_image ? (
                        <Avatar
                          size={51}
                          src={`${IMG_URL}/${user.profile_image}`}
                        />
                      ) : (
                        <Avatar size={51} icon={<UserOutlined />} />
                      )}
                    </div>
                    <div
                      className='td '
                      style={{ width: "100%", height: "100%" }}
                      id='c-inp'
                    >
                      <div className='form-label-group mt-3'>
                        <button
                          className='add-note'
                          style={{ width: "100%" }}
                          onClick={() => {
                            note_modal();
                          }}
                        >
                          Add Note
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Divider />
        <div className='row justify-content-center'>
          {loading ? (
            <>
              <div className='col-12 col-md-11'>
                <Skeleton active paragraph={{ rows: 3 }} />
              </div>
              <div className='col-12 col-md-11'>
                <Skeleton active paragraph={{ rows: 3 }} />
              </div>
            </>
          ) : (
            note
          )}
        </div>
        <div className='justify-content-center d-flex mt-5'>
          <Pagination
            total={total}
            defaultCurrent={1}
            current={pageNumber}
            onChange={changePage}
            hideOnSinglePage={true}
            pageSize={12}
            showSizeChanger={false}
          />
        </div>
      </div>

      <Modal
        visible={modal}
        centered={true}
        closable={true}
        footer={null}
        onCancel={() => {
          note_modal();
        }}
        zIndex={1050}
        width={500}
      >
        <AddNoteFormComponent
          action={add_note}
          note_btn={note_btn}
          formreset={formreset}
          btnWord={"Add Note"}
        />
      </Modal>
    </>
  );
};

const mapStateToProps = (state) => ({
  user: state.auth.user,
  modal: state.notes.note_modal,
  note_btn: state.notes.note_btn,
  formreset: state.notes.formreset,

  loading: state.notes.loading,
  notes: state.notes.notes,
  total: state.notes.note_total,
  pageNumber: state.notes.pageNumber,
});
const mapDispatchToProps = { note_modal, add_note, fetch_admin };
export default connect(mapStateToProps, mapDispatchToProps)(AdminNotes);
