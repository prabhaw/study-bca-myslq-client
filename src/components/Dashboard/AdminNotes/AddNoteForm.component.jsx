import React, { useEffect, useState } from "react";
import { Select, Form, Button, Input } from "antd";
import { connect } from "react-redux";
import {
  syllabus_university,
  search_sem,
  search_subj,
} from "./../../../Redux/actions/search.action";
const { Option } = Select;

const AdminBookForm = (props) => {
  const {
    university,
    syllabus_university,
    semester,
    subject,
    search_sem,
    search_subj,
    note,
    update,
    action,
    note_btn,
    formreset,
    btnWord,
  } = props;
  const [form] = Form.useForm();
  const [data, setData] = useState();
  const [fileerr, setFileErr] = useState();
  const [file, setFile] = useState("");

  useEffect(() => {
    syllabus_university();
  }, [syllabus_university]);

  useEffect(() => {
    if (update) {
      form.setFieldsValue({
        university: note.university,
        semester: note.semester,
        subject: note.subject,
        description: note.description,
        // short_description: blog.short_description,
      });
    }
  }, [form, update, note]);

  useEffect(() => {
    if (formreset) {
      form.resetFields();
      setFile("");
      setFileErr("");
    }
  }, [formreset, form]);

  // ...............file-------------

  const handleFile = (e) => {
    let { files, type } = e.target;

    if (type === "file") {
      if (files[0]) {
        if (files[0].type === "application/pdf") {
          setFile(files[0]);
          setFileErr("");
        } else {
          setFileErr("Onley PDF format is accepted.");
          setFile("");
        }
      }
    }
  };

  //   ------------------------------------

  const university_data = university.map((item, i) => (
    <Option value={item} key={i}>
      {item}
    </Option>
  ));

  const semester_data = semester.map((item, i) => (
    <Option value={item} key={i}>
      {item}
    </Option>
  ));
  const subj_data = subject.map((item, i) => (
    <Option value={item} key={i}>
      {item}
    </Option>
  ));
  const onUniversityFormChange = (value) => {
    search_sem({ university: value });
    setData((pre) => ({ ...pre, university: value }));
  };
  const onSemSelect = (value) => {
    search_subj({ ...data, semester: value });
    setData((pre) => ({ ...pre, semester: value }));
  };
  const onFinish = (value) => {
    if (file || update) {
      const formData = new FormData();
      formData.append("note_file", file);
      for (var key in value) {
        formData.append(key, value[key]);
      }
      action(formData);
    } else {
      setFileErr("Please Input Note File.");
    }
  };

  return (
    <div className='pt-5 form-with-file'>
      <Form name='adminNoteForm' form={form} onFinish={onFinish}>
        <Form.Item
          name='university'
          rules={[{ required: true, message: "Select University" }]}
        >
          <Select
            placeholder='Select University'
            size='large'
            onChange={onUniversityFormChange}
          >
            {university_data}
          </Select>
        </Form.Item>

        <Form.Item
          name='semester'
          rules={[{ required: true, message: "Select Semester" }]}
        >
          <Select
            placeholder='Select Semester'
            size='large'
            onChange={onSemSelect}
          >
            {semester_data}
          </Select>
        </Form.Item>

        <Form.Item
          name='subject'
          rules={[{ required: true, message: "Select Subject" }]}
        >
          <Select placeholder='Select Subject' size='large'>
            {subj_data}
          </Select>
        </Form.Item>

        <Form.Item
          name='description'
          rules={[
            {
              required: true,
              message: "Please input note Description.",
            },
          ]}
        >
          <Input.TextArea
            style={{ borderRadius: "0px" }}
            placeholder='Some word about Note'
          />
        </Form.Item>

        <div className='formm-group' style={{ margin: "20px 0px" }}>
          <div
            className='input-group'
            style={fileerr ? { border: "1px solid red" } : {}}
          >
            <label className='input-group-btn my-0'>
              <span
                className='btn btn-large btn-outline-primary rounded-0'
                id='browse'
              >
                Browse&hellip;
                <input id='csv-input' type='file' onChange={handleFile} />
              </span>
            </label>
            <input
              type='text'
              className='form-control rounded-0'
              readOnly
              placeholder='Upload Note'
              value={file ? file.name : ""}
            />
          </div>
          {fileerr ? <p style={{ color: "red" }}>{fileerr}</p> : null}
        </div>
        <Form.Item>
          <Button
            type='primary'
            htmlType='submit'
            size='large'
            loading={note_btn}
            block
          >
            {btnWord}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

const mapStateToProps = (state) => ({
  university: state.search.search,
  semester: state.search.semester,
  subject: state.search.subject,
});
const mapDispatchToProps = { syllabus_university, search_sem, search_subj };
export default connect(mapStateToProps, mapDispatchToProps)(AdminBookForm);
