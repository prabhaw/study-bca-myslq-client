import React, { useState, useEffect } from "react";
import { Divider } from "antd";
import { Modal, Checkbox, Menu, Dropdown } from "antd";
import { connect } from "react-redux";
import { FaBars, FaEdit } from "react-icons/fa";
import { DeleteOutlined } from "@ant-design/icons";
import { MdDelete } from "react-icons/md";
import AddNoteFormComponent from "./AddNoteForm.component";
import {
  update_note,
  delete_note,
} from "./../../../Redux/actions/notes.action";
const NOTE_URL = process.env.REACT_APP_NOTE_FILE_URL;
const { confirm } = Modal;
const AdminNoteCard = (props) => {
  const { user, update_note, formreset, delete_note } = props;
  const [modal, setModal] = useState(false);
  const { note } = props;
  const dowload_note = () => {
    const url = `${NOTE_URL}/${note.note}`;
    window.open(url, "_blank");
  };
  useEffect(() => {
    if (formreset) {
      setModal(false);
    }
  }, [formreset]);
  const showDeleteConfirm = () => {
    confirm({
      title: "Are you sure delete this Post?",
      icon: <DeleteOutlined style={{ color: "red" }} />,
      content: "The post will no longer available to this site.",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      zIndex: "1500",
      onOk() {
        delete_note(note.id);
      },
    });
  };
  const onChange = (e) => {
    const data = { verified: e.target.checked };
    update_note(note.id, data);
  };

  const udatenote = (data) => {
    update_note(note.id, data);
  };
  const menu = (
    <Menu>
      <Menu.Item
        key='0'
        style={{ color: "blue" }}
        onClick={() => {
          setModal(true);
        }}
      >
        <FaEdit style={{ fontSize: "18px" }} />
        Edit Post
      </Menu.Item>
      <Menu.Item key='1' style={{ color: "red" }} onClick={showDeleteConfirm}>
        <MdDelete style={{ fontSize: "18px" }} />
        Delete Post
      </Menu.Item>

      {user.role === 1 ? (
        <Menu.Item key='3'>
          <Checkbox defaultChecked={note.verified} onChange={onChange}>
            Active
          </Checkbox>
        </Menu.Item>
      ) : null}
    </Menu>
  );

  return (
    <>
      <div className='list-group border-0  '>
        <div className='d-flex w-100 justify-content-between'>
          <h4 className='mb-1 note-h-admin' onClick={dowload_note}>
            {note.subject}
          </h4>
          <Dropdown overlay={menu} trigger={["click"]} placement='bottomRight'>
            <FaBars
              key='menu'
              style={{ fontSize: "20px", cursor: "pointer" }}
            />
          </Dropdown>
        </div>
        <p className='mb-1'>
          {note.university} &nbsp; &nbsp;{note.semester}
        </p>
        <small>Publish At:&nbsp;{note.publish_date}</small>
      </div>
      <Divider />
      <Modal
        visible={modal}
        centered={true}
        closable={true}
        footer={null}
        onCancel={() => {
          setModal(false);
        }}
        zIndex={1050}
        width={500}
      >
        <AddNoteFormComponent
          update={true}
          note={note}
          action={udatenote}
          btnWord='Update Note'
        />
        {/* <BlogForm
          btnWord='Update Post'
          action={udateBlog}
          update={true}
          blog={blog}
          btnload={props.add_btn}
          resetimage={props.resetform}
        /> */}
      </Modal>
    </>
  );
};

const mapStateToProps = (state) => ({
  user: state.auth.user,
  formreset: state.notes.formreset,
});
const mapDispatchToProps = { update_note, delete_note };
export default connect(mapStateToProps, mapDispatchToProps)(AdminNoteCard);
