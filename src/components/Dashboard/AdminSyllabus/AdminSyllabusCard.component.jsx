import React from "react";
import { Card, Popconfirm } from "antd";
import { MdDelete } from "react-icons/md";
import { FaEye } from "react-icons/fa";
const FILE = process.env.REACT_APP_SYLLABUS_FILE_URL;
const AdminSyllabusCard = (props) => {
  const { syllubus, deletes } = props;

  const dowload_syllabus = (file) => {
    const url = `${FILE}/${syllubus.syllabus}`;
    window.open(url, "_blank");
  };

  const onDelete = () => {
    deletes(syllubus.id);
  };

  return (
    <>
      <Card
        hoverable
        actions={[
          <FaEye
            key='view'
            style={{ fontSize: "20px" }}
            onClick={dowload_syllabus}
          />,

          <Popconfirm
            title='Are you sure delete this college?'
            onConfirm={onDelete}
            okText='Yes'
            cancelText='No'
          >
            <MdDelete key='remove' style={{ fontSize: "20px" }} />
          </Popconfirm>,
        ]}
      >
        <h6 style={{ fontWeight: "bold" }}>{syllubus.subject}</h6>
        <p className='m-0'>
          <strong>{syllubus.university}</strong>
        </p>
        <p className='m-0'>{syllubus.semester}</p>
        <p className='m-0'>Course Code:&nbsp;{syllubus.course_code} </p>
        <p className='m-0'>Credit Hours: {syllubus.credit_hrs}hr</p>
      </Card>
    </>
  );
};

export default AdminSyllabusCard;
