import React, { useState, useEffect } from "react";
import { Select, Form, Button, Divider, Input } from "antd";
import { connect } from "react-redux";
import { syllabus_university } from "./../../../Redux/actions/search.action";
const { Option } = Select;
let index = 0;
const SyllabusForm = (props) => {
  const { add_btn, resetform, action, syllabus_university, university } = props;
  const [form] = Form.useForm();
  const [data, setData] = useState({ name: "", items: [] });
  const [fileerr, setFileErr] = useState();
  const [file, setFile] = useState("");

  useEffect(() => {
    if (resetform) {
      form.resetFields();
      setFile("");
      setFileErr("");
    }
  }, [resetform, form]);
  useEffect(() => {
    syllabus_university();
  }, [syllabus_university]);

  useEffect(() => {
    setData((pre) => ({ ...pre, items: [...university] }));
  }, [university]);
  const handleFile = (e) => {
    let { files, type } = e.target;

    if (type === "file") {
      if (files[0]) {
        if (files[0].type === "application/pdf") {
          setFile(files[0]);
          setFileErr("");
        } else {
          setFileErr("Onley PDF format is accepted.");
          setFile("");
        }
      }
    }
  };

  const onFinish = (value) => {
    if (file) {
      const formData = new FormData();
      formData.append("syllabus", file);
      for (var key in value) {
        formData.append(key, value[key]);
      }
      action(formData);
    } else {
      setFileErr("Please Input File.");
    }
  };

  const onNameChange = (event) => {
    setData({ ...data, name: event.target.value });
  };

  const addItem = (e) => {
    setData({ value: e.target.value });
    const { items, name } = data;
    setData({
      items: [...items, name || `University ${index++}`],
      name: "",
    });
  };

  return (
    <>
      <div className='pt-5 form-with-file'>
        <Form name='addSyllabus' form={form} onFinish={onFinish}>
          <Form.Item
            name='university'
            rules={[{ required: true, message: "Select University" }]}
          >
            <Select
              placeholder='Select University'
              size='large'
              dropdownRender={(menu) => (
                <div>
                  {menu}
                  <Divider style={{ margin: "4px 0" }} />
                  <div
                    style={{ display: "flex", flexWrap: "nowrap", padding: 8 }}
                  >
                    <Input
                      style={{ flex: "auto" }}
                      value={data.name}
                      onChange={onNameChange}
                    />
                    &nbsp;
                    <Button onClick={addItem} type='primary'>
                      ADD
                    </Button>
                  </div>
                </div>
              )}
            >
              {data.items.map((item) => (
                <Option key={item} value={item}>
                  {item}
                </Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item
            name='semester'
            rules={[{ required: true, message: "Select Semester" }]}
          >
            <Select placeholder='Select Semester' size='large'>
              <Option value='SEMESTER-1'>SEMESTER-1</Option>
              <Option value='SEMESTER-2'>SEMESTER-2</Option>
              <Option value='SEMESTER-3'>SEMESTER-3</Option>
              <Option value='SEMESTER-4'>SEMESTER-4</Option>
              <Option value='SEMESTER-5'>SEMESTER-5</Option>
              <Option value='SEMESTER-6'>SEMESTER-6</Option>
              <Option value='SEMESTER-7'>SEMESTER-7</Option>
              <Option value='SEMESTER-8'>SEMESTER-8</Option>
            </Select>
          </Form.Item>

          <Form.Item
            name='subject'
            rules={[{ required: true, message: "Input Subject" }]}
          >
            <Input placeholder='Input Subject' size='large' />
          </Form.Item>
          <Form.Item
            name='course_code'
            rules={[{ required: true, message: "Input Course Code" }]}
          >
            <Input placeholder='Input Course Code' size='large' />
          </Form.Item>
          <Form.Item
            name='credit_hrs'
            rules={[
              {
                required: true,
                message: "Please input Credit Hrs",
              },
              {
                pattern: "^[0-9\b]+$",

                message: "Credit Hrs must be Number.",
              },
              // { validator: validatePassword },
            ]}
          >
            <Input placeholder='Input Credit Hrs' size='large' />
          </Form.Item>

          <div className='formm-group' style={{ margin: "20px 0px" }}>
            <div
              className='input-group'
              style={fileerr ? { border: "1px solid red" } : {}}
            >
              <label className='input-group-btn my-0'>
                <span
                  className='btn btn-large btn-outline-primary rounded-0'
                  id='browse'
                >
                  Browse&hellip;
                  <input id='csv-input' type='file' onChange={handleFile} />
                </span>
              </label>
              <input
                type='text'
                className='form-control rounded-0'
                readOnly
                placeholder='Upload Syllabus File.'
                value={file ? file.name : ""}
              />
            </div>
            {fileerr ? <label style={{ color: "red" }}>{fileerr}</label> : null}
          </div>

          <Form.Item>
            <Button
              type='primary'
              htmlType='submit'
              size='large'
              block
              loading={add_btn}
            >
              POST
            </Button>
          </Form.Item>
        </Form>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  university: state.search.search,
});
const mapDispatchToProps = { syllabus_university };
export default connect(mapStateToProps, mapDispatchToProps)(SyllabusForm);
