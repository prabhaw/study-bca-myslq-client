import React, { useEffect, useState } from "react";
import "./AdminSyllabus.component.less";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Button, Divider, Modal, Pagination, Skeleton } from "antd";
import SearchForm from "../../common/SearchForm/SearchForm.component";
import SyllabusForm from "./SyllabusForm.component";
import AdminSyllabusCard from "./AdminSyllabusCard.component";
import {
  add_modal,
  add_syllabus,
  fetch_syllabus,
  search_syllabus,
  delete_syllabus,
} from "./../../../Redux/actions/syllabus.action";
const AdmnSyllabus = (props) => {
  const {
    user,
    history,
    add_modal,
    modal,
    add_syllabus,
    add_btn,
    resetfrom,
    loading,
    syllabus,
    fetch_syllabus,
    total,
    pageNumber,
    search_syllabus,
    delete_syllabus,
  } = props;
  const [search, setSearch] = useState();
  useEffect(() => {
    document.title = "Admin Syllabus";
    fetch_syllabus(1, 12);
  }, [fetch_syllabus]);
  const modal_action = () => {
    add_modal(false);
  };
  const data_syllbus = syllabus.map((item) => (
    <div className='col-12 col-md-4 mb-3' key={item.id}>
      <AdminSyllabusCard syllubus={item} deletes={delete_syllabus} />
    </div>
  ));
  const changePage = (page, pageSize) => {
    if (search) {
      search_syllabus(search, page, pageSize);
    } else {
      fetch_syllabus(page, pageSize);
    }
  };
  const searchSyllabus = (value) => {
    search_syllabus(value, 1, 12);
    setSearch(value);
  };
  return (
    <>
      {user.role && user.role !== 1 ? (
        history.push("/user/dashboard")
      ) : (
        <div className='dash-user'>
          <Button
            type='primary'
            size='large'
            block
            onClick={() => {
              add_modal(true);
            }}
          >
            ADD SYLLABUS
          </Button>
          <Divider />
          <SearchForm action={searchSyllabus} />
          <Divider />
          <div className='row'>
            {loading ? (
              <>
                <div className='col-12 col-md-4 '>
                  <Skeleton active paragraph={{ rows: 6 }} />
                </div>
                <div className='col-12 col-md-4 '>
                  <Skeleton active paragraph={{ rows: 6 }} />
                </div>
                <div className='col-12 col-md-4 '>
                  <Skeleton active paragraph={{ rows: 6 }} />
                </div>
              </>
            ) : (
              data_syllbus
            )}
          </div>
          <div className='justify-content-center d-flex mt-5'>
            <Pagination
              total={total}
              defaultCurrent={1}
              current={pageNumber}
              onChange={changePage}
              hideOnSinglePage={true}
              pageSize={12}
              showSizeChanger={false}
            />
          </div>
        </div>
      )}

      <Modal
        visible={modal}
        centered={true}
        closable={true}
        footer={null}
        onCancel={modal_action}
        zIndex={1050}
        width={500}
      >
        <SyllabusForm
          action={add_syllabus}
          add_btn={add_btn}
          resetform={resetfrom}
        />
      </Modal>
    </>
  );
};

const mapStateToProps = (state) => ({
  user: state.auth.user,
  modal: state.syllabus.syllabus_modal,
  add_btn: state.syllabus.add_btn,
  resetfrom: state.syllabus.resetform,
  loading: state.syllabus.loading,
  syllabus: state.syllabus.syllabus,
  total: state.syllabus.syllabus_total,
  pageNumber: state.syllabus.pageNumber,
});
const mapDispatchToProps = {
  add_modal,
  add_syllabus,
  fetch_syllabus,
  search_syllabus,
  delete_syllabus,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AdmnSyllabus));
