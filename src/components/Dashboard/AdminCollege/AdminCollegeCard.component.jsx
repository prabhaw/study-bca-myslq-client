import React, { useState } from "react";
import { Card, Checkbox, Modal, Popconfirm } from "antd";
import { FaEye } from "react-icons/fa";
import { MdDelete } from "react-icons/md";
import AdminCollegeDetails from "./AdminCollegeDetails.component";

const IMG_URL = process.env.REACT_APP_COLLEGE_IMG_URL;

const AdminCollegeCard = (props) => {
  const { college, active, remove } = props;

  const [modal, setModal] = useState(false);
  const onVerified = (e) => {
    const data = { verified: e.target.checked };
    active(college.id, data);
  };
  const onTopCollege = (e) => {
    const data = { top_college: e.target.checked };
    active(college.id, data);
  };
  const onDelete = () => {
    remove(college.id);
  };
  return (
    <>
      <Card
        hoverable
        cover={
          <img
            alt='college_card_image'
            src={`${IMG_URL}/${college.college_image}`}
            height='200'
          />
        }
        actions={[
          <FaEye
            key='view'
            style={{ fontSize: "20px" }}
            onClick={() => {
              setModal(true);
            }}
          />,

          <Popconfirm
            title='Are you sure delete this college?'
            onConfirm={onDelete}
            okText='Yes'
            cancelText='No'
          >
            <MdDelete key='remove' style={{ fontSize: "20px" }} />
          </Popconfirm>,
          <Checkbox defaultChecked={college.verified} onChange={onVerified}>
            Active
          </Checkbox>,
          <Checkbox
            defaultChecked={college.top_college}
            onChange={onTopCollege}
          >
            Top
          </Checkbox>,
        ]}
      >
        <h6 style={{ fontWeight: "bold" }}>
          {college.college_name.length > 60 ? (
            <span>{`${college.college_name.substring(0, 60)}...`}</span>
          ) : (
            <span>{college.college_name}</span>
          )}
        </h6>
        <p>
          {college.address.length > 60 ? (
            <span>{`${college.address.substring(0, 60)}...`}</span>
          ) : (
            <span>{college.address}</span>
          )}
        </p>
      </Card>

      <Modal
        visible={modal}
        centered={true}
        closable={true}
        footer={null}
        onCancel={() => {
          setModal(false);
        }}
        zIndex={1050}
        width={1000}
      >
        <div className='profile'>
          <AdminCollegeDetails college={college} />
        </div>
      </Modal>
    </>
  );
};

export default AdminCollegeCard;
