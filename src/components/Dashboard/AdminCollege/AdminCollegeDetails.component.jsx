import React from "react";
import htmlparser from "react-html-parser";
const IMG_URL = process.env.REACT_APP_COLLEGE_IMG_URL;

const AdminCollegeDetails = (props) => {
  const { college } = props;
  return (
    <>
      <img
        alt='example'
        src={`${IMG_URL}/${college.college_image}`}
        height='400'
        style={{ width: "100%" }}
      />

      <div className='mt-3'>
        <h4>
          <strong>
            {college.college_name} &nbsp; (
            <span style={{ color: "#1DA57A" }}>{college.university}</span>)
          </strong>
        </h4>
        <p>
          <strong>{college.address}</strong>
        </p>
        <div style={{ fontSize: "16px", fontWeight: "12px" }}>
          {htmlparser(college.description)}
        </div>
      </div>
    </>
  );
};

export default AdminCollegeDetails;
