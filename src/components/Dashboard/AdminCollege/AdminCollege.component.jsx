import React, { useEffect, useState } from "react";
import "./AdminCollege.component.less";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Input, Skeleton, Pagination } from "antd";
import AdminCollegeCard from "./AdminCollegeCard.component";
import {
  admin_college_fetch,
  search_admin_college,
  on_college_active,
  on_college_delete,
} from "./../../../Redux/actions/college.action";
const { Search } = Input;
const AdminCollege = (props) => {
  const {
    user,
    history,
    loading,
    colleges,
    total,
    pageNumber,
    admin_college_fetch,
    search_admin_college,
    on_college_active,
    on_college_delete,
  } = props;
  const [search, setSarch] = useState();
  useEffect(() => {
    admin_college_fetch(1, 12);
  }, [admin_college_fetch]);
  useEffect(() => {
    document.title = "Admin Colleges";
  }, []);
  const searchfun = (value) => {
    setSarch(value);
    search_admin_college(value, 1, 12);
  };
  const changePage = (page, pageSize) => {
    if (search) {
      search_admin_college(search, page, pageSize);
    } else {
      admin_college_fetch(page, pageSize);
    }
  };
  const onActive = (id, data) => {
    on_college_active(id, data);
  };
  const deleteCollege = (id) => {
    on_college_delete(id);
  };
  const college = colleges.map((item) => (
    <div key={item.id} className='col-12 col-md-4 mb-3'>
      <AdminCollegeCard
        college={item}
        active={onActive}
        remove={deleteCollege}
      />
    </div>
  ));
  return (
    <>
      {user.role && user.role !== 1 ? (
        history.push("/user/dashboard")
      ) : (
        <div className='dash-user'>
          <div className='justify-content-center row search-section'>
            <div className='col-12 col-md-6'>
              <div className='row flex-column-reverse flex-md-row'>
                <Search
                  placeholder='Enter College Details'
                  onSearch={searchfun}
                  size='large'
                  style={{ width: "100%" }}
                />
              </div>
            </div>
          </div>
          <div className='row mt-5'>
            {loading ? (
              <>
                <div className='col-12 col-md-4 mb-3'>
                  <Skeleton active paragraph={{ rows: 6 }} />
                </div>

                <div className='col-12 col-md-4 mb-3'>
                  <Skeleton active paragraph={{ rows: 6 }} />
                </div>
                <div className='col-12 col-md-4 mb-3'>
                  <Skeleton active paragraph={{ rows: 6 }} />
                </div>
              </>
            ) : (
              college
            )}
          </div>
          <div className='justify-content-center d-flex mt-5'>
            <Pagination
              total={total}
              defaultCurrent={1}
              current={pageNumber}
              onChange={changePage}
              hideOnSinglePage={true}
              pageSize={12}
              showSizeChanger={false}
            />
          </div>
        </div>
      )}
    </>
  );
};

const mapStateToProps = (state) => ({
  user: state.auth.user,
  loading: state.college.loading,
  colleges: state.college.admin_colleges,
  total: state.college.admin_total,
  pageNumber: state.college.admin_pageNumber,
});
const mapDispatchToProps = {
  admin_college_fetch,
  search_admin_college,
  on_college_active,
  on_college_delete,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AdminCollege));
