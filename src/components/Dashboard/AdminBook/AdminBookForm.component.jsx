import React, { useEffect, useState } from "react";
import { Select, Form, Button, Input } from "antd";
import { connect } from "react-redux";
import {
  syllabus_university,
  search_sem,
  search_subj,
} from "./../../../Redux/actions/search.action";
const { Option } = Select;

const AdminBookForm = (props) => {
  const {
    university,
    syllabus_university,
    semester,
    subject,
    search_sem,
    search_subj,
    add_btn,
    resetform,
    action,
  } = props;
  const [form] = Form.useForm();
  const [data, setData] = useState();
  const [fileerr, setFileErr] = useState();
  const [file, setFile] = useState("");

  useEffect(() => {
    syllabus_university();
  }, [syllabus_university]);

  useEffect(() => {
    if (resetform) {
      form.resetFields();
      setFile("");
      setFileErr("");
    }
  }, [resetform, form]);

  // ...............file-------------

  const handleFile = (e) => {
    let { files, type } = e.target;

    if (type === "file") {
      if (files[0]) {
        if (
          files[0].type === "image/jpeg" ||
          files[0].type === "image/png" ||
          files[0].type === "image/jpg"
        ) {
          setFile(files[0]);
          setFileErr("");
        } else {
          setFileErr("Onley JPEG PNG format is accepted.");
          setFile("");
        }
      }
    }
  };

  //   ------------------------------------

  const university_data = university.map((item, i) => (
    <Option value={item} key={i}>
      {item}
    </Option>
  ));

  const semester_data = semester.map((item, i) => (
    <Option value={item} key={i}>
      {item}
    </Option>
  ));
  const subj_data = subject.map((item, i) => (
    <Option value={item} key={i}>
      {item}
    </Option>
  ));
  const onUniversityFormChange = (value) => {
    search_sem({ university: value });
    setData((pre) => ({ ...pre, university: value }));
  };
  const onSemSelect = (value) => {
    search_subj({ ...data, semester: value });
    setData((pre) => ({ ...pre, semester: value }));
  };
  const onFinish = (value) => {
    if (file) {
      const formData = new FormData();
      formData.append("book_img", file);
      for (var key in value) {
        formData.append(key, value[key]);
      }
      action(formData);
    } else {
      setFileErr("Please Input Image.");
    }
  };

  return (
    <div className='pt-5 form-with-file'>
      <Form name='adminBookForm' form={form} onFinish={onFinish}>
        <Form.Item
          name='university'
          rules={[{ required: true, message: "Select University" }]}
        >
          <Select
            placeholder='Select University'
            size='large'
            onChange={onUniversityFormChange}
          >
            {university_data}
          </Select>
        </Form.Item>

        <Form.Item
          name='semester'
          rules={[{ required: true, message: "Select Semester" }]}
        >
          <Select
            placeholder='Select Semester'
            size='large'
            onChange={onSemSelect}
          >
            {semester_data}
          </Select>
        </Form.Item>

        <Form.Item
          name='subject'
          rules={[{ required: true, message: "Select Subject" }]}
        >
          <Select placeholder='Select Subject' size='large'>
            {subj_data}
          </Select>
        </Form.Item>
        <Form.Item
          name='name'
          rules={[
            {
              required: true,
              message: "Please input Book NAme",
            },
          ]}
        >
          <Input placeholder='Book Name' size='large' />
        </Form.Item>
        <Form.Item
          name='author'
          rules={[
            {
              required: true,
              message: "Please input Book Author.",
            },
          ]}
        >
          <Input placeholder='Book Author' size='large' />
        </Form.Item>
        <Form.Item
          name='publication'
          rules={[
            {
              required: true,
              message: "Please input Book Publication.",
            },
          ]}
        >
          <Input placeholder='Book Publication' size='large' />
        </Form.Item>

        <div className='formm-group' style={{ margin: "20px 0px" }}>
          <div
            className='input-group'
            style={fileerr ? { border: "1px solid red" } : {}}
          >
            <label className='input-group-btn my-0'>
              <span
                className='btn btn-large btn-outline-primary rounded-0'
                id='browse'
              >
                Browse&hellip;
                <input id='csv-input' type='file' onChange={handleFile} />
              </span>
            </label>
            <input
              type='text'
              className='form-control rounded-0'
              readOnly
              placeholder='Upload Book Cover Image.'
              value={file ? file.name : ""}
            />
          </div>
          {fileerr ? <label style={{ color: "red" }}>{fileerr}</label> : null}
        </div>
        <Form.Item>
          <Button
            type='primary'
            htmlType='submit'
            size='large'
            loading={add_btn}
            block
          >
            Post
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

const mapStateToProps = (state) => ({
  university: state.search.search,
  semester: state.search.semester,
  subject: state.search.subject,
});
const mapDispatchToProps = { syllabus_university, search_sem, search_subj };
export default connect(mapStateToProps, mapDispatchToProps)(AdminBookForm);
