import React, { useEffect } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import "./AdminBook.component.less";
import { Divider, Button, Modal, Skeleton, Pagination } from "antd";
import AdminBookFormComponent from "./AdminBookForm.component";
import BookCard from "../../common/BookCard/BookCard.component";
import {
  book_modal,
  add_book,
  fetch_book,
  on_book_delete,
} from "./../../../Redux/actions/books.action";
const AdminBook = (props) => {
  const {
    user,
    history,
    book_modal,
    modal,
    add_book,
    add_btn,
    formreset,
    book,
    loading,
    total,
    pageNumber,
    fetch_book,
    on_book_delete,
  } = props;
  useEffect(() => {
    fetch_book(1, 5);
  }, [fetch_book]);
  const changePage = (page, pageSize) => {
    fetch_book(page, pageSize);
  };

  const book_data = book.map((item) => (
    <div className='col-12 col-md-10' key={item.id}>
      <BookCard admin={true} book={item} onDelete={on_book_delete} />
    </div>
  ));
  return (
    <>
      {user.role && user.role !== 1 ? (
        history.push("/user/dashboard")
      ) : (
        <div className='dash-user'>
          <div className='justify-content-center row search-section'>
            <div className='col-12 col-md-8'>
              <Button
                type='primary'
                size='large'
                block
                onClick={() => {
                  book_modal();
                }}
              >
                Add Book
              </Button>
            </div>
          </div>
          <Divider />
          <div className='row justify-content-center'>
            {loading ? (
              <div className='col-12 col-md-10'>
                <Skeleton active paragraph={{ rows: 8 }} />
              </div>
            ) : (
              book_data
            )}
          </div>
          <div className='justify-content-center d-flex mt-5'>
            <Pagination
              total={total}
              defaultCurrent={1}
              current={pageNumber}
              onChange={changePage}
              hideOnSinglePage={true}
              pageSize={5}
              showSizeChanger={false}
            />
          </div>
        </div>
      )}

      <Modal
        visible={modal}
        centered={true}
        closable={true}
        footer={null}
        onCancel={() => {
          book_modal();
        }}
        zIndex={1050}
        width={500}
      >
        <AdminBookFormComponent
          action={add_book}
          add_btn={add_btn}
          resetform={formreset}
        />
      </Modal>
    </>
  );
};

const mapStateToProps = (state) => ({
  user: state.auth.user,
  modal: state.books.book_modal,
  pageNumber: state.books.pageNumber,
  loading: state.books.loading,
  add_btn: state.books.add_btn,
  formreset: state.books.formreset,
  book: state.books.book,
  total: state.books.total,
});
const mapDispatchToProps = { book_modal, add_book, fetch_book, on_book_delete };
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AdminBook));
