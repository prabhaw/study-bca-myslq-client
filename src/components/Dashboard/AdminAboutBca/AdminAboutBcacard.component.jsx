import React, { useState } from "react";
import { UserOutlined } from "@ant-design/icons";
import htmltoword from "react-html-parser";
import { Card, Avatar, Checkbox, Button } from "antd";
const { Meta } = Card;
const User_IMG_URL = process.env.REACT_APP_USER_IMG_URL;

const AdminAboutBcaCard = (props) => {
  const { aboutbca, action } = props;
  const [readmore, setReadMore] = useState(false);

  const onChanges = (e) => {
    console.log("HEllo");
    const data = { verified: e.target.checked };
    action(aboutbca.id, data);
  };

  const profile =
    aboutbca.user && aboutbca.user.profile_image ? (
      <Avatar
        size={64}
        src={`${User_IMG_URL}/${aboutbca.user.profile_image}`}
      />
    ) : (
      <Avatar size={64} icon={<UserOutlined />} />
    );
  return (
    <>
      <Card
        extra={
          <Checkbox defaultChecked={aboutbca.verified} onChange={onChanges} />
        }
        title={
          <Meta
            avatar={profile}
            title={aboutbca.user.first_name + " " + aboutbca.user.last_name}
            description={aboutbca.user.email}
          />
        }
      >
        {readmore ? (
          <>
            <p style={{ fontWeight: "bold" }}>{aboutbca.short_description}</p>
            <div className='descriptio editor-post '>
              {htmltoword(aboutbca.description)}
              <Button
                type='link'
                onClick={() => {
                  setReadMore(false);
                }}
              >
                Show less
              </Button>
            </div>
          </>
        ) : (
          <p style={{ fontWeight: "bold" }}>
            {aboutbca.short_description}....
            <Button
              type='link'
              onClick={() => {
                setReadMore(true);
              }}
            >
              Read More
            </Button>
          </p>
        )}
      </Card>
    </>
  );
};

export default AdminAboutBcaCard;
