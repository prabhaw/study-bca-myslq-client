import React, { useEffect, useState } from "react";
import "./AdminAboutBca.component.less";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Input, Skeleton, Pagination } from "antd";
import AdminAboutBcaCard from "./AdminAboutBcacard.component";
import {
  admin_fetch,
  admin_search,
  update_aboutbca_admin,
} from "./../../../Redux/actions/aboutbca.action";
const { Search } = Input;

const AdminAboutBca = (props) => {
  const {
    user,
    history,
    admin_fetch,
    loading,
    all_bca,
    pageNumber,
    total,
    admin_search,
    update_aboutbca_admin,
  } = props;
  const [search, setSearch] = useState("");

  useEffect(() => {
    document.title = "Admin About BCA";
    admin_fetch(1, 12);
  }, [admin_fetch]);
  const changePage = (page, pageSize) => {
    if (search) {
      admin_search(search, page, pageSize);
    } else {
      admin_fetch(page, pageSize);
    }
  };

  const searchfun = (data) => {
    setSearch(data);
    admin_search(data, 1, 12);
  };
  const state_update = (id, data) => {
    update_aboutbca_admin(id, data);
  };
  const bca = all_bca.map((item) => (
    <div className='col-12 col-md-6 mb-3' key={item.id}>
      <AdminAboutBcaCard aboutbca={item} action={state_update} />
    </div>
  ));

  return (
    <>
      {user.role && user.role !== 1 ? (
        history.push("/user/dashboard")
      ) : (
        <div className='dash-user'>
          <div className='justify-content-center row search-section'>
            <div className='col-12 col-md-8'>
              <div className='row flex-column-reverse flex-md-row'>
                <Search
                  placeholder='Enter User Name or Email'
                  onSearch={searchfun}
                  size='large'
                  style={{ width: "100%" }}
                />
              </div>
            </div>
          </div>
          <div className='row mt-3'>
            {loading ? (
              <>
                <div className='col-12 col-md-6 mb-3'>
                  <Skeleton active paragraph={{ rows: 12 }} />{" "}
                </div>

                <div className='col-12 col-md-6 mb-3'>
                  <Skeleton active paragraph={{ rows: 12 }} />{" "}
                </div>
              </>
            ) : (
              bca
            )}
          </div>
          <div className='justify-content-center d-flex'>
            <Pagination
              total={total}
              defaultCurrent={1}
              current={pageNumber}
              onChange={changePage}
              hideOnSinglePage={true}
              pageSize={12}
              showSizeChanger={false}
            />
          </div>
        </div>
      )}
    </>
  );
};

const mapStateToProps = (state) => ({
  user: state.auth.user,
  loading: state.bca.admin_loading,
  all_bca: state.bca.all_aboutbca,
  pageNumber: state.bca.pageNumber,
  total: state.bca.total,
});
const mapDispatchToProps = { admin_fetch, admin_search, update_aboutbca_admin };
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AdminAboutBca));
