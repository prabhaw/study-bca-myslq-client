import React, { useEffect } from "react";
import "./AboutBca.component.less";
import { connect } from "react-redux";
import { Avatar, Modal, Skeleton } from "antd";
import { UserOutlined } from "@ant-design/icons";
import AboutBcaDetail from "./AboutBcaDetail.component";
import AboutBcaForm from "./AboutBcaForm.component";
import {
  add_bca_modal,
  fetch_bca_user,
  add_about_bca,
} from "./../../../Redux/actions/aboutbca.action";
const IMG_URL = process.env.REACT_APP_USER_IMG_URL;

const AboutBca = (props) => {
  const {
    user,
    history,
    add_bca_modal,
    add_modal,
    aboutbca,
    loading,
    fetch_bca_user,
    add_about_bca,
    add_btn,
  } = props;

  useEffect(() => {
    document.title = "About BCA";
    if (user.role && user.role !== 1 && user.role !== 2) {
      history.push("/user/dashboard");
    }
  }, [user, history]);

  const add_modal_action = () => {
    add_bca_modal();
  };
  const create_post = (data) => {
    add_about_bca(data);
  };
  useEffect(() => {
    fetch_bca_user();
  }, [fetch_bca_user]);

  return (
    <>
      <div className='dash-blog'>
        {!aboutbca ? (
          <div className='justify-content-center row search-section'>
            <div className='col-12 col-md-8'>
              <div className='row  '>
                <div className='col-12 '>
                  <div id='c-c-main'>
                    <div className='tb'>
                      <div className='td' id='p-c-i'>
                        {user && user.profile_image ? (
                          <Avatar
                            size={51}
                            src={`${IMG_URL}/${user.profile_image}`}
                          />
                        ) : (
                          <Avatar size={51} icon={<UserOutlined />} />
                        )}
                      </div>
                      <div
                        className='td '
                        style={{ width: "100%", height: "100%" }}
                        id='c-inp'
                      >
                        <div className='form-label-group'>
                          <input
                            type='text'
                            placeholder=' Write About BCA....'
                            className='form-control shadow-none'
                            readOnly={true}
                            onClick={add_modal_action}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : loading ? (
          <Skeleton active paragraph={{ rows: 12 }} />
        ) : (
          <AboutBcaDetail bca={aboutbca} />
        )}
      </div>

      <Modal
        visible={add_modal}
        centered={true}
        closable={true}
        footer={null}
        onCancel={add_modal_action}
        zIndex={1050}
        width={900}
      >
        <AboutBcaForm
          action={create_post}
          addbtn={add_btn}
          btn_text={"Add Post"}
        />
      </Modal>
    </>
  );
};

const mapStateToProps = (state) => ({
  user: state.auth.user,
  add_modal: state.bca.add_model,
  loading: state.bca.loading,
  aboutbca: state.bca.aboutbca,
  add_btn: state.bca.add_btn,
});
const mapDispatchToProps = {
  add_bca_modal,
  fetch_bca_user,
  add_about_bca,
};
export default connect(mapStateToProps, mapDispatchToProps)(AboutBca);
