import React, { useEffect, useState } from "react";
import { Form, Button, Input } from "antd";
import JoditEditor from "jodit-react";
import Titles from "./../../common/Title/Titles.component";
const { TextArea } = Input;
const AboutBcaForm = (props) => {
  const { update, action, bca, addbtn } = props;
  const [form] = Form.useForm();
  const [data, setData] = useState({});
  useEffect(() => {
    if (update) {
      form.setFieldsValue({
        short_description: bca.short_description,
      });
    }
  }, [update, form, bca]);
  const handleOnChange = (value) => {
    setData((pre) => ({ ...pre, description: value }));
  };
  const onFormChange = (value) => {
    setData((pre) => ({ ...pre, ...value }));
  };
  const onFinish = () => {
    action(data);
  };
  return (
    <>
      <Titles>Write About BCA</Titles>
      <div className='mt-5'></div>
      <Form
        onFinish={onFinish}
        onValuesChange={onFormChange}
        form={form}
        scrollToFirstError={true}
      >
        <div className='formm-group'>
          <Form.Item
            name='short_description'
            rules={[
              {
                required: true,
                message: "Please input  Short Description",
              },
            ]}
          >
            <TextArea
              placeholder='Write short  about BCA....'
              size='large'
              autoSize={{ minRows: 4, maxRows: 4 }}
              maxLength='500'
            />
          </Form.Item>
        </div>
        <div className='formm-group'>
          <label htmlFor='description'>
            <strong>Description</strong>
          </label>
          <JoditEditor
            value={bca && bca.description ? bca.description : ""}
            tabIndex={1}
            name='description'
            onChange={handleOnChange}
          />
        </div>

        <Form.Item>
          <div className=' d-flex flex-row-reverse mt-3'>
            <div className='col-12 col-md-2' style={{ padding: "0" }}>
              <Button
                type='primary'
                htmlType='submit'
                loading={addbtn}
                block
                size='large'
              >
                {props.btn_text}
              </Button>
            </div>
          </div>
        </Form.Item>
      </Form>
    </>
  );
};

export default AboutBcaForm;
