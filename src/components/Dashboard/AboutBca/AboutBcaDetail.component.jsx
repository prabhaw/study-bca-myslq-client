import React from "react";
import { Card, Avatar, List, Menu, Dropdown, Modal } from "antd";
import { FaBars, FaEdit } from "react-icons/fa";
import { MdDelete } from "react-icons/md";
import { DeleteOutlined, UserOutlined } from "@ant-design/icons";
import AboutBcaForm from "./AboutBcaForm.component";
import htmltoword from "react-html-parser";
import { connect } from "react-redux";
import {
  update_bca_modal,
  update_about_bca,
  delete_aboutbca,
} from "./../../../Redux/actions/aboutbca.action";
const User_IMG_URL = process.env.REACT_APP_USER_IMG_URL;
const { Meta } = Card;
const { confirm } = Modal;

const AboutBcaDetail = (props) => {
  const {
    bca,
    update_bca_modal,
    update_modal,
    update_about_bca,
    delete_aboutbca,
  } = props;

  const showDeleteConfirm = () => {
    confirm({
      title: "Are you sure delete this Post?",
      icon: <DeleteOutlined style={{ color: "red" }} />,
      content: "The post will no longer available to this site.",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      zIndex: "1500",
      onOk() {
        delete_aboutbca(bca.id);
      },
    });
  };
  const update_bca = (data) => {
    update_about_bca(bca.id, data);
  };
  const menu = (
    <Menu>
      <Menu.Item
        key='0'
        style={{ color: "blue" }}
        onClick={() => {
          update_bca_modal();
        }}
      >
        <FaEdit style={{ fontSize: "18px" }} />
        Edit Post
      </Menu.Item>
      <Menu.Item key='1' style={{ color: "red" }} onClick={showDeleteConfirm}>
        <MdDelete style={{ fontSize: "18px" }} />
        Delete Post
      </Menu.Item>
    </Menu>
  );

  const profile =
    bca.user && bca.user.profile_image ? (
      <Avatar size={64} src={`${User_IMG_URL}/${bca.user.profile_image}`} />
    ) : (
      <Avatar size={64} icon={<UserOutlined />} />
    );
  return (
    <>
      <div className='mb-3 '>
        <List.Item
          actions={[
            <Dropdown
              overlay={menu}
              trigger={["click"]}
              placement='bottomRight'
            >
              <FaBars key='menu' style={{ fontSize: "20px" }} />
            </Dropdown>,
          ]}
        >
          <Meta
            avatar={profile}
            title={
              bca.user ? bca.user.first_name + " " + bca.user.last_name : ""
            }
            description={
              bca.user ? (bca.user.role === 1 ? "Admin" : "Teacher") : ""
            }
          />
        </List.Item>

        <div className='mt-3  '>
          <h6>{bca.short_description}</h6>

          <div className='descriptio editor-post '>
            {htmltoword(bca.description)}
          </div>
        </div>
      </div>
      <Modal
        visible={update_modal}
        centered={true}
        closable={true}
        footer={null}
        onCancel={() => {
          update_bca_modal();
        }}
        zIndex={1050}
        width={900}
      >
        <AboutBcaForm
          bca={bca}
          update={true}
          btn_text={"Update Post"}
          action={update_bca}
        />
      </Modal>
    </>
  );
};

const mapStateToProps = (state) => ({
  update_modal: state.bca.update_modal,
  update_btn: state.bca.add_btn,
});
const mapDispatchToProps = {
  update_bca_modal,
  update_about_bca,
  delete_aboutbca,
};
export default connect(mapStateToProps, mapDispatchToProps)(AboutBcaDetail);
