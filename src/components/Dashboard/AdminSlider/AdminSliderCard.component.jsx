import React from "react";
import { Button } from "antd";
const SLIDER_URL = process.env.REACT_APP_SLIDER_IMG_URL;
const AdminSliderCard = (props) => {
  return (
    <>
      <div className='card'>
        <img
          className='card-img-top'
          src={`${SLIDER_URL}/${props.image.slider_img}`}
          alt='SLIDER_IMAGE'
          style={{ maxHeight: "300px", minHeight: "300px" }}
        />
        <div className='card-footer'>
          <Button
            type='primary'
            block
            danger
            onClick={() => {
              props.action(props.image.id);
            }}
          >
            Delete
          </Button>
        </div>
      </div>
    </>
  );
};

export default AdminSliderCard;
