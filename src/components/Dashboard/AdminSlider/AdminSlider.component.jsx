import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Divider, Button, Skeleton } from "antd";
import AdminSliderCard from "./AdminSliderCard.component";
import {
  add_slider,
  fetch_slider,
  delete_slider,
} from "./../../../Redux/actions/slider.action";

const AdminSlider = (props) => {
  const {
    user,
    history,
    add_slider,
    fetch_slider,
    delete_slider,
    loading,
    add_btn,
    sliders,
  } = props;
  const [fileerr, setFileErr] = useState();
  const [file, setFile] = useState("");
  useEffect(() => {
    document.title = "Admin Slider";
    fetch_slider();
  }, [fetch_slider]);

  // ...............file-------------

  const handleFile = (e) => {
    let { files, type } = e.target;

    if (type === "file") {
      if (files[0]) {
        if (
          files[0].type === "image/jpeg" ||
          files[0].type === "image/png" ||
          files[0].type === "image/jpg"
        ) {
          setFile(files[0]);
          setFileErr("");
        } else {
          setFileErr("Onley JPEG/PNG format is accepted.");
          setFile("");
        }
      }
    }
  };
  const submit = (event) => {
    event.preventDefault();
    if (file) {
      const formData = new FormData();
      formData.append("slider_img", file);
      add_slider(formData);
    } else {
      setFileErr("Please Input Slider Image.");
    }
  };
  return (
    <>
      {user.role && user.role !== 1 ? (
        history.push("/user/dashboard")
      ) : (
        <div className='dash-user'>
          <div className='justify-content-center row'>
            <form className='form-with-file' onSubmit={submit}>
              <div className='form-group'>
                <div
                  className='input-group'
                  style={fileerr ? { border: "1px solid red" } : {}}
                >
                  <label className='input-group-btn my-0'>
                    <span
                      className='btn btn-large btn-outline-primary rounded-0'
                      id='browse'
                    >
                      Browse&hellip;
                      <input id='csv-input' type='file' onChange={handleFile} />
                    </span>
                  </label>
                  <input
                    type='text'
                    className='form-control rounded-0'
                    readOnly
                    placeholder='Upload Old Questions'
                    value={file ? file.name : ""}
                  />
                </div>
                {fileerr ? <p style={{ color: "red" }}>{fileerr}</p> : null}
              </div>
              <div className='form-group'>
                <Button
                  type='primary'
                  htmlType='submit'
                  size='large'
                  block
                  loading={add_btn}
                >
                  Add Book
                </Button>
              </div>
            </form>
          </div>

          <Divider />

          <div className='row'>
            {loading ? (
              <>
                <div className='col-12 col-md-6 mb-3'>
                  <Skeleton active paragraph={{ rows: 5 }} />
                </div>
                <div className='col-12 col-md-6 mb-3'>
                  <Skeleton active paragraph={{ rows: 5 }} />
                </div>
              </>
            ) : (
              sliders.map((item) => (
                <div className='col-12 col-md-6 mb-3' key={item.id}>
                  <AdminSliderCard image={item} action={delete_slider} />
                </div>
              ))
            )}
          </div>
        </div>
      )}
    </>
  );
};

const mapStateToProps = (state) => ({
  user: state.auth.user,
  loading: state.slider.loading,
  add_btn: state.slider.add_btn,
  sliders: state.slider.sliders,
});
const mapDispatchToProps = { add_slider, fetch_slider, delete_slider };
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AdminSlider));
