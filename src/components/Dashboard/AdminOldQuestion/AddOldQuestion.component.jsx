import React, { useEffect, useState } from "react";
import { Select, Form, Button, DatePicker } from "antd";
import { connect } from "react-redux";
import {
  syllabus_university,
  search_sem,
  search_subj,
} from "./../../../Redux/actions/search.action";
const { Option } = Select;

const AdminOldQuestionForm = (props) => {
  const {
    university,
    syllabus_university,
    semester,
    subject,
    search_sem,
    search_subj,

    action,
    add_btn,
    formreset,
  } = props;
  const [form] = Form.useForm();
  const [data, setData] = useState();
  const [fileerr, setFileErr] = useState();
  const [file, setFile] = useState("");
  const [date, setDate] = useState();

  useEffect(() => {
    syllabus_university();
  }, [syllabus_university]);

  useEffect(() => {
    if (formreset) {
      form.resetFields();
      setFile("");
      setFileErr("");
    }
  }, [formreset, form]);

  // ...............file-------------

  const handleFile = (e) => {
    let { files, type } = e.target;

    if (type === "file") {
      if (files[0]) {
        if (files[0].type === "application/pdf") {
          setFile(files[0]);
          setFileErr("");
        } else {
          setFileErr("Onley PDF format is accepted.");
          setFile("");
        }
      }
    }
  };

  //   ------------------------------------

  const university_data = university.map((item, i) => (
    <Option value={item} key={i}>
      {item}
    </Option>
  ));

  const semester_data = semester.map((item, i) => (
    <Option value={item} key={i}>
      {item}
    </Option>
  ));
  const subj_data = subject.map((item, i) => (
    <Option value={item} key={i}>
      {item}
    </Option>
  ));
  const onUniversityFormChange = (value) => {
    search_sem({ university: value });
    setData((pre) => ({ ...pre, university: value }));
  };
  const onSemSelect = (value) => {
    search_subj({ ...data, semester: value });
    setData((pre) => ({ ...pre, semester: value }));
  };

  const onFinish = (value) => {
    if (file) {
      const formData = new FormData();
      formData.append("old_question", file);
      formData.append("year", date);

      for (var key in value) {
        formData.append(key, value[key]);
      }

      action(formData);
    } else {
      setFileErr("Please Input Old Question.");
    }
  };
  const dataChange = (date, dates) => {
    setDate(dates);
  };
  return (
    <div className='pt-5 form-with-file'>
      <Form name='OldquestionForm' form={form} onFinish={onFinish}>
        <Form.Item
          name='university'
          rules={[{ required: true, message: "Select University" }]}
        >
          <Select
            placeholder='Select University'
            size='large'
            onChange={onUniversityFormChange}
          >
            {university_data}
          </Select>
        </Form.Item>

        <Form.Item
          name='semester'
          rules={[{ required: true, message: "Select Semester" }]}
        >
          <Select
            placeholder='Select Semester'
            size='large'
            onChange={onSemSelect}
          >
            {semester_data}
          </Select>
        </Form.Item>

        <Form.Item
          name='subject'
          rules={[{ required: true, message: "Select Subject" }]}
        >
          <Select placeholder='Select Subject' size='large'>
            {subj_data}
          </Select>
        </Form.Item>

        <Form.Item
          name='exam_year'
          rules={[{ required: true, message: "Select Exam Date" }]}
        >
          <DatePicker
            size='large'
            picker='year'
            onChange={dataChange}
            style={{ width: "100%" }}
          />
        </Form.Item>
        <Form.Item
          name='exam_time'
          rules={[{ required: true, message: "Select Exam Type" }]}
        >
          <Select placeholder='Select Exam Type' size='large'>
            <Option value='REGULAR'>REGULAR</Option>
            <Option value='RETAKE'>PARTIAL</Option>
          </Select>
        </Form.Item>
        <div className='formm-group' style={{ margin: "20px 0px" }}>
          <div
            className='input-group'
            style={fileerr ? { border: "1px solid red" } : {}}
          >
            <label className='input-group-btn my-0'>
              <span
                className='btn btn-large btn-outline-primary rounded-0'
                id='browse'
              >
                Browse&hellip;
                <input id='csv-input' type='file' onChange={handleFile} />
              </span>
            </label>
            <input
              type='text'
              className='form-control rounded-0'
              readOnly
              placeholder='Upload Old Questions'
              value={file ? file.name : ""}
            />
          </div>
          {fileerr ? <p style={{ color: "red" }}>{fileerr}</p> : null}
        </div>
        <Form.Item>
          <Button
            type='primary'
            htmlType='submit'
            size='large'
            loading={add_btn}
            block
          >
            Post Old Question
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

const mapStateToProps = (state) => ({
  university: state.search.search,
  semester: state.search.semester,
  subject: state.search.subject,
});
const mapDispatchToProps = { syllabus_university, search_sem, search_subj };
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdminOldQuestionForm);
