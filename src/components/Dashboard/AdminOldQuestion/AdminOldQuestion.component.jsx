import React, { useEffect } from "react";
import "./AdminOldQuestion.component.less";
import { Divider, Avatar, Pagination, Modal, Skeleton } from "antd";
import { connect } from "react-redux";
import { UserOutlined } from "@ant-design/icons";
import AddOldQuestionComponent from "./AddOldQuestion.component";
import AdminOldQuestionCard from "./AdminOldQuestionCard.component";
import {
  add_question_modal,
  add_question,
  fetch_admin,
  delete_question,
} from "./../../../Redux/actions/question.action";
const IMG_URL = process.env.REACT_APP_USER_IMG_URL;

const AdminOldQuestion = (props) => {
  const {
    user,
    modal,
    add_modal,
    add_question,
    questions,
    add_btn,
    loading,
    formreset,
    total,
    pageNumber,
    fetch_admin,
    delete_question,
  } = props;
  useEffect(() => {
    document.title = "BCA-OldQuestion";
    fetch_admin(1, 12);
  }, [fetch_admin]);
  const changePage = (page, pageSize) => {
    fetch_admin(page, pageSize);
  };
  return (
    <>
      <div className='dash-blog'>
        <div className='justify-content-center row search-section'>
          <div className='col-12 col-md-6'>
            <div className='row  '>
              <div className='col-12 '>
                <div id='c-c-main'>
                  <div className='tb'>
                    <div className='td' id='p-c-i'>
                      {user && user.profile_image ? (
                        <Avatar
                          size={51}
                          src={`${IMG_URL}/${user.profile_image}`}
                        />
                      ) : (
                        <Avatar size={51} icon={<UserOutlined />} />
                      )}
                    </div>
                    <div
                      className='td '
                      style={{ width: "100%", height: "100%" }}
                      id='c-inp'
                    >
                      <div className='form-label-group mt-3'>
                        <button
                          className='add-note'
                          style={{ width: "100%" }}
                          onClick={() => {
                            add_modal();
                          }}
                        >
                          Add OldQuestion
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Divider />
        <div className='row justify-content-center'>
          {loading ? (
            <>
              <div className='col-12 col-md-11'>
                <Skeleton active paragraph={{ rows: 3 }} />
              </div>
              <div className='col-12 col-md-11'>
                <Skeleton active paragraph={{ rows: 3 }} />
              </div>
            </>
          ) : (
            questions.map((item) => (
              <div className='col-12 col-md-11' key={item.id}>
                <AdminOldQuestionCard
                  question={item}
                  delete_old={delete_question}
                />
              </div>
            ))
          )}
        </div>
        <div className='justify-content-center d-flex mt-5'>
          <Pagination
            total={total}
            defaultCurrent={1}
            current={pageNumber}
            onChange={changePage}
            hideOnSinglePage={true}
            pageSize={12}
            showSizeChanger={false}
          />
        </div>
      </div>
      <Modal
        visible={modal}
        centered={true}
        closable={true}
        footer={null}
        onCancel={() => {
          add_modal();
        }}
        zIndex={1050}
        width={500}
      >
        <AddOldQuestionComponent
          action={add_question}
          formreset={formreset}
          add_btn={add_btn}
        />
      </Modal>
    </>
  );
};

const mapStateToProps = (state) => ({
  user: state.auth.user,
  modal: state.question.modal,
  questions: state.question.questions,
  add_btn: state.question.add_btn,
  loading: state.question.loading,
  formreset: state.question.formreset,
  total: state.question.total,
  pageNumber: state.question.pageNumber,
});
const mapDispatchToProps = {
  add_modal: add_question_modal,
  add_question,
  fetch_admin,
  delete_question,
};
export default connect(mapStateToProps, mapDispatchToProps)(AdminOldQuestion);
