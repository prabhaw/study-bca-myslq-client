import React from "react";
import { MdDelete } from "react-icons/md";
import { DeleteOutlined } from "@ant-design/icons";
import { Modal, Divider } from "antd";
const { confirm } = Modal;
const OLD_URL = process.env.REACT_APP_QUESTION_FILE_URL;
const AdminOldQuestionCard = (props) => {
  const { question, delete_old } = props;
  const showDeleteConfirm = () => {
    confirm({
      title: "Are you sure delete this Post?",
      icon: <DeleteOutlined style={{ color: "red" }} />,
      content: "The post will no longer available to this site.",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      zIndex: "1500",
      onOk() {
        delete_old(question.id);
      },
    });
  };
  const dowload_old = () => {
    const url = `${OLD_URL}/${question.old_question}`;
    window.open(url, "_blank");
  };
  return (
    <>
      <div className='list-group border-0  '>
        <div className='d-flex w-100 justify-content-between'>
          <h4 className='mb-1 note-h-admin' onClick={dowload_old}>
            {question.subject}
          </h4>

          <MdDelete
            key='menu'
            style={{ fontSize: "20px", cursor: "pointer", color: "red" }}
            onClick={showDeleteConfirm}
          />
        </div>
        <p className='mb-1'>
          {question.university} &nbsp; &nbsp; {question.semester} &nbsp;{" "}
          {question.year} &nbsp; {question.exam_time}
        </p>
        <small>Publish At:&nbsp;{question.publish_date}</small>
      </div>
      <Divider />
    </>
  );
};

export default AdminOldQuestionCard;
