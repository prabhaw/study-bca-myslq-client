import React, { useState, useEffect } from "react";
import "./Blogcard.component.less";
import { Card, Avatar, List, Modal, Checkbox, Menu, Dropdown } from "antd";
import { DeleteOutlined, UserOutlined } from "@ant-design/icons";
import { MdDelete } from "react-icons/md";
import { FaBars, FaEdit } from "react-icons/fa";
import BlogForm from "../../Dashboard/Blog/BlogForm.component";

import {
  update_blog,
  update_blog_active,
  delete_blog,
} from "./../../../Redux/actions/blogs.action";
import { connect } from "react-redux";
import htmltoword from "react-html-parser";

const { Meta } = Card;
const { confirm } = Modal;
const User_IMG_URL = process.env.REACT_APP_USER_IMG_URL;
const BLOG_IMG_URL = process.env.REACT_APP_BLOG_IMG_URL;
const BlogCard = (props) => {
  const {
    user,
    blog,
    update_blog,

    delete_blog,
    update_blog_active,
    resetform,
  } = props;
  const [modal, setModal] = useState(false);
  const [readmore, serReadmore] = useState(false);
  useEffect(() => {
    if (resetform) {
      setModal(false);
    }
  }, [resetform]);
  const onChange = (e) => {
    const data = { verified: e.target.checked };
    update_blog_active(blog.id, data);
  };
  const udateBlog = (data) => {
    update_blog(blog.id, data);
  };
  const showDeleteConfirm = () => {
    confirm({
      title: "Are you sure delete this Post?",
      icon: <DeleteOutlined style={{ color: "red" }} />,
      content: "The post will no longer available to this site.",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      zIndex: "1500",
      onOk() {
        delete_blog(blog.id);
      },
    });
  };
  const role = () => {
    if (blog.user.role === 1 || blog.user.role === "1") {
      return "Admin";
    } else if (blog.user.role === 2 || blog.user.role === "2") {
      return "Teacher";
    } else {
      return "Student";
    }
  };
  const menu = (
    <Menu>
      <Menu.Item
        key='0'
        style={{ color: "blue" }}
        onClick={() => {
          setModal(true);
        }}
      >
        <FaEdit style={{ fontSize: "18px" }} />
        Edit Post
      </Menu.Item>
      <Menu.Item key='1' style={{ color: "red" }} onClick={showDeleteConfirm}>
        <MdDelete style={{ fontSize: "18px" }} />
        Delete Post
      </Menu.Item>

      {user.role === 1 ? (
        <Menu.Item key='3'>
          <Checkbox defaultChecked={blog.verified} onChange={onChange}>
            Active
          </Checkbox>
        </Menu.Item>
      ) : null}
    </Menu>
  );

  const profile = blog.user.profile_image ? (
    <Avatar size={64} src={`${User_IMG_URL}/${blog.user.profile_image}`} />
  ) : (
    <Avatar size={64} icon={<UserOutlined />} />
  );
  return (
    <>
      <div
        className='blog-card'
        style={
          blog.verified && blog.user.active ? null : { border: "2px solid red" }
        }
      >
        <div className='mb-3'>
          <List.Item
            actions={[
              <Dropdown
                overlay={menu}
                trigger={["click"]}
                placement='bottomRight'
              >
                <FaBars key='menu' style={{ fontSize: "20px" }} />
              </Dropdown>,
            ]}
          >
            <Meta
              avatar={profile}
              title={blog.user.first_name + " " + blog.user.last_name}
              description={role()}
            />
          </List.Item>
        </div>
        <Card
          hoverable={false}
          className='card-body'
          style={{ width: "100%", border: "none" }}
          cover={
            <img
              alt='blog_img'
              src={`${BLOG_IMG_URL}/${blog.blog_image}`}
              className='blog-image img-fluid'
            />
          }
        >
          <Meta
            title={blog.title}
            description={
              <>
                {!readmore ? (
                  <p className='blog-desc'>
                    {blog.short_description}...
                    <span
                      style={{
                        fontSize: "16px",
                        color: "#1DA57A",
                        cursor: "pointer",
                        fontWeight: "bold",
                      }}
                      onClick={() => {
                        serReadmore(true);
                      }}
                    >
                      Read More
                    </span>
                  </p>
                ) : (
                  <div className='blog-desc editor-post'>
                    {blog.short_description}
                    <br />
                    {htmltoword(blog.description)}......
                    <span
                      style={{
                        fontSize: "16px",
                        color: "#1DA57A",
                        cursor: "pointer",
                        fontWeight: "bold",
                      }}
                      onClick={() => {
                        serReadmore(false);
                      }}
                    >
                      show less
                    </span>
                  </div>
                )}
              </>
            }
          />
        </Card>
      </div>
      <Modal
        visible={modal}
        centered={true}
        closable={true}
        footer={null}
        onCancel={() => {
          setModal(false);
        }}
        zIndex={1050}
        width={900}
      >
        <BlogForm
          btnWord='Update Post'
          action={udateBlog}
          update={true}
          blog={blog}
          btnload={props.add_btn}
          resetimage={props.resetform}
        />
      </Modal>
    </>
  );
};
const mapStateToProps = (state) => ({
  user: state.auth.user,
  add_btn: state.blogs.add_btn,

  resetform: state.blogs.formreset,
});
const mapDispatchToProps = {
  update_blog,
  delete_blog,
  update_blog_active,
};
export default connect(mapStateToProps, mapDispatchToProps)(BlogCard);
