import React from "react";
import "./Footer.component.less";
import {
  FacebookOutlined,
  TwitterSquareFilled,
  GithubFilled,
  LinkedinFilled,
  MailOutlined,
  PhoneOutlined,
} from "@ant-design/icons";
const Footer = () => {
  return (
    <>
      <div className='footer'>
        <div className='container'>
          <div className='row justify-content-md-center'>
            <div className='col-12 col-sm-9'>
              <p>
                <strong>Follow Me:</strong>
              </p>
              <a
                href='https://www.facebook.com/prabhaw.soti.927'
                target='_blank'
                rel='noopener noreferrer'
              >
                <FacebookOutlined
                  style={{
                    margin: "0 7px 0 0",
                    fontSize: "30px",
                    color: "white",
                    cursor: "pointer",
                  }}
                />
              </a>
              <a
                href='https://twitter.com/prabhaw148'
                target='_blank'
                rel='noopener noreferrer'
              >
                <TwitterSquareFilled
                  style={{
                    margin: "0 7px 0 0",
                    fontSize: "30px",
                    color: "white",
                    cursor: "pointer",
                  }}
                />
              </a>
              <a
                href='https://github.com/prabhaw'
                target='_blank'
                rel='noopener noreferrer'
              >
                <GithubFilled
                  style={{
                    margin: "0 7px 0 0",
                    fontSize: "30px",
                    color: "white",
                    cursor: "pointer",
                  }}
                />
              </a>
              <a
                href='https://github.com/prabhaw'
                target='_blank'
                rel='noopener noreferrer'
              >
                <LinkedinFilled
                  style={{
                    margin: "0 7px 0 0",
                    fontSize: "30px",
                    color: "white",
                    cursor: "pointer",
                  }}
                />
              </a>
            </div>
            <div className='col-12 col-sm-3'>
              <strong>Contact:</strong>

              <p>
                <MailOutlined style={{ fontSize: "20px" }} />{" "}
                mail.prabahw@gmail.com
              </p>
              <p>
                <PhoneOutlined style={{ fontSize: "20px" }} /> +9779845697677
              </p>
            </div>
          </div>
          <div className='row copyrignt'>
            <div className='col-12'>
              <strong>
                Copyright &copy; 2020 StudyBCA All rights reserved
              </strong>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Footer;
