import React, { useEffect, useState } from "react";
import "./NavDashboard.component.less";
import { Menu, Avatar, Drawer, Modal } from "antd";
import { FiSettings, FiLogOut, FiLock } from "react-icons/fi";
import { FaBars } from "react-icons/fa";
import { UserOutlined, WarningOutlined } from "@ant-design/icons";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import { fetch_user } from "./../../../Redux/actions/user.action";
import { log_out } from "./../../../Redux/actions/login.action";
import Logo from "./../Logo/Logo.component";
import DashboardSideMenu from "../DashboardSideMenu/SideMenu.component";
import VerificationUser from "../VerificationUser/VerificationUser.component";
const IMG_URL = process.env.REACT_APP_USER_IMG_URL;

const { SubMenu } = Menu;

const NavDashboard = (props) => {
  const [visible, setVisible] = useState(false);
  const { user, fetch_user, location, log_out, history } = props;
  const [active, setActive] = useState(false);
  const onCLose = () => {
    setActive(!active);
  };
  useEffect(() => {
    fetch_user(history);
  }, [fetch_user, history]);

  useEffect(() => {
    if (user && user.active === false) {
      setActive(true);
    }
  }, [user]);

  const onOpen = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };
  const logOut = () => {
    log_out();
    history.push("/");
  };

  return (
    <>
      <div className='dash-header fixed-top'>
        <div className='menu'>
          <FaBars onClick={onOpen} style={{ marginTop: "7px" }} />
        </div>

        <Menu
          mode='horizontal'
          style={{ float: "right", border: "none" }}
          selectedKeys={[location.pathname]}
        >
          <Menu.Item key='/user/profile'>
            <Link to='/user/profile' style={{ textDecoration: "none" }}>
              {user && user.profile_image ? (
                <Avatar size={30} src={`${IMG_URL}/${user.profile_image}`} /> //okay
              ) : (
                <Avatar size={30} icon={<UserOutlined />} />
              )}
              <strong> {user.first_name}</strong>
            </Link>
          </Menu.Item>

          <SubMenu
            title={
              <span className='setting-menu'>
                <FiSettings />
                <strong>Settings </strong>
              </span>
            }
          >
            <Menu.Item
              key='/user/setting/change-password'
              className='settings-sub-menu'
            >
              <Link
                to='/user/setting/change-password'
                style={{ textDecoration: "none" }}
              >
                <FiLock /> Change Password
              </Link>
            </Menu.Item>
            <Menu.Item
              key='sign-out'
              className='settings-sub-menu'
              onClick={logOut}
            >
              <FiLogOut /> LogOut
            </Menu.Item>
          </SubMenu>
        </Menu>
      </div>
      <div style={{ marginTop: "50px" }}></div>
      <Drawer
        title={
          <div className='header-logo'>
            <Logo src={`${process.env.PUBLIC_URL}/img/logo.png`} />
          </div>
        }
        zIndex={1500}
        placement='left'
        closable={false}
        onClose={onClose}
        visible={visible}
        className={"dash-drawer"}
        width={225}
      >
        <DashboardSideMenu action={onClose} />
      </Drawer>
      {user.email ? (
        <>
          <Modal
            visible={!user.verify}
            closable={false}
            footer={false}
            zIndex={1500}
            style={{ padding: "0px" }}
          >
            <VerificationUser />
          </Modal>
          <Modal
            visible={active}
            closable={true}
            footer={false}
            zIndex={1500}
            style={{ padding: "0px" }}
            onCancel={onCLose}
          >
            <>
              <div
                className='alert alert-danger '
                style={{ textAlign: "center" }}
              >
                <WarningOutlined style={{ fontSize: "30px" }} />
                Your Accoutn is Suspended.
              </div>
              <div className='alert  alert-warning'>
                <h4>NOTES</h4>
                <ul>
                  <li>Hi &nbsp;{user.first_name}</li>
                  <li>
                    if you're seeing a message that your account is Suspended,
                    then we may have take it down from studyBCA because of
                    somethings saved to your account.
                  </li>
                  <li>
                    Your post will not publish if your account is Suspended.
                  </li>
                  <li>Please Change Your Setting and contact Us</li>
                  <li>contact: +977-9845697677</li>
                  <li>email: mail.prabhaw@gmail.com</li>
                </ul>
              </div>
            </>
          </Modal>
        </>
      ) : null}
    </>
  );
};

const mapStateToProps = (state) => ({
  user: state.auth.user,
});

const mapDispatchToProps = { fetch_user, log_out };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(NavDashboard));
