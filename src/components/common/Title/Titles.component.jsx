import React from "react";
import "./Title.component.less";
const Titles = (props) => (
  <>
    <div className='view-title'>
      <strong>{props.children}</strong>
    </div>
    <div className='title-margin'></div>
  </>
);

export default Titles;
