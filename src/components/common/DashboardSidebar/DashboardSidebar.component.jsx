import React from "react";
import "./DashboardSidebar.component.less";
import Logo from "./../Logo/Logo.component";
import { Layout } from "antd";
import DashboardSideMenu from "../DashboardSideMenu/SideMenu.component";
import { Link } from "react-router-dom";
const { Sider } = Layout;
const Sidebar = (props) => {
  return (
    <>
      <Sider
        className='sider'
        id='sider'
        width={225}
        style={{
          height: "100vh",
          background: "#ffff",
          overflowY: "auto",
          position: "fixed",
          zIndex: "1008",
          left: 0,
          borderRight: "1px solid #eee",
          overflowX: "hidden",
        }}
      >
        <Link to='/'>
          <div
            style={{
              border: "1px solid green",
              padding: "0px 5px 10px 5px",
              borderRadius: "10px",
              margin: "20px 10px",
            }}
          >
            <Logo
              src={`${process.env.PUBLIC_URL}/img/logo.png`}
              style={{ border: "1px solid green" }}
            />
          </div>
        </Link>
        <DashboardSideMenu />
      </Sider>
      <div className='sidebar-margin'></div>
    </>
  );
};

export default Sidebar;
