import React, { useEffect } from "react";

import { Result, Button } from "antd";
import { NavLink } from "react-router-dom";

const DashPageNotFound = (props) => {
  useEffect(() => {
    document.title = "Page-Not-Found";
  }, []);
  return (
    <>
      <Result
        status='404'
        title='404'
        subTitle='Sorry, the page you visited does not exist.'
        extra={
          <NavLink to='/user/dashboard'>
            <Button type='primary' danger>
              Back To Dashboard
            </Button>
          </NavLink>
        }
      />
    </>
  );
};

export default DashPageNotFound;
