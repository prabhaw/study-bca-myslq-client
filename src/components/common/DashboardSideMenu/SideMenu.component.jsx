import React from "react";
import "./SideMenu.component.less";
import { Menu } from "antd";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import { MdDashboard } from "react-icons/md";
import { IoIosPaper } from "react-icons/io";
import { GiBookCover } from "react-icons/gi";
import {
  FaUser,
  FaBlogger,
  FaClipboardList,
  FaCommentDots,
  FaBuilding,
  FaBook,
  FaRegImages,
} from "react-icons/fa";

const DashboardSideMenu = (props) => {
  const { location, user } = props;
  const handleClick = (e) => {
    if (props.action) {
      props.action();
    }
  };
  return (
    <>
      <Menu
        onClick={handleClick}
        style={{ width: 225 }}
        selectedKeys={[location.pathname]}
        mode='inline'
      >
        <Menu.Item
          key='/user/dashboard'
          icon={<MdDashboard style={{ fontSize: "20px" }} />}
        >
          &nbsp;
          <Link
            to='/user/dashboard'
            style={{ fontWeight: "bold", textDecoration: "none" }}
          >
            Dashboard
          </Link>
        </Menu.Item>
        {user && (user.role === 1 || user.role === 2) ? (
          <Menu.Item
            key='/user/aboutbca'
            icon={<FaCommentDots style={{ fontSize: "20px" }} />}
          >
            &nbsp;
            <Link
              style={{ fontWeight: "bold", textDecoration: "none" }}
              to='/user/aboutbca'
            >
              About BCA
            </Link>
          </Menu.Item>
        ) : (
          ""
        )}
        {user && user.role === 1 ? (
          <Menu.Item
            key='/user/admin-aboutbca'
            icon={<FaCommentDots style={{ fontSize: "20px" }} />}
          >
            &nbsp;
            <Link
              to='/user/admin-aboutbca'
              style={{ fontWeight: "bold", textDecoration: "none" }}
            >
              Admin AboutBCA
            </Link>
          </Menu.Item>
        ) : (
          ""
        )}

        <Menu.Item
          key='/user/articles'
          icon={<FaBlogger style={{ fontSize: "20px" }} />}
        >
          &nbsp;
          <Link
            to='/user/articles'
            style={{ fontWeight: "bold", textDecoration: "none" }}
          >
            Articles
          </Link>
        </Menu.Item>

        {user && user.role === 1 ? (
          <Menu.Item key='/user' icon={<FaUser style={{ fontSize: "20px" }} />}>
            &nbsp;
            <Link
              to='/user'
              style={{ fontWeight: "bold", textDecoration: "none" }}
            >
              User
            </Link>
          </Menu.Item>
        ) : (
          ""
        )}
        <Menu.Item
          key='/user/oldquestion'
          icon={<IoIosPaper style={{ fontSize: "20px" }} />}
        >
          <Link
            to='/user/oldquestion'
            style={{ fontWeight: "bold", textDecoration: "none" }}
          >
            &nbsp;OLD QUESRION
          </Link>
        </Menu.Item>

        <Menu.Item
          key='/user/notes'
          icon={<GiBookCover style={{ fontSize: "20px" }} />}
        >
          <Link
            to='/user/notes'
            style={{ fontWeight: "bold", textDecoration: "none" }}
          >
            &nbsp; NOTES
          </Link>
        </Menu.Item>
        {user && user.role === 1 ? (
          <Menu.Item
            key='/user/college'
            icon={<FaBuilding style={{ fontSize: "20px" }} />}
          >
            &nbsp;
            <Link
              to='/user/college'
              style={{ fontWeight: "bold", textDecoration: "none" }}
            >
              College
            </Link>
          </Menu.Item>
        ) : (
          ""
        )}
        {user && user.role === 1 ? (
          <Menu.Item
            key='/user/syllabus'
            icon={<FaClipboardList style={{ fontSize: "20px" }} />}
          >
            &nbsp;
            <Link
              to='/user/syllabus'
              style={{ fontWeight: "bold", textDecoration: "none" }}
            >
              Syllabus
            </Link>
          </Menu.Item>
        ) : (
          ""
        )}

        {user && user.role === 1 ? (
          <Menu.Item
            key='/user/book'
            icon={<FaBook style={{ fontSize: "20px" }} />}
          >
            <Link to='/user/book'>
              <span style={{ fontWeight: "bold", textDecoration: "none" }}>
                &nbsp;BCA BOOKS
              </span>
            </Link>
          </Menu.Item>
        ) : (
          ""
        )}

        {user && user.role === 1 ? (
          <Menu.Item
            key='/user/slider'
            icon={<FaRegImages style={{ fontSize: "20px" }} />}
          >
            <Link to='/user/slider'>
              <span style={{ fontWeight: "bold", textDecoration: "none" }}>
                &nbsp; Slider
              </span>
            </Link>
          </Menu.Item>
        ) : (
          ""
        )}
      </Menu>
    </>
  );
};
const mapStateToProps = (state) => ({
  user: state.auth.user,
});
export default connect(mapStateToProps)(withRouter(DashboardSideMenu));
