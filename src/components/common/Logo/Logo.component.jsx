import React from "react";
import "./Logo.component.less";

const Logo = (props) => {
  return (
    <>
      <img
        src={props.src}
        alt='Logo'
        style={{ height: `${props.height}px`, width: `${props.width}px` }}
        className='img-fluid'
      />
    </>
  );
};

export default Logo;
