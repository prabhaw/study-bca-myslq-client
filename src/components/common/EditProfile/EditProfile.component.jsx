import React, { useEffect, useState } from "react";
import "./EditProfile.component.less";
import { Form, Input, Button } from "antd";
import JoditEditor from "jodit-react";
const EditProfile = (props) => {
  const [data, setData] = useState({});
  const [form] = Form.useForm();
  const { user, action, btn } = props;
  const setRef = React.createRef();

  useEffect(() => {
    form.setFieldsValue({
      first_name: user.first_name,
      last_name: user.last_name,
      email: user.email,
      contact: user.contact,
      university: user.university,
      college: user.college,
    });
  }, [user, form]);
  const handleOnChange = (value) => {
    setData((pre) => ({ ...pre, description: value }));
  };
  const onFormChange = (value) => {
    setData((pre) => ({ ...pre, ...value }));
  };
  const onFinish = (value) => {
    // console.log(data);
    action(data);
  };
  return (
    <>
      <Form
        name='Profile_Edit'
        onFinish={onFinish}
        form={form}
        scrollToFirstError={true}
        onValuesChange={onFormChange}
      >
        <ul className='list-group list-group-flush detail-list border-0'>
          <li className='list-group-item pb-0'>
            <div className='row justify-content-start'>
              <div className='col-12 col-md-3 profile-text'>FirstName</div>
              <div className=' col-12 col-md-9 profile-text'>
                <Form.Item
                  name='first_name'
                  rules={[
                    {
                      required: true,
                      message: "Please input your First Name!",
                    },
                  ]}
                >
                  <Input size='large' placeholder='Enter Your First Name' />
                </Form.Item>
              </div>
            </div>
          </li>
          <li className='list-group-item pb-0 '>
            <div className='row justify-content-start'>
              <div className='col-12 col-md-3 profile-text'>LastName</div>
              <div className=' col-12 col-md-9 profile-text'>
                <Form.Item
                  name='last_name'
                  rules={[
                    {
                      required: true,
                      message: "Please input your Last Name!",
                    },
                  ]}
                >
                  <Input size='large' placeholder='Enter Your Last Name' />
                </Form.Item>
              </div>
            </div>
          </li>

          <li className='list-group-item pb-0'>
            <div className='row justify-content-start'>
              <div className='col-12 col-md-3 profile-text'>Email</div>
              <div className=' col-12 col-md-9 profile-text'>
                <Form.Item
                  name='email'
                  rules={[
                    {
                      type: "email",
                      message: "The input is not valid E-mail!",
                    },
                    {
                      required: true,
                      message: "Please input your Email!",
                    },
                  ]}
                >
                  <Input size='large' placeholder='Enter Your Email' />
                </Form.Item>
              </div>
            </div>
          </li>
          <li className='list-group-item pb-0 '>
            <div className='row justify-content-start'>
              <div className='col-12 col-md-3 profile-text'>Contact </div>
              <div className=' col-12 col-md-9 profile-text'>
                <Form.Item name='contact'>
                  <Input size='large' placeholder='Enter Your Contact Number' />
                </Form.Item>
              </div>
            </div>
          </li>
          <li className='list-group-item pb-0'>
            <div className='row justify-content-start'>
              <div className='col-12 col-md-3 profile-text'>University</div>
              <div className=' col-12 col-md-9 profile-text'>
                <Form.Item name='university'>
                  <Input size='large' placeholder='Enter Your University' />
                </Form.Item>
              </div>
            </div>
          </li>
          <li className='list-group-item pb-0 '>
            <div className='row justify-content-start'>
              <div className='col-12 col-md-3 profile-text'>College</div>
              <div className=' col-12 col-md-9 profile-text'>
                <Form.Item name='college'>
                  <Input size='large' placeholder='Enter Your College Name' />
                </Form.Item>
              </div>
            </div>
          </li>
          <li className='list-group-item '>
            <div className='row justify-content-start'>
              <div className='col-12 col-md-3 profile-text'>About You</div>
              <div className=' col-12 col-md-9 profile-text'>
                <JoditEditor
                  editorRef={setRef}
                  value={user.description ? user.description : ""}
                  tabIndex={1}
                  name='description'
                  onChange={handleOnChange}
                />
              </div>
            </div>
          </li>

          <li className='list-group-item pb-0 '>
            <div className='row justify-content-start'>
              <div className='col-12 col-md-3 profile-text'>Your Password</div>
              <div className=' col-12 col-md-9 profile-text'>
                <Form.Item
                  name='password'
                  rules={[
                    {
                      required: true,
                      message: "Please input your Password to Edit!",
                    },
                  ]}
                >
                  <Input
                    type='password'
                    size='large'
                    placeholder='Enter Your Password'
                  />
                </Form.Item>
              </div>
            </div>
          </li>
          <li className='list-group-item pb-0'>
            <Form.Item>
              <div className='float-right '>
                <Button
                  type='primary'
                  htmlType='submit'
                  loading={btn}
                  className='mr-2'
                >
                  Submit
                </Button>
                <Button type='primary' danger onClick={props.oncancle}>
                  Cancel
                </Button>
              </div>
            </Form.Item>
          </li>
        </ul>
      </Form>
    </>
  );
};

export default EditProfile;
