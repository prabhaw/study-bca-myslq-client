import React from "react";
import { Popconfirm, Divider } from "antd";
import { MdDelete } from "react-icons/md";
const BOOK_URL = process.env.REACT_APP_BOOKS_FILE_URL;
const BookCard = (props) => {
  const { book, onDelete } = props;
  const confirm = () => {
    onDelete(book.id);
  };
  return (
    <>
      <div className='card  border-0 '>
        <div className='row no-gutters'>
          <div className='col-md-4'>
            <img
              src={`${BOOK_URL}/${book.book_img}`}
              alt='book_image'
              className='img-fluid'
              style={{ maxHeight: "360px", minHeight: "360px", width: "100%" }}
            />
          </div>
          <div className='col-md-8'>
            <div className='card-body'>
              <ul
                className='list-group list-group-flush'
                style={{ height: "auto" }}
              >
                <li className='list-group-item'>
                  <h6>
                    <strong>{book.subject}</strong>
                  </h6>
                </li>
                <li className='list-group-item'>
                  <strong>{book.name}</strong>
                </li>
                <li className='list-group-item '>{book.university}</li>
                <li className='list-group-item'>{book.semester}</li>
                <li className='list-group-item'>Author:&nbsp;{book.author}</li>
                <li className='list-group-item'>
                  Publication:&nbsp;{book.publication}
                </li>
              </ul>

              {props.admin ? (
                <div
                  className='card-footer d-flex justify-content-center bg-transparent mt-3 '

                  // style={{ position: "absolute", bottom: "0", width: "100%" }}
                >
                  <Popconfirm
                    placement='topRight'
                    title={"Do You Want To Delete Book?"}
                    onConfirm={confirm}
                    okText='Yes'
                    cancelText='No'
                  >
                    <MdDelete
                      style={{
                        fontSize: "24px",
                        color: "red",
                        cursor: "pointer",
                      }}
                    />
                  </Popconfirm>
                </div>
              ) : null}
            </div>
          </div>
        </div>
      </div>
      <Divider />
    </>
  );
};

export default BookCard;
