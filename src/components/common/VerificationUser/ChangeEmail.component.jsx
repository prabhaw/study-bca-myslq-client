import React from "react";
import { Form, Input, Button } from "antd";
import { MailOutlined, LockOutlined } from "@ant-design/icons";
import { connect } from "react-redux";
import { update_user } from "./../../../Redux/actions/user.action";

const ChangeEmail = (props) => {
  const onFinish = (data) => {
    props.update_user(data);
  };
  return (
    <>
      <Form name='changeEmail' onFinish={onFinish}>
        <Form.Item
          name='email'
          rules={[
            {
              type: "email",
              message: "The input is not valid E-mail!",
            },
            {
              required: true,
              message: "Please input your E-mail!",
            },
          ]}
        >
          <Input
            prefix={<MailOutlined className='site-form-item-icon' />}
            placeholder='Email'
          />
        </Form.Item>
        <Form.Item
          name='password'
          rules={[{ required: true, message: "Please input your Password!" }]}
        >
          <Input
            prefix={<LockOutlined className='site-form-item-icon' />}
            type='password'
            placeholder='Your Password'
          />
        </Form.Item>
        <Form.Item>
          <Button
            type='primary'
            htmlType='submit'
            className='login-form-button'
            loading={props.loding_btn}
            block
          >
            Change Email
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};
const mapStateToProps = (state) => ({
  loding_btn: state.editprofile.update_btn,
});
const mapDispatchToProps = { update_user };
export default connect(mapStateToProps, mapDispatchToProps)(ChangeEmail);
