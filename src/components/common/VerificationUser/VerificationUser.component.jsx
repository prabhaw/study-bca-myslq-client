import React, { useState } from "react";
import { Form, Input, Button, Divider } from "antd";
import { WarningOutlined } from "@ant-design/icons";
import ChangeEmail from "./ChangeEmail.component";
import { connect } from "react-redux";
import {
  send_mail,
  vefify_code,
} from "./../../../Redux/actions/verifyemail.action";
const VerificationUser = (props) => {
  const [changemail, setChangeMail] = useState(false);
  const onFinish = (value) => {
    props.vefify_code(value);
  };
  const verifyInput = props.send ? (
    <>
      <div className='row justify-content-center'>
        <p style={{ color: "red" }}>Code is valid onley for 30 Second</p>
        <Form name='verify_email' layout='inline' onFinish={onFinish}>
          <Form.Item
            name='verifycode'
            rules={[{ required: true, message: "Enter Code Here" }]}
          >
            <Input placeholder='Enter Verification Code' />
          </Form.Item>

          <Form.Item shouldUpdate={true}>
            <Button type='primary' htmlType='submit' loading={props.verify_btn}>
              Verify
            </Button>
          </Form.Item>
        </Form>
      </div>
      <div className='row justify-content-center' style={{ marginTop: "5px" }}>
        <span style={{ color: "#1DA57A" }}>
          {"Click to Send Verification Code again."}
        </span>
      </div>
      <div className='row justify-content-center' style={{ marginTop: "5px" }}>
        <Button
          type='primary'
          danger
          loading={props.send_btn}
          onClick={() => {
            props.send_mail();
          }}
        >
          Send Verification Code
        </Button>
      </div>
    </>
  ) : (
    <>
      <div className='row justify-content-center'>
        <h6>Click to send Verification code.</h6>
      </div>
      <div className='row justify-content-center'>
        <Button
          type='primary'
          danger
          loading={props.send_btn}
          onClick={() => {
            props.send_mail();
          }}
        >
          Send Verification Code
        </Button>
      </div>
      <Divider />
      <div className='row justify-content-center'>
        {changemail ? (
          <>
            <ChangeEmail />
          </>
        ) : (
          <span
            onClick={() => {
              setChangeMail(true);
            }}
            style={{ color: "blue", cursor: "pointer" }}
          >
            Change Email
          </span>
        )}
      </div>
    </>
  );

  return (
    <>
      <div className='alert alert-warning ' style={{ textAlign: "center" }}>
        <WarningOutlined style={{ fontSize: "30px" }} /> Your email address is
        not verified
      </div>
      <div className='alert alert-info' style={{ marginBottom: "0px" }}>
        <h4>NOTES</h4>
        <ul>
          <li>
            You will recieve a verification code on your mail after you click
            send verification code. Enter that code below.
          </li>
        </ul>
        {verifyInput}
      </div>
    </>
  );
};
const mapStateToProps = (state) => ({
  send: state.verifyemail.verify_code,
  send_btn: state.verifyemail.sendign_btn,
  verify_btn: state.verifyemail.verify_btn,
});
const mapDispatchToProps = { send_mail, vefify_code };
export default connect(mapStateToProps, mapDispatchToProps)(VerificationUser);
