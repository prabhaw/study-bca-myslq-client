import React from "react";
import "./UserRegisterForm.component.less";
import { Form, Input, Button } from "antd";
import { LockOutlined, MailOutlined } from "@ant-design/icons";
const UserRegisterForm = (props) => {
  return (
    <>
      <Form
        name='normal_login'
        className=''
        initialValues={{
          remember: true,
        }}
        onFinish={(value) => {
          props.action(value);
        }}
      >
        <label htmlFor='first_name'>
          <span style={{ color: "red" }}>*</span>First Name
        </label>
        <Form.Item
          name='first_name'
          rules={[
            {
              required: true,
              message: "Please input your First Name!",
            },
          ]}
        >
          <Input placeholder='First Name' size='large' />
        </Form.Item>
        <label htmlFor='last_name'>
          <span style={{ color: "red" }}>*</span>Last Name
        </label>
        <Form.Item
          name='last_name'
          rules={[
            {
              required: true,
              message: "Please input your Last Name!",
            },
          ]}
        >
          <Input placeholder='Last Name' size='large' />
        </Form.Item>

        <label htmlFor='email'>
          <span style={{ color: "red" }}>*</span>Email
        </label>
        <Form.Item
          name='email'
          rules={[
            {
              required: true,
              message: "Please input your Email!",
            },
            { type: "email" },
          ]}
        >
          <Input
            prefix={<MailOutlined className='site-form-item-icon' />}
            placeholder='Email'
            size='large'
          />
        </Form.Item>

        <label htmlFor='password'>
          <span style={{ color: "red" }}>*</span> Password
        </label>

        <Form.Item
          name='password'
          rules={[
            {
              required: true,
              message: "Please input your New Password!",
            },
            {
              pattern: "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z@$&!\\d]{8,32}$",

              message: "Password must be 8 character long and One Number!",
            },
          ]}
          hasFeedback
        >
          <Input.Password
            prefix={<LockOutlined className='site-form-item-icon' />}
            type='password'
            placeholder='New Password'
            size='large'
          />
        </Form.Item>

        <label htmlFor='varifypassword'>
          <span style={{ color: "red" }}>*</span>Confirm Password
        </label>
        <Form.Item
          name='varifypassword'
          rules={[
            {
              required: true,
              message: "Please input your Confirm Password!",
            },

            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }
                return Promise.reject("Password not match!");
              },
            }),
          ]}
          hasFeedback
        >
          <Input.Password
            prefix={<LockOutlined className='site-form-item-icon' />}
            type='password'
            placeholder='Confirm Password'
            size='large'
          />
        </Form.Item>

        <Form.Item>
          <Button
            type='primary'
            htmlType='submit'
            block
            loading={props.btn_load}
            className='login-form-button'
            size='large'
          >
            Sign Up
          </Button>
        </Form.Item>
      </Form>
      <Button
        type='ghost'
        block
        size='large'
        onClick={() => {
          props.loginform();
        }}
      >
        Log In
      </Button>
    </>
  );
};

export default UserRegisterForm;
