import React, { useState, useEffect } from "react";
import { Form, Input, Button } from "antd";
import "./ForgetPassword.component.less";
import { MailOutlined, WarningOutlined } from "@ant-design/icons";
import { send_reset_email } from "./../../../Redux/actions/resetpassword.action";
import { connect } from "react-redux";

const ForgetPassword = (props) => {
  const [form] = Form.useForm();
  const [, Update] = useState();

  useEffect(() => {
    Update({});
  }, []);
  useEffect(() => {
    document.title = "Forget-Password.";
  }, []);
  const onFinish = (values) => {
    props.send_reset_email(values, props.history);
  };

  const forgotPass = (
    <Form form={form} onFinish={onFinish}>
      <div className='row justify-content-center'>
        <div className='col-11 col-md-8'>
          <label htmlFor='email'>Please Enter and Verify Your Email.</label>

          <Form.Item
            name='email'
            rules={[
              {
                type: "email",
                message: "The input is not valid E-mail!",
              },
              {
                required: true,
                message: "Please input your Email!",
              },
            ]}
          >
            <Input
              size='large'
              prefix={<MailOutlined className='site-form-item-icon' />}
              placeholder='Enter your Email'
            />
          </Form.Item>
        </div>
      </div>

      <div className='row justify-content-center'>
        <div className='col-11 col-md-8'>
          <Form.Item>
            <Button
              block
              type='primary'
              danger
              htmlType='submit'
              loading={props.lode_btn}
            >
              Send Verification
            </Button>
          </Form.Item>
        </div>
      </div>
    </Form>
  );

  return (
    <>
      <div
        className='container'
        style={{ marginTop: "20px", marginBottom: "20px" }}
      >
        <div className='justify-content-center row '>
          <div className='col-11 col-md-6'>
            <div
              className='alert alert-warning '
              style={{ textAlign: "center" }}
            >
              <WarningOutlined style={{ fontSize: "30px" }} /> We heard that you
              want to reset your password
            </div>
            <div className='alert alert-info'>
              <h4>NOTES</h4>
              <ul>
                <li>
                  You will recieve a verification link on your mail after you
                  click Send verification. You will reach to password reset
                  page.
                </li>
              </ul>
              {forgotPass}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  lode_btn: state.resetpass.sendemail_btn,
});

const mapDispatchToProps = {
  send_reset_email,
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgetPassword);
