import React, { useState, useEffect } from "react";
import { Form, Input, Button } from "antd";
import { reset_password } from "./../../../Redux/actions/resetpassword.action";
import { LockOutlined } from "@ant-design/icons";
import { WarningOutlined } from "@ant-design/icons";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

const ResetPassword = (props) => {
  const [form] = Form.useForm();
  const [, forceUpdate] = useState();
  useEffect(() => {
    forceUpdate({});
  }, []);
  useEffect(() => {
    document.title = "Reset-Password.";
  }, []);
  const token = props.match.params.token;
  const onFinish = (values) => {
    props.reset_password(values, token, props.history);
  };

  return (
    <>
      <div
        style={{
          marginTop: "20px",
        }}
        className='container'
      >
        <div className='justify-content-center row'>
          <div className='col-12 col-md-6'>
            <div
              className='alert alert-warning '
              style={{ textAlign: "center" }}
            >
              <WarningOutlined style={{ fontSize: "30px" }} /> We heard that you
              want to reset your password
            </div>
            <div className='alert alert-info '>
              <div className='row justify-content-center '>
                <div className=' col-12 col-md-8 form-col '>
                  <div className=' login-form'>
                    <h4>Enter Your New Password</h4>
                    <Form
                      form={form}
                      name='normal_login'
                      initialValues={{
                        remember: true,
                      }}
                      onFinish={onFinish}
                    >
                      <label htmlFor='varifypassword'>
                        <span style={{ color: "red" }}>*</span>New Password
                      </label>
                      <Form.Item
                        name='newpassword'
                        rules={[
                          {
                            required: true,
                            message: "Please input your New Password!",
                          },
                          {
                            pattern:
                              "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z@$&!\\d]{8,32}$",

                            message:
                              "Password must be 8 character long and One Number!",
                          },
                          // { validator: validatePassword },
                        ]}
                        hasFeedback
                      >
                        <Input.Password
                          prefix={
                            <LockOutlined className='site-form-item-icon' />
                          }
                          type='password'
                          placeholder='New Password'
                          size='large'
                        />
                      </Form.Item>

                      <label htmlFor='varifypassword'>
                        <span style={{ color: "red" }}>*</span>Confirm Password
                      </label>
                      <Form.Item
                        name='varifypassword'
                        rules={[
                          {
                            required: true,
                            message: "Please input your Confirm Password!",
                          },

                          ({ getFieldValue }) => ({
                            validator(rule, value) {
                              if (
                                !value ||
                                getFieldValue("newpassword") === value
                              ) {
                                return Promise.resolve();
                              }
                              return Promise.reject("Password not match!");
                            },
                          }),
                        ]}
                        hasFeedback
                      >
                        <Input.Password
                          prefix={
                            <LockOutlined className='site-form-item-icon' />
                          }
                          type='password'
                          placeholder='Confirm Password'
                          size='large'
                        />
                      </Form.Item>
                      <Form.Item shouldUpdate={true}>
                        {() => (
                          <Button
                            type='primary'
                            htmlType='submit'
                            block
                            size='large'
                            loading={props.reset_btn}
                            className='login-form-button'
                            disabled={
                              !form.isFieldsTouched(true) ||
                              form
                                .getFieldsError()
                                .filter(({ errors }) => errors.length).length
                            }
                          >
                            Reset Password
                          </Button>
                        )}
                      </Form.Item>
                    </Form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  reset_btn: state.resetpass.reset_btn,
});
const mapDispatchToProps = { reset_password };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ResetPassword));
