import React from "react";
import "./BlogPageCard.component.less";

import { Link } from "react-router-dom/cjs/react-router-dom.min";
const BLOG_URL = process.env.REACT_APP_BLOG_IMG_URL;
const BlogPageCard = (props) => {
  const { blog } = props;
  return (
    <div className='card blods-card mb-3' style={{ border: "none" }}>
      <div className='row no-gutters'>
        <div className='col-md-4'>
          <img
            src={`${BLOG_URL}/${blog.blog_image}`}
            alt='...'
            style={{ minHeight: "200px", maxHeight: "200px", width: "100%" }}
            className='img-fluid'
          />
        </div>
        <div className='col-md-8'>
          <div className='card-body blog-cards '>
            <Link
              to={`/articles/${blog.id}`}
              style={{ textDecoration: "none" }}
            >
              <h5 className='card-title'>
                {blog.title && blog.title.length > 105 ? (
                  <span>{`${blog.title.substring(0, 105)}...`}</span>
                ) : (
                  <span>{blog.title}</span>
                )}
              </h5>
            </Link>
            <div className='card-text'>
              {blog.short_description && blog.short_description.length > 270 ? (
                <span>{`${blog.short_description.substring(
                  0,
                  270
                )}.....`}</span>
              ) : (
                <span>{blog.short_description}</span>
              )}
              {/* <div className='d-flex justify-content-end'>
                
                  <Button type='primary'>Read More</Button>
                
              </div> */}
            </div>
          </div>
          <div
            className='card-footer footer-card'
            style={{ position: "absolute", bottom: "0", width: "100%" }}
          >
            <small className='text-muted'>
              Publish At: {blog.publish_date}
            </small>
            &nbsp; &nbsp; &nbsp;
            <small className='text-muted'>
              By: {blog.user.first_name}&nbsp;{blog.user.last_name}
            </small>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BlogPageCard;
