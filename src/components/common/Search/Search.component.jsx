import React from "react";

import { Input } from "antd";

const { Search } = Input;
const SearchInput = (props) => {
  const SearchData = (value) => {
    props.action(value);
  };

  return (
    <>
      <Search
        placeholder='Input search text'
        onSearch={SearchData}
        size='large'
      />
    </>
  );
};

export default SearchInput;
