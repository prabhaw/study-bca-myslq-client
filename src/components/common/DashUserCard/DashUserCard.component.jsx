import React, { useState } from "react";
import { Card, Modal, Avatar, Popconfirm, Checkbox } from "antd";
import { FaEye, FaUserEdit } from "react-icons/fa";
import CreateUserForm from "../../Pages/DashUser/CreateUserForm.component";
import ProfileDetail from "../ProfileDetails/ProfileDetails.component";
import { UserOutlined } from "@ant-design/icons";
import {
  user_udate,
  update_model_fun,
} from "./../../../Redux/actions/users.action";
import { connect } from "react-redux";
const USER_IMG = process.env.REACT_APP_USER_IMG_URL;
const { Meta } = Card;
const DashUserCard = (props) => {
  const [modal, setModal] = useState(false);
  const [modalup, setModalup] = useState(false);

  const { user, user_udate, update_modal, update_model_fun } = props;
  const confirm = () => {
    const data = { active: !user.active };
    user_udate(user.id, data);
  };
  const change_user = (data) => {
    user_udate(user.id, data);
  };
  const closeModal = () => {
    setModalup(false);
  };
  const modalUPOpen = () => {
    setModalup(true);
    update_model_fun();
  };
  return (
    <>
      <Card
        hoverable
        cover={
          <img
            alt='example'
            src={`${USER_IMG}/${user.profile_image}`}
            height='225'
          />
        }
        actions={[
          <FaEye
            key='view'
            style={{ fontSize: "20px" }}
            onClick={() => {
              setModal(true);
            }}
          />,
          <FaUserEdit
            key='Edit'
            style={{ fontSize: "20px" }}
            onClick={modalUPOpen}
          />,
          <Popconfirm
            placement='topRight'
            title={"Change Status?"}
            onConfirm={confirm}
            okText='Yes'
            cancelText='No'
          >
            <Checkbox checked={user.active}></Checkbox>
          </Popconfirm>,
        ]}
      >
        <Meta
          title={user.first_name + " " + user.last_name}
          description={user.email}
        />
      </Card>

      <Modal
        visible={modal}
        centered={true}
        closable={true}
        footer={null}
        onCancel={() => {
          setModal(false);
        }}
        //onCancel={this.handleCancel}
        zIndex={1050}
        width={1000}
      >
        <div className='profile'>
          <div className='row justify-content-center'>
            <div className='col-12 col-md-3 picture-section '>
              {user && user.profile_image ? (
                <Avatar
                  shape='square'
                  size={200}
                  src={`${USER_IMG}/${user.profile_image}`}
                />
              ) : (
                <Avatar
                  shape='square'
                  size={200}
                  icon={<UserOutlined style={{ fontSize: "200px" }} />}
                />
              )}
            </div>
            <div className='col-12 col-md-9 details-section'>
              <div className='card'>
                <div className='card-header bg-light'>
                  <span
                    style={{
                      float: "left",
                      fontSize: "24px",
                      fontWeight: "bold",
                      color: "#364f6b",
                    }}
                  >
                    Profile
                  </span>
                </div>

                <ProfileDetail user={user} />
              </div>
            </div>
          </div>
        </div>
      </Modal>

      <Modal
        visible={modalup}
        centered={true}
        closable={true}
        footer={null}
        // mask={false}
        onCancel={closeModal}
        zIndex={1050}
        width={400}
      >
        <CreateUserForm
          user={user}
          update={true}
          action={change_user}
          form_name={user.email}
          modal={update_modal}
          close={closeModal}
        />
      </Modal>
    </>
  );
};
const mapStateToProps = (state) => ({
  update_modal: state.users.update_modal,
});
const mapDispatchToProps = { user_udate, update_model_fun };
export default connect(mapStateToProps, mapDispatchToProps)(DashUserCard);
