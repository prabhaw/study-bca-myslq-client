import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { Drawer, Modal, Menu, Dropdown } from "antd";
import "./Navbar.component.less";
import { FiSettings } from "react-icons/fi";
import { MdDashboard } from "react-icons/md";

import NavMenu from "./Navmenu/Navmenu.component";
import { connect } from "react-redux";
import {
  login_modal,
  logIn_user,
  log_out,
  create_user,
  register_user,
} from "./../../../Redux/actions/login.action";
import {
  FaBars,
  FaEnvelope,
  FaPhone,
  FaSignInAlt,
  FaSignOutAlt,
} from "react-icons/fa";
import Loginform from "../LogInForm/Loginform.component";

import Logo from "../Logo/Logo.component";
import UserRegisterForm from "../UserRegisterForm/UserRegisterForm.component";

class Navbar extends Component {
  state = { visible: false, placement: "right" };

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };
  handleCancel = () => {
    this.props.login_modal();
  };
  login = (data) => {
    this.props.logIn_user(data, this.props.history);
  };

  singOut = (data) => {
    this.props.log_out();
    this.props.history.push("/");
  };
  onClose = () => {
    this.setState({
      visible: false,
    });
  };
  loginform = () => {
    this.props.create_user();
  };
  register_action = (data) => {
    this.props.register_user(data);
  };

  render() {
    const { placement, visible } = this.state;
    const { model, loggingIn, sign_in, signup_btn } = this.props;
    const menu = (
      <Menu>
        <Menu.Item>
          <Link to='/user/dashboard' style={{ textDecoration: "none" }}>
            <MdDashboard />
            &nbsp;Dashboard
          </Link>
        </Menu.Item>
        <Menu.Item onClick={this.singOut}>
          <FaSignOutAlt />
          <span>&nbsp;LogOut</span>
        </Menu.Item>
      </Menu>
    );
    return (
      <>
        <header id='header' className='header fixed-top'>
          <div className='header-top-wrapper'>
            <div className='container-fluid' style={{ padding: "0 5% 0 5%" }}>
              <div className='row'>
                <div className='ml-auto col-8'>
                  <ul className='d-flex flex-wrap list-unstyled justify-content-end aling-items-center tobar-item'>
                    <li>
                      <span
                        className='a'
                        onClick={() => {
                          window.location.href = `mailto:mail.studybca@gmail.com`;
                        }}
                      >
                        <FaEnvelope />
                        <span>&nbsp;mail.studybca@gmail.com</span>
                      </span>
                    </li>
                    <li>
                      <span
                        className='a'
                        onClick={() => {
                          window.location.href = `tel:+9779845697677`;
                        }}
                      >
                        <FaPhone />
                        <span>&nbsp;+977-9845697677</span>
                      </span>
                    </li>

                    <li className='top-word'>
                      {localStorage.getItem("token") ? (
                        <>
                          {/* <div onClick={this.singOut}>
                            <FaSignOutAlt />
                            <span>&nbsp;LogOut</span>
                          </div> */}
                          <Dropdown
                            overlay={menu}
                            trigger={["click"]}
                            placement='bottomLeft'
                          >
                            <span onClick={(e) => e.preventDefault()}>
                              <FiSettings /> Settings
                            </span>
                          </Dropdown>
                        </>
                      ) : (
                        <>
                          <div onClick={this.props.login_modal}>
                            <FaSignInAlt />
                            <span>&nbsp;LogIn</span>
                          </div>
                        </>
                      )}
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className='header-bottom-block'>
            <div
              className='container-fluid'
              style={{ padding: "5px 5% 5px 5%" }}
            >
              <div className='header-bottom-wrapper d-flex justify-content-between'>
                <div className='header-logo'>
                  <Link to='/'>
                    {/* <Logo src={`${process.env.PUBLIC_URL}/img/logo.png`} />
                     */}
                    <img
                      src={`${process.env.PUBLIC_URL}/img/logo.png`}
                      alt='Logo'
                      className='img-fluid nav-logo'
                    />
                  </Link>
                </div>

                <ul className='tab-item d-flex list-unstyled'>
                  <li>
                    <FaBars
                      style={{
                        fontSize: "30px",
                        color: "#ff304f",
                        cursor: "pointer",
                      }}
                      onClick={this.showDrawer}
                    />
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <Drawer
            title={
              <div className='header-logo'>
                <Logo src={`${process.env.PUBLIC_URL}/img/logo.png`} />
              </div>
            }
            zIndex={1500}
            placement={placement}
            closable={false}
            onClose={this.onClose}
            visible={visible}
            key={placement}
            className={"nav-drawer"}
          >
            <NavMenu onClose={this.onClose} onCancle={this.onClose} />
          </Drawer>
        </header>
        <div style={{ paddingTop: "80px" }}></div>
        <Modal
          visible={model}
          // visible={true}
          centered={true}
          closable={true}
          footer={null}
          onCancel={this.handleCancel}
          zIndex={1050}
          width={400}
        >
          {sign_in ? (
            <Loginform
              login={this.login}
              closeModel={this.handleCancel}
              loggingIn={loggingIn}
              loginform={this.loginform}
            />
          ) : (
            <UserRegisterForm
              register={this.register}
              loginform={this.loginform}
              btn_load={signup_btn}
              action={this.register_action}
            />
          )}
        </Modal>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  model: state.auth.logInmodal,
  loggingIn: state.auth.loggingIn,
  user: state.auth.user,
  sign_in: state.auth.sign_in,
  signup_btn: state.auth.signup_btn,
});

const mapDispatchToProps = {
  login_modal,
  logIn_user,
  log_out,
  create_user,
  register_user,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navbar));
