import React from "react";
import "./Navmenu.component.less";
import { Menu } from "antd";
import {
  FaHome,
  FaCommentDots,
  FaBuilding,
  FaBook,
  FaClipboardList,
  FaUser,
} from "react-icons/fa";
import { MdLineWeight } from "react-icons/md";
import { IoIosPaper } from "react-icons/io";
import { GiBookCover } from "react-icons/gi";
import { Link, withRouter } from "react-router-dom";

const Navmenu = (props) => {
  const { location } = props;
  return (
    <>
      <div className='nav-menu'>
        <Menu
          selectedKeys={[location.pathname]}
          mode='inline'
          onClick={props.onCancle}
        >
          <Menu.Item
            key='/'
            icon={<FaHome style={{ fontSize: "20px" }} />}
            onClick={props.onClose}
          >
            <Link to='/'>
              <span className='menu-word'>&nbsp;HOME</span>
            </Link>
          </Menu.Item>
          <Menu.Item
            key='/aboutbca'
            icon={<FaCommentDots style={{ fontSize: "20px" }} />}
          >
            <Link to='/aboutbca'>
              <span className='menu-word'>&nbsp;ABOUT BCA</span>
            </Link>
          </Menu.Item>
          <Menu.Item
            key='/college'
            icon={<FaBuilding style={{ fontSize: "20px" }} />}
          >
            <Link to='/college'>
              <span className='menu-word'>&nbsp;COLLEGES</span>
            </Link>
          </Menu.Item>
          <Menu.Item
            key='/questions'
            icon={<IoIosPaper style={{ fontSize: "20px" }} />}
          >
            <Link to='/questions'>
              <span className='menu-word'>&nbsp;BCA OLD QUESRION</span>
            </Link>
          </Menu.Item>
          <Menu.Item
            key='/syllabus'
            icon={<FaClipboardList style={{ fontSize: "20px" }} />}
          >
            <Link to='/syllabus'>
              <span className='menu-word'>&nbsp;BCA SYLLABUS</span>
            </Link>
          </Menu.Item>
          <Menu.Item
            key='/articles'
            icon={<MdLineWeight style={{ fontSize: "20px" }} />}
          >
            <Link to='/articles'>
              <span className='menu-word'>&nbsp;BCA ARTICLES</span>
            </Link>
          </Menu.Item>
          <Menu.Item
            key='/books'
            icon={<FaBook style={{ fontSize: "20px" }} />}
          >
            <Link to='/books'>
              {" "}
              <span className='menu-word'>&nbsp;BCA BOOKS</span>
            </Link>
          </Menu.Item>
          <Menu.Item
            key='/notes'
            icon={<GiBookCover style={{ fontSize: "20px" }} />}
          >
            <Link to='/notes'>
              <span className='menu-word'>&nbsp;BCA NOTES</span>
            </Link>
          </Menu.Item>
          <Menu.Item
            key='/professor'
            icon={<FaUser style={{ fontSize: "20px" }} />}
          >
            <Link to='/professor'>
              <span className='menu-word'>&nbsp;Our Professor</span>
            </Link>
          </Menu.Item>
          {/* <Menu.Item key='9' icon={<MdContacts style={{ fontSize: "20px" }} />}>
            <span className='menu-word'>&nbsp;CONTACT</span>
          </Menu.Item> */}
        </Menu>
      </div>
    </>
  );
};

export default withRouter(Navmenu);
