import React from "react";
import "./ProfileDetails.component.less";
import htmltoword from "react-html-parser";
const ProfileDetailList = (props) => {
  const { user } = props;

  const setRole = () => {
    if (user.role === "1" || user.role === 1) {
      return "Administrator";
    } else if (user.role === "2" || user.role === 2) {
      return "Teacher";
    } else {
      return "Student";
    }
  };
  return (
    <>
      <ul className='list-group list-group-flush detail-list border-0'>
        <li className='list-group-item '>
          <div className='row justify-content-start'>
            <div className='col-12 col-md-3 profile-text'>Name</div>
            <div className=' col-12 col-md-9 profile-text'>
              {user ? user.first_name + " " + user.last_name : ""}
            </div>
          </div>
        </li>

        <li className='list-group-item '>
          <div className='row justify-content-start'>
            <div className='col-12 col-md-3 profile-text'>Email</div>
            <div className=' col-12 col-md-9 profile-text'>
              {user ? user.email : ""}
            </div>
          </div>
        </li>
        <li className='list-group-item '>
          <div className='row justify-content-start'>
            <div className='col-12 col-md-3 profile-text'>Contact </div>
            <div className=' col-12 col-md-9 profile-text'>
              {user ? user.contact : ""}
            </div>
          </div>
        </li>
        <li className='list-group-item '>
          <div className='row justify-content-start'>
            <div className='col-12 col-md-3 profile-text'>Role</div>
            <div className=' col-12 col-md-9 profile-text'>{setRole()}</div>
          </div>
        </li>
        <li className='list-group-item '>
          <div className='row justify-content-start'>
            <div className='col-12 col-md-3 profile-text'>University</div>
            <div className=' col-12 col-md-9 profile-text'>
              {user ? user.university : ""}
            </div>
          </div>
        </li>
        <li className='list-group-item '>
          <div className='row justify-content-start'>
            <div className='col-12 col-md-3 profile-text'>College</div>
            <div className=' col-12 col-md-9 profile-text'>
              {user ? user.college : ""}
            </div>
          </div>
        </li>
        <li className='list-group-item '>
          <div className='row justify-content-start'>
            <div className=' col-12'>
              {user ? htmltoword(user.description) : ""}
            </div>
          </div>
        </li>
      </ul>
    </>
  );
};

export default ProfileDetailList;
