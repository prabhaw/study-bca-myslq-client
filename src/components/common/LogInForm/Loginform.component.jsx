import React, { useState } from "react";
import "./Loginform.component.less";
import { Form, Input, Button, Modal } from "antd";
import { Link } from "react-router-dom";
import { MailOutlined, LockOutlined } from "@ant-design/icons";
import Logo from "../Logo/Logo.component";
import { FaFacebookF, FaGoogle } from "react-icons/fa";
import { FacebookProvider, Login } from "react-facebook";
import notifications from "../../../utils/notifications";
import { GoogleLogin } from "react-google-login";
import { connect } from "react-redux";
import {
  password_modal,
  signup_social,
  password_modal_close,
} from "./../../../Redux/actions/socialaccoutn.action";

const Loginform = (props) => {
  const {
    social_modal,
    password_modal,
    signup_social,
    password_modal_close,
  } = props;
  const [data, setData] = useState({});
  const handleResponse = (data) => {
    if (data) {
      setData({ ...data.profile });
      password_modal(data.profile);
    }
  };
  const responseGoogle = (data) => {
    if (data) {
      password_modal(data.profileObj);
      setData({
        email: data.profileObj.email,
        first_name: data.profileObj.givenName,
        last_name: data.profileObj.familyName,
      });
    }
  };

  const handleCancel = () => {
    password_modal_close();
  };
  const handleError = (error) => {
    notifications.showError("Connection Fail.");
  };

  const setPasswordForsocial = (value) => {
    setData((pre) => ({ ...pre, ...value }));
  };
  const onFinish = (value) => {
    signup_social(data);
  };
  return (
    <>
      <div className='d-flex  justify-content-center'>
        <div
          style={{
            border: "1px solid green",
            padding: "0px 5px 10px 5px",
            borderRadius: "10px",
          }}
        >
          <Logo
            src={`${process.env.PUBLIC_URL}/img/logo.png`}
            width={200}
            style={{ border: "1px solid green" }}
          />
        </div>
      </div>
      <div
        style={{ marginTop: "24px", textAlign: "center" }}
        className='welcome-text'
      ></div>

      <Form
        name='normal_login'
        className=''
        onFinish={(value) => {
          props.login(value);
        }}
      >
        <Form.Item
          name='username'
          rules={[
            {
              required: true,
              message: "Please input your Username!",
            },
          ]}
        >
          <Input
            prefix={<MailOutlined className='site-form-item-icon' />}
            placeholder='Enter your Email'
            size='large'
          />
        </Form.Item>
        <Form.Item
          name='password'
          rules={[
            {
              required: true,
              message: "Please input your Password!",
            },
          ]}
        >
          <Input.Password
            prefix={<LockOutlined className='site-form-item-icon' />}
            type='password'
            placeholder='Password'
            size='large'
          />
        </Form.Item>

        <Form.Item>
          <Button
            type='primary'
            htmlType='submit'
            className='login-form-button'
            loading={props.loggingIn}
            block
            size='large'
          >
            Log In
          </Button>
        </Form.Item>
      </Form>

      <GoogleLogin
        clientId='771630552430-m2op15cop80j2tuulmp87342jg84le78.apps.googleusercontent.com'
        render={(renderProps) => (
          <Button
            type='ghost'
            block
            size='large'
            style={{ marginTop: "10px" }}
            onClick={renderProps.onClick}
            loading={renderProps.disabled}
          >
            <FaGoogle className='login-social-icon login-google' />
            &nbsp; Register with Google
          </Button>
        )}
        onSuccess={responseGoogle}
        cookiePolicy={"single_host_origin"}
      />

      <FacebookProvider appId='272857954095361'>
        <Login scope='email' onCompleted={handleResponse} onError={handleError}>
          {({ loading, handleClick }) => (
            <Button
              type='ghost'
              block
              size='large'
              style={{ marginTop: "10px" }}
              loading={loading}
              onClick={handleClick}
            >
              <FaFacebookF className='login-social-icon login-facebook' />
              &nbsp; Register with Facebook
            </Button>
          )}
        </Login>
      </FacebookProvider>

      <div className='row' style={{ marginTop: "10px" }}>
        <div className='col-6'>
          <Link
            to='/forgot-password'
            style={{ textDecoration: "none", color: "#1DA57A" }}
            onClick={() => {
              props.closeModel();
            }}
          >
            Forgot password ?
          </Link>
        </div>{" "}
        <div className='col-6'>
          <div className='d-flex flex-row-reverse '>
            <span
              style={{
                textDecoration: "none",
                color: "#1DA57A",
                cursor: "pointer",
                fontSize: "16px",
              }}
              onClick={() => {
                props.loginform();
              }}
            >
              Create Account
            </span>
          </div>
        </div>
      </div>

      <Modal
        visible={social_modal}
        centered={true}
        closable={true}
        footer={null}
        onCancel={handleCancel}
        zIndex={2000}
        width={400}
      >
        <h4>Enter Your New Password</h4>
        <Form
          name='normal_login'
          onValuesChange={setPasswordForsocial}
          onFinish={onFinish}
        >
          <label htmlFor='varifypassword'>
            <span style={{ color: "red" }}>*</span>New Password
          </label>
          <Form.Item
            name='newpassword'
            rules={[
              {
                required: true,
                message: "Please input your New Password!",
              },
              {
                pattern: "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z@$&!\\d]{8,32}$",

                message: "Password must be 8 character long and One Number!",
              },
              // { validator: validatePassword },
            ]}
            hasFeedback
          >
            <Input.Password
              prefix={<LockOutlined className='site-form-item-icon' />}
              type='password'
              placeholder='New Password'
              size='large'
            />
          </Form.Item>

          <label htmlFor='varifypassword'>
            <span style={{ color: "red" }}>*</span>Confirm Password
          </label>
          <Form.Item
            name='password'
            rules={[
              {
                required: true,
                message: "Please input your Confirm Password!",
              },

              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (!value || getFieldValue("newpassword") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject("Password not match!");
                },
              }),
            ]}
            hasFeedback
          >
            <Input.Password
              prefix={<LockOutlined className='site-form-item-icon' />}
              type='password'
              placeholder='Confirm Password'
              size='large'
            />
          </Form.Item>
          <Form.Item shouldUpdate={true}>
            <Button
              type='primary'
              htmlType='submit'
              block
              size='large'
              // loading={props.reset_btn}
            >
              Register
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
const mapStateToProps = (state) => ({
  social_modal: state.socialreg.password_modal,
  pass_btn: state.socialreg.password_btn,
});
const mapDispatchToProps = {
  password_modal,
  signup_social,
  password_modal_close,
};
export default connect(mapStateToProps, mapDispatchToProps)(Loginform);
