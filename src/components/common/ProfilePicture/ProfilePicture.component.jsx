import React from "react";
import "./ProfilePicture.component.less";
import { FaCamera } from "react-icons/fa";
import notifications from "../../../utils/notifications";
const IMG_URL = process.env.REACT_APP_USER_IMG_URL;
const ProfilePicture = (props) => {
  const { action, picture, pic_state } = props;
  const ImageUpload = (e) => {
    let { files, type } = e.target;

    const image = files;

    if (type === "file") {
      if (image[0]) {
        if (
          image[0].type === "image/jpeg" ||
          image[0].type === "image/png" ||
          image[0].type === "image/jpg"
        ) {
          if (image[0].size / 1024 / 1024 <= 2) {
            const formData = new FormData();
            formData.append("img", image[0], image[0].name);
            action(formData);
          } else {
            notifications.showError("Image Is More Then 2MB.");
          }
        } else {
          notifications.showError("File Format Onley JPG or PNG Is Accepted.");
        }
      }
    }
  };

  const images = picture ? (
    <img
      src={`${IMG_URL}/${picture}`}
      className=' img-fluid image rounded '
      alt='profile-pic'
    />
  ) : (
    <img
      src={`${process.env.PUBLIC_URL}/img/profile.PNG`}
      className='img-fluid image rounded '
      alt='profile-pic'
    />
  );
  return (
    <>
      <div className='picture-container'>
        {images}

        <div className='input-section'>
          <label className='overlay' htmlFor='file-input'>
            <FaCamera style={{ fontSize: "20px" }} /> Choose Picture
          </label>
          <input
            id='file-input'
            name='img'
            type='file'
            disabled={pic_state}
            onChange={ImageUpload}
          />
        </div>
      </div>
    </>
  );
};

export default React.memo(ProfilePicture);
