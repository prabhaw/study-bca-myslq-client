import React, { useEffect, useState } from "react";
import { Select, Form, Button } from "antd";
import { connect } from "react-redux";
import {
  syllabus_university,
  search_sem,
  search_subj,
} from "./../../../Redux/actions/search.action";
const { Option } = Select;

const SearchForm = (props) => {
  const {
    university,
    syllabus_university,
    semester,
    subject,
    search_sem,
    search_subj,
    action,
  } = props;
  const [data, setData] = useState();

  useEffect(() => {
    syllabus_university();
  }, [syllabus_university]);

  const university_data = university.map((item, i) => (
    <Option value={item} key={i}>
      {item}
    </Option>
  ));

  const semester_data = semester.map((item, i) => (
    <Option value={item} key={i}>
      {item}
    </Option>
  ));
  const subj_data = subject.map((item, i) => (
    <Option value={item} key={i}>
      {item}
    </Option>
  ));
  const onUniversityFormChange = (value) => {
    search_sem({ university: value });
    setData((pre) => ({ ...pre, university: value }));
  };
  const onSemSelect = (value) => {
    search_subj({ ...data, semester: value });
    setData((pre) => ({ ...pre, semester: value }));
  };
  const onFinish = (value) => {
    action(value);
  };

  return (
    <>
      <Form name='search_form' onFinish={onFinish}>
        <div className='row'>
          <div className='col-12 col-md-6'>
            <Form.Item name='university'>
              <Select
                placeholder='Select University'
                size='large'
                onChange={onUniversityFormChange}
              >
                {university_data}
              </Select>
            </Form.Item>
          </div>
          <div className='col-12 col-md-6'>
            <Form.Item name='semester'>
              <Select
                placeholder='Select Semester'
                size='large'
                onChange={onSemSelect}
              >
                {semester_data}
              </Select>
            </Form.Item>
          </div>
        </div>
        <div className='row'>
          <div className='col-12 '>
            <Form.Item name='subject'>
              <Select placeholder='Select Subject' size='large'>
                {subj_data}
              </Select>
            </Form.Item>
          </div>
        </div>
        <Form.Item>
          <Button type='primary' htmlType='submit' size='large' block>
            Search
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};

const mapStateToProps = (state) => ({
  university: state.search.search,
  semester: state.search.semester,
  subject: state.search.subject,
});
const mapDispatchToProps = { syllabus_university, search_sem, search_subj };
export default connect(mapStateToProps, mapDispatchToProps)(SearchForm);
