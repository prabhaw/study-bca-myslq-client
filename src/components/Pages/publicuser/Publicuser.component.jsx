import React, { useEffect } from "react";
import "./Publicuser.component.less";
import { Pagination, Skeleton } from "antd";
import Titles from "../../common/Title/Titles.component";
import PublicUserCard from "./PublicuserCard.component";
import { connect } from "react-redux";
import { fetch_public_users } from "./../../../Redux/actions/users.action";

const PublicUser = (props) => {
  const { fetch_public_users, users, total, fetching, pageNumber } = props;
  const changePage = (page, pageSize) => {
    fetch_public_users(page, pageSize);
  };
  useEffect(() => {
    fetch_public_users(1, 12);
    document.title = "Meet  Our Professors";
  }, [fetch_public_users]);
  const user_card = users.map((item, index) => (
    <div className='col-12 col-md-3 mb-4' key={item.id}>
      <PublicUserCard user={item} />
    </div>
  ));
  return (
    <>
      <div style={{ padding: "20px 5% 20px 5%" }}>
        <Titles>Meet our Professors</Titles>
        {fetching ? (
          <>
            <div className='row pt-5'>
              <div className='col-12 col-md-3'>
                <Skeleton active paragraph={{ rows: 6 }} />
              </div>
              <div className='col-12 col-md-3'>
                <Skeleton active paragraph={{ rows: 6 }} />
              </div>
              <div className='col-12 col-md-3'>
                <Skeleton active paragraph={{ rows: 6 }} />
              </div>
              <div className='col-12 col-md-3'>
                <Skeleton active paragraph={{ rows: 6 }} />
              </div>
            </div>
          </>
        ) : (
          <>
            <div className='pt-5 row '>{user_card}</div>
            <div className='justify-content-end d-flex'>
              <Pagination
                total={total}
                defaultCurrent={1}
                current={pageNumber}
                onChange={changePage}
                hideOnSinglePage={true}
                pageSize={12}
                showSizeChanger={false}
              />
            </div>
          </>
        )}
      </div>
    </>
  );
};
const mapStateToProps = (state) => ({
  users: state.users.public_user,
  total: state.users.public_total,
  fetching: state.users.public_fetch,
  pageNumber: state.users.public_pageNum,
});
const mapDispatchToProps = {
  fetch_public_users,
};
export default connect(mapStateToProps, mapDispatchToProps)(PublicUser);
