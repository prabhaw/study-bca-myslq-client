import React, { useState } from "react";
import { Card, Avatar, Modal } from "antd";
import { UserOutlined } from "@ant-design/icons";
import ProfileDetail from "./../../common/ProfileDetails/ProfileDetails.component";
const USER_IMG = process.env.REACT_APP_USER_IMG_URL;

const { Meta } = Card;

const PublicUserCard = (props) => {
  const [modal, setModal] = useState(false);
  const { user } = props;

  return (
    <>
      <Card
        hoverable
        onClick={() => {
          setModal(true);
        }}
        cover={
          <img
            alt='profile_pic'
            src={`${USER_IMG}/${user.profile_image}`}
            height='225'
          />
        }
      >
        <Meta
          title={`${user.first_name + " " + user.last_name}`}
          description={user.email}
        />
      </Card>

      <Modal
        visible={modal}
        centered={true}
        closable={true}
        footer={null}
        onCancel={() => {
          setModal(false);
        }}
        //onCancel={this.handleCancel}
        zIndex={1050}
        width={1000}
      >
        <div className='profile'>
          <div className='row justify-content-center'>
            <div className='col-12 col-md-3 picture-section '>
              {user && user.profile_image ? (
                <Avatar
                  shape='square'
                  size={200}
                  src={`${USER_IMG}/${user.profile_image}`}
                />
              ) : (
                <Avatar
                  shape='square'
                  size={200}
                  icon={<UserOutlined style={{ fontSize: "200px" }} />}
                />
              )}
            </div>
            <div className='col-12 col-md-9 details-section'>
              <div className='card'>
                <div className='card-header bg-light'>
                  <span
                    style={{
                      float: "left",
                      fontSize: "24px",
                      fontWeight: "bold",
                      color: "#364f6b",
                    }}
                  >
                    Profile
                  </span>
                </div>

                <ProfileDetail user={user} />
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </>
  );
};

export default PublicUserCard;
