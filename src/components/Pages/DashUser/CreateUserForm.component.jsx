import React, { useEffect } from "react";

import { Form, Input, Button, Select } from "antd";
import { LockOutlined, MailOutlined } from "@ant-design/icons";

const { Option } = Select;

const CreateUserForm = (props) => {
  const [form] = Form.useForm();
  const { update, modal, action, add_btn, formreset, user, close } = props;

  useEffect(() => {
    if (update) {
      form.setFieldsValue({
        first_name: user.first_name,
        last_name: user.last_name,
        email: user.email,
        role: `${user.role === 1 ? "Administrator" : user.role}`,
      });
    }
  }, [form, update, user]);
  useEffect(() => {
    if (modal) {
      close();
    }
  }, [modal, close]);
  useEffect(() => {
    if (formreset) {
      form.resetFields();
    }
  }, [formreset, form]);
  return (
    <>
      <Form
        name={props.form_name ? props.form_name : "Create_user"}
        form={form}
        initialValues={{
          remember: true,
        }}
        onFinish={(value) => {
          action(value);
        }}
      >
        <label htmlFor='first_name'>
          <span style={{ color: "red" }}>*</span>First Name
        </label>
        <Form.Item
          name='first_name'
          rules={[
            {
              required: true,
              message: "Please input your First Name!",
            },
          ]}
        >
          <Input
            placeholder='First Name'
            size='large'
            readOnly={props.update}
          />
        </Form.Item>
        <label htmlFor='last_name'>
          <span style={{ color: "red" }}>*</span>Last Name
        </label>
        <Form.Item
          name='last_name'
          rules={[
            {
              required: true,
              message: "Please input your Last Name!",
            },
          ]}
        >
          <Input placeholder='Last Name' size='large' readOnly={props.update} />
        </Form.Item>

        <label htmlFor='email'>
          <span style={{ color: "red" }}>*</span>Email
        </label>
        <Form.Item
          name='email'
          rules={[
            {
              required: true,
              message: "Please input your Email!",
            },
            { type: "email" },
          ]}
        >
          <Input
            prefix={<MailOutlined className='site-form-item-icon' />}
            placeholder='Email'
            size='large'
            readOnly={props.update}
          />
        </Form.Item>
        <label htmlFor='role'>
          <span style={{ color: "red" }}>*</span> Role for Application
        </label>
        <Form.Item
          name='role'
          rules={[
            { required: true, message: "Please Select Role For Application." },
          ]}
        >
          <Select
            placeholder='Select Role'
            // onChange={this.onGenderChange}
            size='large'
          >
            <Option value='2'>Teacher</Option>
            <Option value='0'>Student</Option>
          </Select>
        </Form.Item>
        {!props.update ? (
          <>
            <label htmlFor='password'>
              <span style={{ color: "red" }}>*</span> Password
            </label>
            <Form.Item
              name='password'
              rules={[
                {
                  required: true,
                  message: "Please input your New Password!",
                },
                {
                  pattern: "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z@$&!\\d]{8,32}$",

                  message: "Password must be 8 character long and One Number!",
                },
                // { validator: validatePassword },
              ]}
              hasFeedback
            >
              <Input.Password
                prefix={<LockOutlined className='site-form-item-icon' />}
                type='password'
                placeholder='New Password'
                size='large'
              />
            </Form.Item>
            <label htmlFor='varifypassword'>
              <span style={{ color: "red" }}>*</span>Confirm Password
            </label>
            <Form.Item
              name='varifypassword'
              rules={[
                {
                  required: true,
                  message: "Please input your Confirm Password!",
                },

                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (!value || getFieldValue("password") === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject("Password not match!");
                  },
                }),
              ]}
              hasFeedback
            >
              <Input.Password
                prefix={<LockOutlined className='site-form-item-icon' />}
                type='password'
                placeholder='Confirm Password'
                size='large'
              />
            </Form.Item>
          </>
        ) : null}

        <Form.Item>
          <Button
            type='primary'
            htmlType='submit'
            block
            loading={add_btn}
            className='login-form-button'
            size='large'
          >
            {props.update ? "Update User" : "Register User"}{" "}
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default React.memo(CreateUserForm);
