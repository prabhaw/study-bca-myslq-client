import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import "./Dashuser.component.less";
import { Input, Button, Divider, Pagination, Modal, Skeleton } from "antd";
import DashUserCard from "../../common/DashUserCard/DashUserCard.component";
import CreateUserForm from "./CreateUserForm.component";
import {
  fetch_users,
  add_user,
  add_modal_fun,
  search_users,
} from "./../../../Redux/actions/users.action";

const { Search } = Input;
const Dashuser = (props) => {
  const {
    user,
    history,
    fetch_users,
    fetching_user,
    users,
    total,
    pageNumber,
    register_user,
    add_modal,
    add_modal_fun,
    add_btn,
    formreset,
    search_users,
  } = props;
  const [searchs, setSearch] = useState();
  useEffect(() => {
    document.title = "User";
    if (user.role && user.role !== 1) {
      history.push("/user/dashboard");
    }
  }, [user, history]);
  useEffect(() => {
    fetch_users(1, 12);
  }, [fetch_users]);

  const changePage = (page, pageSize) => {
    if (searchs) {
      search_users(searchs, page, pageSize);
    } else {
      fetch_users(page, pageSize);
    }
  };
  const add_user = (value) => {
    register_user(value);
  };
  const search = (value) => {
    setSearch(value);
    search_users(value, 1, 12);
  };
  const user_card = users.map((item, index) => (
    <div className='col-12 col-md-3 mb-4' key={item.id}>
      <DashUserCard user={item} />
    </div>
  ));

  return (
    <>
      <div className='dash-user'>
        <div className='justify-content-center row search-section'>
          <div className='col-12 col-md-8'>
            <div className='row flex-column-reverse flex-md-row'>
              <div className='col-12 col-md-9 '>
                <Search
                  placeholder='Enter User Name or Email'
                  onSearch={search}
                  size='large'
                  style={{ width: "100%" }}
                />
              </div>
              <div className='col-12 col-md-3 btn-margin '>
                <Button
                  type='primary'
                  size='large'
                  block
                  onClick={() => {
                    add_modal_fun();
                  }}
                >
                  Add User
                </Button>
              </div>
            </div>
          </div>
        </div>
        <Divider />
        {fetching_user ? (
          <>
            <div className='row'>
              <div className='col-12 col-md-3'>
                <Skeleton active paragraph={{ rows: 6 }} />
              </div>
              <div className='col-12 col-md-3'>
                <Skeleton active paragraph={{ rows: 6 }} />
              </div>
              <div className='col-12 col-md-3'>
                <Skeleton active paragraph={{ rows: 6 }} />
              </div>
              <div className='col-12 col-md-3'>
                <Skeleton active paragraph={{ rows: 6 }} />
              </div>
            </div>
          </>
        ) : (
          <>
            <div className='row'>{user_card}</div>
            <div className='justify-content-end d-flex'>
              <Pagination
                total={total}
                defaultCurrent={1}
                current={pageNumber}
                onChange={changePage}
                hideOnSinglePage={true}
                pageSize={12}
                showSizeChanger={false}
              />
            </div>
          </>
        )}
      </div>

      <Modal
        visible={add_modal}
        centered={true}
        closable={true}
        footer={null}
        onCancel={() => {
          add_modal_fun();
        }}
        //onCancel={this.handleCancel}
        zIndex={1050}
        width={400}
      >
        <CreateUserForm
          action={add_user}
          add_btn={add_btn}
          formreset={formreset}
        />
      </Modal>
    </>
  );
};

const mapStateToProps = (state) => ({
  users: state.users.users,
  fetching_user: state.users.fetching_user,
  user: state.auth.user,
  total: state.users.total,
  pageNumber: state.users.pageNumber,
  add_modal: state.users.add_modal,
  add_btn: state.users.add_btn,
  formreset: state.users.formreset,
});
const mapDispatchToProps = {
  fetch_users,
  register_user: add_user,
  add_modal_fun,
  search_users,
};
export default connect(mapStateToProps, mapDispatchToProps)(Dashuser);
