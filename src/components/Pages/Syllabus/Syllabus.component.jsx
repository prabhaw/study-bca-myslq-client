import React, { useEffect, useState } from "react";
import "./Syllabus.component.less";
import SearchFormComponent from "../../common/SearchForm/SearchForm.component";
import SyllabusCard from "./SyllabusCard.component";
import { Skeleton, Pagination } from "antd";
import { connect } from "react-redux";
import {
  fetch_syllabus,
  search_syllabus,
} from "./../../../Redux/actions/syllabus.action";
import Titles from "./../../common/Title/Titles.component";

const Syllabus = (props) => {
  const {
    fetch_syllabus,
    loading,
    syllabus,
    total,
    pageNumber,
    search_syllabus,
  } = props;
  const [search, setSearch] = useState();

  useEffect(() => {
    fetch_syllabus(1, 12);
  }, [fetch_syllabus]);

  const changePage = (page, pageSize) => {
    if (search) {
      search_syllabus(search, page, pageSize);
    } else {
      fetch_syllabus(page, pageSize);
    }
  };
  const searchSyllabus = (value) => {
    search_syllabus(value, 1, 12);
    setSearch(value);
  };

  const data_syllbus = syllabus.map((item) => (
    <div className='col-12 col-md-3 mb-3' key={item.id}>
      <SyllabusCard syllabus={item} />
    </div>
  ));
  return (
    <>
      <div className='search-cover'>
        <div
          style={{
            padding: "0 5% 0 5%",
            paddingTop: "5vh",
            paddingBottom: "5vh",
          }}
        >
          <h4 className='search-text'>Search</h4>
          <SearchFormComponent action={searchSyllabus} />
        </div>
      </div>
      <div style={{ padding: "5px 5% 20px 5%", paddingTop: "7vh" }}>
        <Titles>Syllabus</Titles>
        <div className='row pt-5'>
          {loading ? (
            <>
              <div className='col-12 col-md-4 '>
                <Skeleton active paragraph={{ rows: 6 }} />
              </div>
              <div className='col-12 col-md-4 '>
                <Skeleton active paragraph={{ rows: 6 }} />
              </div>
              <div className='col-12 col-md-4 '>
                <Skeleton active paragraph={{ rows: 6 }} />
              </div>
            </>
          ) : (
            data_syllbus
          )}
        </div>
        <div className='justify-content-end d-flex mt-5'>
          <Pagination
            total={total}
            defaultCurrent={1}
            current={pageNumber}
            onChange={changePage}
            hideOnSinglePage={true}
            pageSize={12}
            showSizeChanger={false}
          />
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  loading: state.syllabus.loading,
  syllabus: state.syllabus.syllabus,
  total: state.syllabus.syllabus_total,
  pageNumber: state.syllabus.pageNumber,
});
const mapDispatchToProps = {
  fetch_syllabus,
  search_syllabus,
};
export default connect(mapStateToProps, mapDispatchToProps)(Syllabus);
