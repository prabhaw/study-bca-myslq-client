import React from "react";

import { Button } from "antd";
const FILE = process.env.REACT_APP_SYLLABUS_FILE_URL;
const SyllabusCard = (props) => {
  const { syllabus } = props;
  const dowload_syllabus = (file) => {
    const url = `${FILE}/${syllabus.syllabus}`;
    window.open(url, "_blank");
  };

  return (
    <div className='card ' style={{ minHeight: "300px", maxHeight: "300px" }}>
      <div className='card-body '>
        <h5 style={{ fontWeight: "bold" }}>{syllabus.subject}</h5>
        <h6 style={{ color: "gray" }}>
          <strong>{syllabus.university}</strong>
        </h6>
        <p style={{ marginBottom: "0px" }}>{syllabus.semester}</p>
        <p style={{ marginBottom: "0px" }}>
          Course Code: <strong>{syllabus.course_code}</strong>
        </p>
        <p>
          Credit Hours: <strong>{syllabus.credit_hrs}Hr</strong>
        </p>
      </div>
      <div className='card-footer'>
        <Button type='primary' block onClick={dowload_syllabus}>
          View Syllabus
        </Button>
      </div>
    </div>
  );
};

export default SyllabusCard;
