import React from "react";
import { Button } from "antd";
import { Link } from "react-router-dom";
const IMG_URL = process.env.REACT_APP_COLLEGE_IMG_URL;

const CollegeCard = (props) => {
  const { college } = props;
  return (
    <>
      <div className='card ' style={{ minHeight: "375px", maxHeight: "375px" }}>
        <img
          className='card-img-top'
          src={`${IMG_URL}/${college.college_image}`}
          alt='College_image'
          height={200}
          width={100}
        />
        <div className='card-body '>
          <h6 style={{ fontWeight: "bold" }}>
            {college.college_name.length > 60 ? (
              <span>{`${college.college_name.substring(0, 60)}...`}</span>
            ) : (
              <span>{college.college_name}</span>
            )}
          </h6>
          <p>
            {college.address.length > 50
              ? `${college.address.substring(0, 50)}...`
              : college.address}
          </p>
        </div>
        <div className='card-footer'>
          <Link to={`/college/${college.id}`}>
            <Button type='primary' block>
              View Details
            </Button>
          </Link>
        </div>
      </div>
    </>
  );
};

export default CollegeCard;
