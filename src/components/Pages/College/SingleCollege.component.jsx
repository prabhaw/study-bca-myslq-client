import React, { useEffect } from "react";
import Titles from "../../common/Title/Titles.component";
import { Skeleton } from "antd";
import { DiscussionEmbed } from "disqus-react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { singel_fetch } from "./../../../Redux/actions/college.action";
import { college_fetch } from "./../../../Redux/actions/home.action";
import htmltoword from "react-html-parser";
const IMG_URL = process.env.REACT_APP_COLLEGE_IMG_URL;

const SingleCollege = (props) => {
  const {
    singel_fetch,
    college,
    history,
    loading,
    college_fetch,
    topcolleges,
  } = props;
  const collegeid = props.match.params.id;
  useEffect(() => {
    singel_fetch(collegeid);
  }, [collegeid, singel_fetch]);
  useEffect(() => {
    college_fetch();
  }, [college_fetch]);
  useEffect(() => {
    if (college === false || college === null) {
      history.push("/college");
    }
  }, [college, history]);

  useEffect(() => {
    document.title = `Colleges-college Name`;
  }, []);

  const topcollege = topcolleges.slice(0, 5).map((item) => (
    <div key={item.id}>
      <Link to={`/college/${item.id}`} style={{ color: "gray" }}>
        <h6>{item.college_name}</h6>
      </Link>
      <hr />
    </div>
  ));

  return (
    <>
      <div style={{ padding: "20px 5% 20px 5%" }}>
        <div className='jumbotron' style={{ height: "170px" }}>
          <h5
            style={{
              textAlign: "center",
              fontWeight: "bold",
              fontSize: "24px",
            }}
          >
            Add Your ads Here
          </h5>
        </div>
        <div className='row'>
          {loading ? (
            <Skeleton active paragraph={{ rows: 10 }} />
          ) : (
            <>
              <div className='col-12 col-md-8'>
                <img
                  src={`${IMG_URL}/${college ? college.college_image : ""}`}
                  className='img-fluid'
                  style={{ width: "100%", height: "auto" }}
                  alt='blog_image'
                />

                <br />
                <br />
                <h4 className='blog-title'>
                  {college ? college.college_name : ""}&nbsp;(
                  {college ? college.university : ""})
                </h4>
                <p>{college ? college.address : ""}</p>
                {/* <h6>{blog ? blog.short_description : null}</h6> */}

                <div className='descriptio editor-post '>
                  {htmltoword(college ? college.description : "")}
                </div>
                <br />
                <DiscussionEmbed
                  shortname='studybca-com'
                  config={{
                    url: `https://studybca.com/college/${collegeid}`,
                    identifier: collegeid,
                    title: college ? college.college_name : "",
                  }}
                />
              </div>
              <div className='col-12 col-md-4  '>
                <Titles>More Colleges</Titles>
                <div className='pt-5'>
                  {props.toploading ? (
                    <Skeleton active paragraph={{ rows: 7 }} />
                  ) : (
                    topcollege
                  )}
                </div>
              </div>
            </>
          )}
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  loading: state.college.single_loading,
  college: state.college.college,
  topcolleges: state.home.colleges,
  toploading: state.home.loading,
});
const mapDispatchToProps = { singel_fetch, college_fetch };
export default connect(mapStateToProps, mapDispatchToProps)(SingleCollege);
