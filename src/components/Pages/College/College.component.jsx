import React, { useEffect, useState } from "react";
import { Input, Button, Divider, Pagination, Modal, Skeleton } from "antd";
import AddCollegeForm from "./AddCollegeForm.component";
import { connect } from "react-redux";
import {
  college_modal,
  add_college,
  public_college_fetch,
  public_search_college,
} from "./../../../Redux/actions/college.action";
import CollegeCard from "./CollegeCard.component";
import Titles from "./../../common/Title/Titles.component";

const { Search } = Input;

const College = (props) => {
  const {
    college_modal,
    modal,
    resetform,
    add_btn,
    add_college,
    public_college_fetch,
    loading,
    colleges,
    total,
    pageNumber,
    public_search_college,
  } = props;
  const [search, setSearch] = useState();
  useEffect(() => {
    document.title = "Colleges";
  }, []);
  useEffect(() => {
    public_college_fetch(1, 12);
  }, [public_college_fetch]);
  const actions = (data) => {
    add_college(data);
  };

  const changePage = (page, pageSize) => {
    if (search) {
      public_search_college(search, page, pageSize);
    } else {
      public_college_fetch(page, pageSize);
    }
  };
  const onSearch = (value) => {
    setSearch(value);
    public_search_college(value, 1, 12);
  };
  const college = colleges.map((item) => (
    <div className='col-12 col-md-4 mb-3' key={item.id}>
      <CollegeCard college={item} />
    </div>
  ));
  return (
    <>
      <div style={{ padding: "20px 5% 20px 5%" }}>
        <div className='justify-content-center row search-section'>
          <div className='col-12 col-md-8'>
            <div className='row flex-column-reverse flex-md-row'>
              <div className='col-12 col-md-9 '>
                <Search
                  placeholder='Enter College Details'
                  onSearch={onSearch}
                  size='large'
                  style={{ width: "100%" }}
                />
              </div>
              <div className='col-12 col-md-3 btn-margin '>
                <Button
                  type='primary'
                  size='large'
                  block
                  onClick={() => {
                    college_modal();
                  }}
                >
                  Add College
                </Button>
              </div>
            </div>
          </div>
        </div>
        <Divider></Divider>
        <Titles>{search ? "Search College" : "Colleges"}</Titles>
        <div className='row pt-5'>
          {/* ------------------- */}

          {loading ? (
            <>
              <div className='col-12 col-md-4 mb-3'>
                <Skeleton active paragraph={{ rows: 6 }} />{" "}
              </div>

              <div className='col-12 col-md-4 mb-3'>
                <Skeleton active paragraph={{ rows: 6 }} />{" "}
              </div>
              <div className='col-12 col-md-4 mb-3'>
                <Skeleton active paragraph={{ rows: 6 }} />{" "}
              </div>
            </>
          ) : (
            college
          )}
          {/* ------------------- */}
        </div>
        <div className='justify-content-end d-flex'>
          <Pagination
            total={total}
            defaultCurrent={1}
            current={pageNumber}
            onChange={changePage}
            hideOnSinglePage={true}
            pageSize={12}
            showSizeChanger={false}
          />
        </div>
      </div>

      <Modal
        visible={modal}
        centered={true}
        closable={true}
        footer={null}
        onCancel={() => {
          college_modal();
        }}
        zIndex={1050}
        width={900}
      >
        <AddCollegeForm
          action={actions}
          add_btn={add_btn}
          resetform={resetform}
        />
      </Modal>
    </>
  );
};
const mapStateToProps = (state) => ({
  modal: state.college.add_modal,
  add_btn: state.college.add_btn,
  resetform: state.college.resetform,
  loading: state.college.public_loading,
  colleges: state.college.public_college,
  total: state.college.public_total,
  pageNumber: state.college.public_pageNumber,
});
const mapDispatchToProps = {
  college_modal,
  add_college,
  public_college_fetch,
  public_search_college,
};
export default connect(mapStateToProps, mapDispatchToProps)(College);
