import React, { useState, useEffect } from "react";
import JoditEditor from "jodit-react";
import { Form, Input, Button } from "antd";
import notifications from "../../../utils/notifications";

const AddCollegeForm = (props) => {
  const [form] = Form.useForm();
  const [image, setImage] = useState("");
  const [description, setDescription] = useState("");

  useEffect(() => {
    if (props.resetform) {
      form.resetFields();
      setImage("");
      setDescription("");
    }
  }, [props.resetform, form]);

  const handleImage = (e) => {
    let { files, type } = e.target;
    const image = files;

    if (type === "file") {
      if (image[0]) {
        if (
          image[0].type === "image/jpeg" ||
          image[0].type === "image/png" ||
          image[0].type === "image/jpg"
        ) {
          if (image[0].size / 1024 / 1024 <= 2) {
            setImage(image);
          } else {
            notifications.showWarning("File is more then 2MB.");
          }
        } else {
          notifications.showWarning(
            "File format onley jpge or png is accepted."
          );
        }
      }
    }
  };

  const Image = image ? (
    <img
      src={URL.createObjectURL(image[0])}
      className='img-fluid'
      style={{ maxHeight: "400px", width: "100%" }}
      alt='College'
    />
  ) : null;

  const onFinish = (value) => {
    const formData = new FormData();
    formData.append("description", description);
    formData.append("college_image", image[0]);
    for (var key in value) {
      formData.append(key, value[key]);
    }
    props.action(formData);
  };
  return (
    <>
      <div className='college-form'>
        <Form onFinish={onFinish} form={form} scrollToFirstError={true}>
          <div className='formm-group'>
            <label htmlFor='college_name'>
              <span style={{ color: "red" }}>*</span>
              <strong>College Name</strong>
            </label>
            <Form.Item
              name='college_name'
              rules={[
                {
                  required: true,
                  message: "Please input College Name",
                },
              ]}
            >
              <Input placeholder='College Name Here....' size='large' />
            </Form.Item>
          </div>
          <div className='formm-group'>
            <label htmlFor='address'>
              <span style={{ color: "red" }}>*</span>
              <strong>Colege Address</strong>
            </label>
            <Form.Item
              name='address'
              rules={[
                {
                  required: true,
                  message: "Please input College Address",
                },
              ]}
            >
              <Input placeholder='Input College Address....' size='large' />
            </Form.Item>
          </div>
          <div className='formm-group'>
            <label htmlFor='university'>
              <span style={{ color: "red" }}>*</span>
              <strong>University</strong>
            </label>
            <Form.Item
              name='university'
              rules={[
                {
                  required: true,
                  message: "Please input University",
                },
              ]}
            >
              <Input placeholder='Input University....' size='large' />
            </Form.Item>
          </div>
          <div className='formm-group'>
            <label htmlFor='description'>
              <strong>College Description</strong>
            </label>
            <JoditEditor
              tabIndex={1}
              name='description'
              onChange={(value) => {
                setDescription(value);
              }}
            />
          </div>
          <div className='formm-group' style={{ margin: "20px 0px" }}>
            <label htmlFor='college_image'>
              <strong>Image Of COllege</strong>
            </label>
            {image ? <div style={{ marginBottom: "20px" }}>{Image}</div> : null}
            <div className='input-group'>
              <label className='input-group-btn my-0'>
                <span
                  className='btn btn-large btn-outline-primary rounded-0'
                  id='browse'
                >
                  Browse&hellip;
                  <input id='csv-input' type='file' onChange={handleImage} />
                </span>
              </label>
              <input
                type='text'
                className='form-control rounded-0'
                readOnly
                placeholder='Upload Image For Article'
                value={image ? image[0].name : ""}
              />
            </div>
          </div>

          <Form.Item>
            <div className=' d-flex flex-row-reverse '>
              <div className='col-12 col-md-2' style={{ padding: "0" }}>
                <Button
                  type='primary'
                  htmlType='submit'
                  loading={props.add_btn}
                  block
                  size='large'
                >
                  Add College
                </Button>
              </div>
            </div>
          </Form.Item>
        </Form>
      </div>
    </>
  );
};

export default AddCollegeForm;
