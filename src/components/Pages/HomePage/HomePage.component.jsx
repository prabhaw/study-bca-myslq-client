import React, { useEffect } from "react";
import "./HomePage.component.less";
import Slider from "react-slick";
import Titles from "../../common/Title/Titles.component";
import HomeCollegeCard from "./HomeCollegeCard.component";
import { Skeleton } from "antd";
import { connect } from "react-redux";
import { college_fetch } from "./../../../Redux/actions/home.action";
import HomeSlider from "./HomeSlider.component";
import HomeBlogCard from "./HomeBlogCard.component";
import { fetch_public_blog } from "./../../../Redux/actions/blogpublic.action";
import { public_fetch } from "./../../../Redux/actions/notes.action";
import { public_fetch_home } from "./../../../Redux/actions/question.action";
import { Link } from "react-router-dom";
import HomeNoteCard from "./HomeNoteCard.component";
import HomeOldQuestionCard from "./HomeOldQuestionCard.component";
const HomePage = (props) => {
  const {
    college_fetch,
    colleges,
    collegeloading,
    fetch_public_blog,
    public_fetch_home,
    blogs,
    loading,
    public_fetch,
    notes,
    questions,
  } = props;
  useEffect(() => {
    document.title = "Home";
  }, []);
  useEffect(() => {
    college_fetch();
  }, [college_fetch]);
  useEffect(() => {
    fetch_public_blog(1, 6);
  }, [fetch_public_blog]);
  useEffect(() => {
    public_fetch(1, 5);
  }, [public_fetch]);
  useEffect(() => {
    public_fetch_home(1, 5);
  }, [public_fetch_home]);

  const totaldata = () => {
    if (colleges.length === 1) {
      return 1;
    } else if (colleges.length === 2) {
      return 2;
    } else if (colleges.length >= 3) {
      return 3;
    }
  };
  const settings = {
    dots: false,
    infinite: true,
    speed: 2000,
    slidesToShow: totaldata(),
    slidesToScroll: 1,
    swipeToSlide: true,
    autoplay: true,
    autoplaySpeed: 4000,
    cssEase: "linear",
    arrows: false,
    pauseOnHover: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: totaldata(),
          slidesToScroll: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: colleges.length < 2 ? 1 : 2,
          slidesToScroll: 1,
          infinite: true,
        },
      },

      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  const college = colleges.map((item, i) => (
    <div className={"pr-2 pl-2"} key={item.id}>
      <HomeCollegeCard college={item} />
    </div>
  ));

  return (
    <>
      <HomeSlider />
      <div style={{ padding: "20px 5% 10px 5%" }}>
        {collegeloading ? (
          <div className='row'>
            <div className='col-12 col-md-4'>
              <Skeleton active paragraph={{ rows: 10 }} />
            </div>
            <div className='col-12 col-md-4'>
              <Skeleton active paragraph={{ rows: 10 }} />
            </div>
            <div className='col-12 col-md-4'>
              <Skeleton active paragraph={{ rows: 10 }} />
            </div>
          </div>
        ) : (
          <>
            <Titles> Colleges</Titles>
            <div className='pt-5 pb-5'>
              <Slider {...settings}>{college}</Slider>
            </div>
          </>
        )}
      </div>
      <div style={{ background: "#F7F7F7" }}>
        <div style={{ padding: "20px 5% 20px 5%" }}>
          {loading ? (
            <div className='row'>
              <div className='col-12 col-md-4'>
                <Skeleton active paragraph={{ rows: 10 }} />
              </div>
              <div className='col-12 col-md-4'>
                <Skeleton active paragraph={{ rows: 10 }} />
              </div>
              <div className='col-12 col-md-4'>
                <Skeleton active paragraph={{ rows: 10 }} />
              </div>
            </div>
          ) : (
            <>
              <Titles>Articles</Titles>
              <div className='pt-5 row'>
                {blogs.map((item) => (
                  <div className='col-12 col-md-4 mb-3' key={item.id}>
                    <HomeBlogCard blog={item} />
                  </div>
                ))}
              </div>
              {blogs.length === 6 ? (
                <div className='row justify-content-center'>
                  <div className='col-12 col-md-4'>
                    <Link to='/articles'>
                      <button
                        type='button'
                        class='btn btn-success w-100'
                        style={{ borderRadius: "0px" }}
                      >
                        More Articles
                      </button>
                    </Link>
                  </div>
                </div>
              ) : null}
            </>
          )}
        </div>
      </div>
      <div className='note-container'>
        <div style={{ padding: "20px 5% 20px 5%" }}>
          <div className='row'>
            <div
              className='col-12 col-md-6 mb-2 '
              style={{ background: "#eee", borderRadius: "10px" }}
            >
              <Titles>Recent NOtes</Titles>
              <div className='pt-5'></div>
              {notes.map((item) => (
                <div key={item.id}>
                  <HomeNoteCard note={item} />
                </div>
              ))}
            </div>
            <div className='col-0 col-md-1'></div>
            <div
              className='col-12 col-md-5 mb-2 '
              style={{ background: "#eee", borderRadius: "10px" }}
            >
              <Titles>Recent OldQuestion</Titles>
              <div className='pt-5'>
                {questions.map((item) => (
                  <div key={item.id}>
                    <HomeOldQuestionCard question={item} />
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
const mapStateToProps = (state) => ({
  colleges: state.home.colleges,
  collegeloading: state.home.loading,
  sliders: state.slider.sliders,
  blogs: state.publicblog.blogs,
  loading: state.publicblog.loading,
  notes: state.notes.notes,
  questions: state.question.questions,
});
const mapDispatchToProps = {
  college_fetch,
  fetch_public_blog,
  public_fetch,
  public_fetch_home,
};
export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
