import React from "react";

import { Link } from "react-router-dom";

const HomeOldQuestionCard = (props) => {
  const { question } = props;
  return (
    <>
      <div className='list-group border-0  '>
        <div className='d-flex w-100 justify-content-between'>
          <h5 className='mb-1 note-h'>
            <Link
              to={`/questions/${question.id}`}
              style={{ textDecoration: "none" }}
            >
              {question.subject}
            </Link>
          </h5>
        </div>
        <p className='mb-1'>
          {question.university}&nbsp; &nbsp;{question.semester} &nbsp;
          {question.year} &nbsp; {question.exam_time}
        </p>
        <small>
          Publish At:&nbsp;{question.publish_date}
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; By:&nbsp;{question.user.first_name}
          &nbsp;{question.user.last_name}
        </small>
      </div>
      <hr />
    </>
  );
};

export default HomeOldQuestionCard;
