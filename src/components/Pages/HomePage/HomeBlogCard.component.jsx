import React from "react";
import { Link } from "react-router-dom";
const BLOG_URL = process.env.REACT_APP_BLOG_IMG_URL;

const HomeBlogCard = (props) => {
  const { blog } = props;
  return (
    <>
      <div className='card ' style={{ minHeight: "380px", maxHeight: "385px" }}>
        <img
          src={`${BLOG_URL}/${blog.blog_image}`}
          alt='...'
          style={{ minHeight: "200px", maxHeight: "200px", width: "100%" }}
          className='img-fluid'
        />
        <div className='card-body' style={{ overflow: "hidden" }}>
          <Link to={`/articles/${blog.id}`} style={{ textDecoration: "none" }}>
            <h6 style={{ fontWeight: "bold" }} className='card-title'>
              {blog.title && blog.title.length > 100 ? (
                <span>{`${blog.title.substring(0, 100)}...`}</span>
              ) : (
                <span>{blog.title}</span>
              )}
            </h6>
          </Link>
          <p>
            {blog.short_description && blog.short_description.length > 144 ? (
              <span>{`${blog.short_description.substring(0, 144)}.....`}</span>
            ) : (
              <span>{blog.short_description}</span>
            )}
          </p>
        </div>
      </div>
    </>
  );
};

export default HomeBlogCard;
