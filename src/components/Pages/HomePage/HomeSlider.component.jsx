import React, { useEffect } from "react";
import Slider from "react-slick";
import { fetch_slider } from "./../../../Redux/actions/slider.action";
import { connect } from "react-redux";

const SLIDER_URL = process.env.REACT_APP_SLIDER_IMG_URL;

const HomeSlider = (props) => {
  const { sliders, fetch_slider } = props;
  const settings = {
    dots: false,
    // fade: true,
    infinte: true,
    speed: 2000,
    slidesToShow: 1,
    arrows: true,
    slidesTOScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    pauseOnHover: false,
    className: "slides",
  };

  useEffect(() => {
    fetch_slider();
  }, [fetch_slider]);
  return (
    <>
      <Slider {...settings}>
        {sliders.map((item) => (
          <div key={item.id}>
            <img
              src={`${SLIDER_URL}/${item.slider_img}`}
              alt='carousel_img'
              className='slider-image '
              width='100%'
            />
          </div>
        ))}
      </Slider>
    </>
  );
};

const mapStateToProps = (state) => ({
  sliders: state.slider.sliders,
});
const mapDispatchToProps = { fetch_slider };
export default connect(mapStateToProps, mapDispatchToProps)(HomeSlider);
