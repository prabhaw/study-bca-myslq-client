import React, { useState, useEffect } from "react";
import { SizeMe } from "react-sizeme";
import { Document, Page } from "react-pdf/dist/entry.webpack";
import { FaDownload } from "react-icons/fa";
import {
  ArrowLeftOutlined,
  ArrowRightOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Skeleton, Card, Avatar } from "antd";
import { fetch_singel_question } from "./../../../Redux/actions/question.action";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
const { Meta } = Card;
const QUESTION_URL = process.env.REACT_APP_QUESTION_FILE_URL;
const USER_URL = process.env.REACT_APP_USER_IMG_URL;
const SingleNote = (props) => {
  const { fetch_singel_question, question, loading, history } = props;
  const Noteid = props.match.params.id;
  const [notes, setNote] = useState({ numPage: null, pageNumber: 1 });

  useEffect(() => {
    fetch_singel_question(Noteid);
  }, [fetch_singel_question, Noteid]);
  useEffect(() => {
    document.title = `Old Question-${question ? question.subject : ""}`;
    if (question === false) {
      history.push("/questions");
    }
  }, [question, history]);

  const onDocumentLoadSuccess = ({ numPages }) => {
    setNote((pre) => ({ ...pre, numPages }));
  };

  const goToPrevPage = () => {
    setNote((state) => ({ ...state, pageNumber: notes.pageNumber - 1 }));
  };
  const goToNextPage = () => {
    setNote((state) => ({ ...state, pageNumber: notes.pageNumber + 1 }));
  };
  const dowload = () => {
    const Url = `${QUESTION_URL}/${question.old_question}`;
    window.open(Url, "_blank");
  };

  const user_img =
    question && question.user ? (
      question.user.profile_image ? (
        <Avatar size={64} src={`${USER_URL}/${question.user.profile_image}`} />
      ) : (
        <Avatar
          size={64}
          icon={<UserOutlined />}
          className='article-page-icon'
        />
      )
    ) : null;
  console.log(question);
  return (
    <>
      <div className='container' style={{ marginTop: "20px" }}>
        <h3 className='bca-oldquestiion-title'>
          <strong>BCA Question</strong>
        </h3>
        <div className='row'>
          <div className='col-12 col-md-8'>
            {loading ? (
              <Skeleton active paragraph={{ rows: 10 }} />
            ) : (
              <>
                <h4 style={{ fontSize: "24px", fontWeight: "bold" }}>
                  {question ? question.subject : null}{" "}
                </h4>
                <p>
                  {question ? question.university : null}&nbsp;&nbsp;
                  {question ? question.semester : null}&nbsp;&nbsp;
                  {question ? question.year : null}
                </p>

                <div>
                  <div className='row'>
                    <Card style={{ border: "none", marginTop: 16 }}>
                      <Meta
                        avatar={user_img}
                        title={
                          question && question.user
                            ? question.user.first_name +
                              " " +
                              question.user.last_name
                            : ""
                        }
                        description={question ? question.publish_date : null}
                      />
                    </Card>
                  </div>

                  {question ? (
                    <>
                      <SizeMe
                        monitorHeight
                        refreshRate={128}
                        refreshMode={"debounce"}
                        render={({ size }) => (
                          <div>
                            <Document
                              file={`${QUESTION_URL}/${question.old_question}`}
                              onLoadSuccess={onDocumentLoadSuccess}
                            >
                              <Page
                                width={size.width}
                                pageNumber={notes.pageNumber}
                              />
                            </Document>
                            <nav
                              className='navbar navbar-light bg-light '
                              style={{ marginBottom: "20px" }}
                            >
                              <span className='mr-auto'>
                                <button
                                  className='ml-auto btn btn-link pdf-btn'
                                  onClick={goToPrevPage}
                                  disabled={
                                    notes.pageNumber === 1 ? true : false
                                  }
                                >
                                  <ArrowLeftOutlined /> Previous
                                </button>
                              </span>
                              <span className='mx-auto'>
                                <p className='mx-auto'>
                                  Page {notes.pageNumber} of {notes.numPages}
                                </p>
                              </span>
                              <span className='ml-auto'>
                                <button
                                  className='mr-auto btn btn-link pdf-btn'
                                  onClick={goToNextPage}
                                  disabled={
                                    notes.pageNumber === notes.numPages
                                      ? true
                                      : false
                                  }
                                >
                                  Next
                                  <ArrowRightOutlined />
                                </button>
                              </span>
                            </nav>
                          </div>
                        )}
                      />

                      <button
                        className='btn-download'
                        style={{ width: "100%" }}
                        onClick={dowload}
                      >
                        <FaDownload /> Download
                      </button>
                    </>
                  ) : (
                    ""
                  )}
                </div>
              </>
            )}
          </div>
          <div className='col-12 col-md-4'></div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  question: state.question.question,
  loading: state.question.single_load,
});
const mapDispatchToProps = { fetch_singel_question };
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(SingleNote));
