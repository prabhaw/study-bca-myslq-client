import React, { useState, useEffect } from "react";
import "./OldQuestion.component.less";
import { connect } from "react-redux";
import { Pagination, Skeleton } from "antd";
import SearchFormComponent from "../../common/SearchForm/SearchForm.component";
import Titles from "./../../common/Title/Titles.component";
import {
  public_fetch,
  search_question,
} from "./../../../Redux/actions/question.action";
import OldQuestionCard from "./OldQuestionCard.component";
const OldQuestion = (props) => {
  const {
    public_fetch,
    questions,
    loading,
    total,
    pageNumber,
    search_question,
  } = props;

  const [search, setSearch] = useState();

  useEffect(() => {
    public_fetch(1, 12);
  }, [public_fetch]);

  const searchQuestion = (value) => {
    search_question(value, 1, 12);
    setSearch(value);
  };
  const changePage = (page, pageSize) => {
    if (search) {
      search_question(search, page, pageSize);
    } else {
      public_fetch(page, pageSize);
    }
  };
  return (
    <>
      <div className='search-cover'>
        <div
          style={{
            padding: "0 5% 0 5%",
            paddingTop: "5vh",
            paddingBottom: "5vh",
          }}
        >
          <h4 className='search-text'>Search</h4>

          <SearchFormComponent action={searchQuestion} />
        </div>
      </div>
      <div style={{ padding: "5px 5% 20px 5%", paddingTop: "7vh" }}>
        <div className='row'>
          {loading ? (
            <div className='col-12 col-md-8'>
              <Skeleton active paragraph={{ rows: 3 }} />{" "}
              <Skeleton active paragraph={{ rows: 3 }} />
            </div>
          ) : (
            <div className='col-12 col-md-8'>
              <Titles>OLD Question</Titles>
              <div className='pt-5'>
                {questions.map((item) => (
                  <div key={item.id}>
                    <OldQuestionCard question={item} />
                  </div>
                ))}
              </div>
              <div className='justify-content-end d-flex mt-5'>
                <Pagination
                  total={total}
                  defaultCurrent={1}
                  current={pageNumber}
                  onChange={changePage}
                  hideOnSinglePage={true}
                  pageSize={12}
                  showSizeChanger={false}
                />
              </div>
            </div>
          )}

          <div className='col-12 col-md-4'>
            <div className='jumbotron' style={{ height: "400px" }}>
              <h5
                style={{
                  textAlign: "center",
                  fontWeight: "bold",
                  fontSize: "24px",
                  paddingTop: "100px",
                }}
              >
                Add Your ads Here
              </h5>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  questions: state.question.questions,
  loading: state.question.loading,
  total: state.question.total,
  pageNumber: state.question.pageNumber,
});
const mapDispatchToProps = { public_fetch, search_question };
export default connect(mapStateToProps, mapDispatchToProps)(OldQuestion);
