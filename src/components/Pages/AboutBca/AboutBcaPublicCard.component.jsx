import React from "react";
import { Card, Avatar } from "antd";
import { UserOutlined } from "@ant-design/icons";
import htmltoword from "react-html-parser";

const { Meta } = Card;
const User_IMG_URL = process.env.REACT_APP_USER_IMG_URL;
const AboutBcaPublicCard = (props) => {
  const { bca } = props;

  const profile =
    bca.user && bca.user.profile_image ? (
      <Avatar size={64} src={`${User_IMG_URL}/${bca.user.profile_image}`} />
    ) : (
      <Avatar size={64} icon={<UserOutlined />} />
    );
  return (
    <>
      <Card
        size='small'
        hoverable={true}
        title={
          <Meta
            avatar={profile}
            title={bca.user.first_name + " " + bca.user.last_name}
            description={bca.user.email}
          />
        }
      >
        <div className='about-bca-body'>
          <p style={{ fontWeight: "bold", fontSize: "16px" }}>
            {bca.short_description}
          </p>
          <div className='editor-post about-bca-page'>{htmltoword(bca.description)}</div>
        </div>
      </Card>
    </>
  );
};

export default AboutBcaPublicCard;
