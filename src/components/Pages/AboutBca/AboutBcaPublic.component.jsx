import React, { useEffect } from "react";
import "./AboutBcaPublic.component.less";
import Titles from "../../common/Title/Titles.component";
import AboutBcaPublicCard from "./AboutBcaPublicCard.component";
import { Skeleton, Pagination } from "antd";
import { connect } from "react-redux";
import { public_fetch } from "./../../../Redux/actions/aboutbca.action";
import MetaTags from "react-meta-tags";
const AboutBcaPublic = (props) => {
  const { public_fetch, loading, bca, pageNumber, total } = props;
  useEffect(() => {
    public_fetch(1, 12);
    document.title = "About BCA";
  }, [public_fetch]);

  const changePage = (page, pageSize) => {
    public_fetch(page, pageSize);
  };
  const aboutbca = bca.map((item) => (
    <div className='col-12 col-md-6 mb-3 about-bca-card' key={item.id}>
      <AboutBcaPublicCard bca={item} />
    </div>
  ));
  return (
    <>
      <MetaTags>
        <title>About BCA</title>
        <meta name='description' content='Articles about bca' />
      </MetaTags>
      <div style={{ padding: "20px 5% 20px 5%" }}>
        <Titles>About BCA</Titles>
        {loading ? (
          <div className='pt-5 row '>
            <div className='col-12 col-md-6'>
              <Skeleton active paragraph={{ rows: 10 }} />
            </div>

            <div className='col-12 col-md-6 '>
              <Skeleton active paragraph={{ rows: 10 }} />
            </div>
          </div>
        ) : (
          <>
            <div className='pt-5 row '>{aboutbca}</div>
            <div className='justify-content-end d-flex'>
              <Pagination
                total={total}
                defaultCurrent={1}
                current={pageNumber}
                onChange={changePage}
                hideOnSinglePage={true}
                pageSize={12}
                showSizeChanger={false}
              />
            </div>
          </>
        )}
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  loading: state.bca.public_loading,
  bca: state.bca.public_bca,
  total: state.bca.public_total,
  pageNumber: state.bca.public_pageNumber,
});
const mapDispatchToProps = { public_fetch };
export default connect(mapStateToProps, mapDispatchToProps)(AboutBcaPublic);
