import React, { useEffect } from "react";
import "./Profile.component.less";
import ProfileDetailList from "../../common/ProfileDetails/ProfileDetails.component";
import { Button } from "antd";
import {
  edit_profile,
  edit_profile_close,
} from "./../../../Redux/actions/EditProfile.action";
import {
  update_user,
  change_profile_pic,
} from "./../../../Redux/actions/user.action";
import { connect } from "react-redux";
import EditProfile from "../../common/EditProfile/EditProfile.component";
import ProfilePicture from "../../common/ProfilePicture/ProfilePicture.component";
import { FaUserEdit } from "react-icons/fa";
const Profile = (props) => {
  const {
    edit_profile,
    update_user,
    edit,
    user,
    update_btn,
    pic_btn,
    change_profile_pic,
    edit_profile_close,
  } = props;
  useEffect(() => {
    return () => {
      edit_profile_close();
    };
  }, [edit_profile_close]);
  useEffect(() => {
    document.title = "Profile";
  }, []);
  return (
    <>
      <div className='profile'>
        <div className='row justify-content-center'>
          <div className='col-12 col-md-3 picture-section '>
            <ProfilePicture
              picture={user.profile_image}
              action={change_profile_pic}
              pic_state={pic_btn}
            />
          </div>
          <div className='col-12 col-md-9 details-section'>
            <div className='card'>
              <div className='card-header bg-light'>
                <span
                  style={{
                    float: "left",
                    fontSize: "24px",
                    fontWeight: "bold",
                    color: "#364f6b",
                  }}
                >
                  Profile
                </span>
                {edit ? (
                  <Button
                    type='primary'
                    danger
                    style={{ float: "right" }}
                    onClick={() => {
                      edit_profile();
                    }}
                  >
                    Cancel
                  </Button>
                ) : (
                  <Button
                    type='link'
                    style={{ float: "right" }}
                    onClick={() => {
                      edit_profile();
                    }}
                  >
                    <FaUserEdit style={{ fontSize: "30px" }} />
                  </Button>
                )}
              </div>

              {edit ? (
                <EditProfile
                  user={user}
                  action={update_user}
                  btn={update_btn}
                  oncancle={() => {
                    edit_profile();
                  }}
                />
              ) : (
                <ProfileDetailList user={user} />
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
const mapStateToProps = (state) => ({
  edit: state.editprofile.edit_profile,
  user: state.auth.user,
  update_btn: state.editprofile.update_btn,
  pic_btn: state.editprofile.disable_profile_input,
});
const mapDispatchToProps = {
  edit_profile,
  update_user,
  change_profile_pic,
  edit_profile_close,
};
export default connect(mapStateToProps, mapDispatchToProps)(Profile);
