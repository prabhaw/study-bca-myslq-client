import React, { useEffect, useState } from "react";
import "./Notes.component.less";
import SearchFormComponent from "../../common/SearchForm/SearchForm.component";
import Titles from "../../common/Title/Titles.component";
import NotesCard from "./NotesCard.component";
import { Pagination, Skeleton } from "antd";
import { connect } from "react-redux";
import {
  public_fetch,
  search_note,
} from "./../../../Redux/actions/notes.action";
const Notes = (props) => {
  const {
    loading,
    notes,
    total,
    pageNumber,
    public_fetch,
    search_note,
  } = props;
  const [search, setSearch] = useState();
  useEffect(() => {
    public_fetch(1, 12);
  }, [public_fetch]);

  const searchNote = (value) => {
    search_note(value, 1, 12);
    setSearch(value);
  };

  const changePage = (page, pageSize) => {
    if (search) {
      search_note(search, page, pageSize);
    } else {
      public_fetch(page, pageSize);
    }
  };
  return (
    <>
      <div className='search-cover'>
        <div
          style={{
            padding: "0 5% 0 5%",
            paddingTop: "5vh",
            paddingBottom: "5vh",
          }}
        >
          <h4 className='search-text'>Search</h4>

          <SearchFormComponent action={searchNote} />
        </div>
      </div>
      <div style={{ padding: "5px 5% 20px 5%", paddingTop: "7vh" }}>
        <div className='row'>
          {loading ? (
            <div className='col-12 col-md-8'>
              <Skeleton active paragraph={{ rows: 3 }} />{" "}
              <Skeleton active paragraph={{ rows: 3 }} />
            </div>
          ) : (
            <div className='col-12 col-md-8'>
              <Titles>BCA NOTES</Titles>
              <div className='pt-5'>
                {notes.map((item) => (
                  <div key={item.id}>
                    <NotesCard note={item} />
                  </div>
                ))}
              </div>
              <div className='justify-content-end d-flex mt-5'>
                <Pagination
                  total={total}
                  defaultCurrent={1}
                  current={pageNumber}
                  onChange={changePage}
                  hideOnSinglePage={true}
                  pageSize={12}
                  showSizeChanger={false}
                />
              </div>
            </div>
          )}

          <div className='col-12 col-md-4'>
            <div className='jumbotron' style={{ height: "400px" }}>
              <h5
                style={{
                  textAlign: "center",
                  fontWeight: "bold",
                  fontSize: "24px",
                  paddingTop: "100px",
                }}
              >
                Add Your ads Here
              </h5>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  loading: state.notes.loading,
  notes: state.notes.notes,
  total: state.notes.note_total,
  pageNumber: state.notes.pageNumber,
});
const mapDispatchToProps = { public_fetch, search_note };
export default connect(mapStateToProps, mapDispatchToProps)(Notes);
