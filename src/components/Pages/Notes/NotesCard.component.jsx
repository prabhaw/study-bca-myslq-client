import React from "react";
import { Divider } from "antd";
import { Link } from "react-router-dom";

const NotesCard = (props) => {
  const { note } = props;
  return (
    <>
      <div className='list-group border-0  '>
        <div className='d-flex w-100 justify-content-between'>
          <h4 className='mb-1 note-h'>
            <Link to={`/notes/${note.id}`} style={{ textDecoration: "none" }}>
              {note.subject}
            </Link>
          </h4>
        </div>
        <p className='mb-1'>
          {note.university}&nbsp; &nbsp;{note.semester}
        </p>
        <small>
          Publish At:&nbsp;{note.publish_date} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          By:&nbsp;{note.user.first_name}&nbsp;{note.user.last_name}
        </small>
      </div>
      <Divider />
    </>
  );
};

export default NotesCard;
