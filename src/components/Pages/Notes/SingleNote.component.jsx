import React, { useState, useEffect } from "react";
import { SizeMe } from "react-sizeme";
import { Document, Page } from "react-pdf/dist/entry.webpack";
import { FaDownload } from "react-icons/fa";
import { DiscussionEmbed } from "disqus-react";

import {
  ArrowLeftOutlined,
  ArrowRightOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Skeleton, Card, Avatar } from "antd";
import { fetch_singel_note } from "./../../../Redux/actions/notes.action";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
const { Meta } = Card;
const NOTE_URL = process.env.REACT_APP_NOTE_FILE_URL;
const USER_URL = process.env.REACT_APP_USER_IMG_URL;
const SingleNote = (props) => {
  const { fetch_singel_note, note, loading, history } = props;
  const Noteid = props.match.params.id;
  const [notes, setNote] = useState({ numPage: null, pageNumber: 1 });

  useEffect(() => {
    fetch_singel_note(Noteid);
  }, [fetch_singel_note, Noteid]);
  useEffect(() => {
    document.title = `Note-${note ? note.subject : ""}`;
    if (note === false) {
      history.push("/notes");
    }
  }, [note, history]);

  const onDocumentLoadSuccess = ({ numPages }) => {
    setNote((pre) => ({ ...pre, numPages }));
  };

  const goToPrevPage = () => {
    setNote((state) => ({ ...state, pageNumber: notes.pageNumber - 1 }));
  };
  const goToNextPage = () => {
    setNote((state) => ({ ...state, pageNumber: notes.pageNumber + 1 }));
  };
  const dowload = () => {
    const Url = `${NOTE_URL}/${note.note}`;
    window.open(Url, "_blank");
  };

  const user_img =
    note && note.user ? (
      note.user.profile_image ? (
        <Avatar size={64} src={`${USER_URL}/${note.user.profile_image}`} />
      ) : (
        <Avatar
          size={64}
          icon={<UserOutlined />}
          className='article-page-icon'
        />
      )
    ) : null;

  return (
    <>
      <div className='container' style={{ marginTop: "20px" }}>
        <h3 className='bca-oldquestiion-title'>
          <strong>BCA Notes</strong>
        </h3>
        <div className='row'>
          <div className='col-12 col-md-8'>
            {loading ? (
              <Skeleton active paragraph={{ rows: 10 }} />
            ) : (
              <>
                <h4 style={{ fontSize: "24px", fontWeight: "bold" }}>
                  {note ? note.subject : null}{" "}
                </h4>
                <p>
                  {note ? note.university : null}&nbsp;&nbsp;
                  {note ? note.semester : null}
                </p>

                <div>
                  <div className='row'>
                    <Card style={{ border: "none", marginTop: 16 }}>
                      <Meta
                        avatar={user_img}
                        title={
                          note && note.user
                            ? note.user.first_name + " " + note.user.last_name
                            : ""
                        }
                        description={note ? note.publish_date : null}
                      />
                    </Card>
                  </div>

                  {note ? (
                    <>
                      <SizeMe
                        monitorHeight
                        refreshRate={128}
                        refreshMode={"debounce"}
                        render={({ size }) => (
                          <div>
                            <Document
                              file={`${NOTE_URL}/${note.note}`}
                              onLoadSuccess={onDocumentLoadSuccess}
                            >
                              <Page
                                width={size.width}
                                pageNumber={notes.pageNumber}
                              />
                            </Document>
                            <nav
                              className='navbar navbar-light bg-light '
                              style={{ marginBottom: "20px" }}
                            >
                              <span className='mr-auto'>
                                <button
                                  className='ml-auto btn btn-link pdf-btn'
                                  onClick={goToPrevPage}
                                  disabled={
                                    notes.pageNumber === 1 ? true : false
                                  }
                                >
                                  <ArrowLeftOutlined /> Previous
                                </button>
                              </span>
                              <span className='mx-auto'>
                                <p className='mx-auto'>
                                  Page {notes.pageNumber} of {notes.numPages}
                                </p>
                              </span>
                              <span className='ml-auto'>
                                <button
                                  className='mr-auto btn btn-link pdf-btn'
                                  onClick={goToNextPage}
                                  disabled={
                                    notes.pageNumber === notes.numPages
                                      ? true
                                      : false
                                  }
                                >
                                  Next
                                  <ArrowRightOutlined />
                                </button>
                              </span>
                            </nav>
                          </div>
                        )}
                      />
                      <p style={{ fontSize: "18px" }}>
                        {note ? note.description : null}
                      </p>
                      <button
                        className='btn-download'
                        style={{ width: "100%" }}
                        onClick={dowload}
                      >
                        <FaDownload /> Download
                      </button>
                      <br />
                      <DiscussionEmbed
                        shortname='studybca-com'
                        config={{
                          url: `https://studybca.com/notes/${Noteid}`,
                          identifier: Noteid,
                          title: note
                            ? note.subject +
                              " " +
                              note.university +
                              " " +
                              note.semester
                            : "",
                        }}
                      />
                    </>
                  ) : (
                    ""
                  )}
                </div>
              </>
            )}
          </div>
          <div className='col-12 col-md-4'></div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  note: state.notes.note,
  loading: state.notes.single_load,
});
const mapDispatchToProps = { fetch_singel_note };
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(SingleNote));
