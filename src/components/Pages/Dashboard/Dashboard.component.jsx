import React, { useEffect } from "react";
import "./Dashboard.component.less";
import { FaRegNewspaper } from "react-icons/fa";
import { MdLibraryBooks } from "react-icons/md";
import { RightOutlined } from "@ant-design/icons";
import { GiBookCover } from "react-icons/gi";
import { connect } from "react-redux";
import {
  total_blog,
  total_note,
  total_question,
} from "./../../../Redux/actions/total.action";
import { Link } from "react-router-dom";
const Dashboard = (props) => {
  const {
    total_blog,
    total_note,
    total_question,
    blog,
    note,
    question,
  } = props;

  useEffect(() => {
    document.title = "Dashboard";
  }, []);
  useEffect(() => {
    total_blog();
  }, [total_blog]);
  useEffect(() => {
    total_note();
  }, [total_note]);
  useEffect(() => {
    total_question();
  }, [total_question]);
  return (
    <>
      <div
        className='jumbotron'
        style={{ textAlign: "center", background: "#1DA57A" }}
      >
        <div className='logo-holder logo-6'>
          <h3>
            Welcome To <span>Study BCA Team</span>
          </h3>
        </div>
      </div>
      <div className='row justify-content-center'>
        <div className='col-xl-3 col-sm-6 mb-3 col-11'>
          <div
            className='card text-white bg-warning  o-hidden h-100'
            style={{ overflowX: "hidden" }}
          >
            <div className='card-body'>
              <div className='card-body-icon '>
                <FaRegNewspaper />
              </div>
              <div className='mr-5'>{blog}&nbsp;New Post!</div>
            </div>

            <span className='card-footer text-white clearfix small z-1'>
              <Link to='/user/articles' style={{ color: "#fff" }}>
                <>
                  <span className='float-left'>View Details</span>
                  <span className='float-right'>
                    <RightOutlined />
                  </span>
                </>
              </Link>
            </span>
          </div>
        </div>
        <div className='col-xl-3 col-sm-6 mb-3 col-11'>
          <div
            className='card text-white bg-primary o-hidden h-100'
            style={{ overflowX: "hidden" }}
          >
            <div className='card-body'>
              <div className='card-body-icon'>
                <GiBookCover />
              </div>
              <div className='mr-5'>{note} &nbsp; Total Notes!</div>
            </div>
            <span className='card-footer text-white clearfix small z-1'>
              <Link to='/user/notes' style={{ color: "#fff" }}>
                <>
                  <span className='float-left'>View Details</span>
                  <span className='float-right'>
                    <RightOutlined />
                  </span>
                </>
              </Link>
            </span>
          </div>
        </div>
        <div className='col-xl-3 col-sm-6 mb-3 col-11'>
          <div
            className='card text-white bg-success o-hidden h-100'
            style={{ overflowX: "hidden" }}
          >
            <div className='card-body'>
              <div className='card-body-icon'>
                <MdLibraryBooks />
              </div>
              <div className='mr-5'>{question}&nbsp; Total Question!</div>
            </div>
            <span className='card-footer text-white clearfix small z-1'>
              {" "}
              <Link to='/user/oldquestion' style={{ color: "#fff" }}>
                <>
                  <span className='float-left'>View Details</span>
                  <span className='float-right'>
                    <RightOutlined />
                  </span>
                </>
              </Link>
            </span>
          </div>
        </div>
      </div>
    </>
  );
};
const mapStateToProps = (state) => ({
  blog: state.total.totalpost,
  note: state.total.totalnote,
  question: state.total.totalquestion,
});
const mapDispatchToProps = { total_blog, total_note, total_question };
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
