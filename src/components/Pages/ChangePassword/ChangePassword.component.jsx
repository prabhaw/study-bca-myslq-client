import React, { useEffect } from "react";
import { LockOutlined } from "@ant-design/icons";
import { update_password } from "./../../../Redux/actions/user.action";
import { connect } from "react-redux";
import { Form, Input, Button } from "antd";

const ChangePassword = (props) => {
  useEffect(() => {
    document.title = "Change-Password.";
  }, []);
  const [form] = Form.useForm();
  const onFinish = (value) => {
    props.update_password(value);
    form.resetFields();
  };

  return (
    <div className='container-fluid password-change '>
      <div className='row justify-content-center '>
        <div className=' col-12 col-md-5  '>
          <div className=' login-form'>
            {/* <DividerPage title='Change Password' /> */}
            <Form
              name='normal_login'
              form={form}
              className=''
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
            >
              <label htmlFor='password'>
                <span style={{ color: "red" }}>*</span>Old Password
              </label>
              <Form.Item
                name='password'
                rules={[
                  {
                    required: true,
                    message: "Please input your Password!",
                  },
                ]}
              >
                <Input.Password
                  size='large'
                  prefix={<LockOutlined className='site-form-item-icon' />}
                  type='password'
                  name='password'
                  placeholder='Old Password'
                />
              </Form.Item>
              <label htmlFor='varifypassword'>
                <span style={{ color: "red" }}>*</span>New Password
              </label>
              <Form.Item
                name='newpassword'
                rules={[
                  {
                    required: true,
                    message: "Please input your New Password!",
                  },
                  {
                    pattern: "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z@$&!\\d]{8,32}$",

                    message:
                      "Password must be 8 character long and One Number!",
                  },
                  // { validator: validatePassword },
                ]}
                hasFeedback
              >
                <Input.Password
                  size='large'
                  prefix={<LockOutlined className='site-form-item-icon' />}
                  type='password'
                  placeholder='New Password'
                />
              </Form.Item>

              <label htmlFor='varifypassword'>
                <span style={{ color: "red" }}>*</span>Confirm Password
              </label>
              <Form.Item
                name='varifypassword'
                rules={[
                  {
                    required: true,
                    message: "Please input your Confirm Password!",
                  },

                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (!value || getFieldValue("newpassword") === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject("Password not match!");
                    },
                  }),
                ]}
                hasFeedback
              >
                <Input.Password
                  size='large'
                  prefix={<LockOutlined className='site-form-item-icon' />}
                  type='password'
                  placeholder='Confirm Password'
                />
              </Form.Item>
              <Form.Item>
                <Button
                  type='primary'
                  htmlType='submit'
                  block
                  loading={props.update_btn}
                  className='login-form-button'
                >
                  Change Password
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};
const mapStateToProps = (state) => ({
  update_btn: state.editprofile.password_btn,
});

const mapDispatchToProps = {
  update_password,
};
export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);
