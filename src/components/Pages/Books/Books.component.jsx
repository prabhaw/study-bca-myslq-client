import React, { useEffect, useState } from "react";
import Titles from "../../common/Title/Titles.component";
import { Pagination, Skeleton } from "antd";
import BookCard from "../../common/BookCard/BookCard.component";
import SearchFormComponent from "../../common/SearchForm/SearchForm.component";
import { fetch_book, search_book } from "./../../../Redux/actions/books.action";
import { connect } from "react-redux";
const Books = (props) => {
  const { book, loading, total, pageNumber, fetch_book, search_book } = props;
  const [search, setSearch] = useState();
  useEffect(() => {
    document.title = "BCA Books";
  }, []);

  useEffect(() => {
    fetch_book(1, 5);
  }, [fetch_book]);
  const changePage = (page, pageSize) => {
    if (search) {
      search_book(search, page, pageSize);
    } else {
      fetch_book(page, pageSize);
    }
  };

  const searchBook = (value) => {
    search_book(value, 1, 5);
    setSearch(value);
  };

  const book_data = book.map((item) => (
    <div key={item.id}>
      <BookCard book={item} />
    </div>
  ));

  return (
    <>
      <div className='search-cover'>
        <div
          style={{
            padding: "0 5% 0 5%",
            paddingTop: "5vh",
            paddingBottom: "5vh",
          }}
        >
          <h4 className='search-text'>Search</h4>

          <SearchFormComponent action={searchBook} />
        </div>
      </div>
      <div style={{ padding: "20px 5% 20px 5%" }}>
        <div className='row  '>
          <div className='col-12 col-md-8  '>
            <Titles>BCA Books</Titles>
            <div className='pt-5'></div>
            {loading ? <Skeleton active paragraph={{ rows: 8 }} /> : book_data}
            <div className='justify-content-end d-flex mt-5'>
              <Pagination
                total={total}
                defaultCurrent={1}
                current={pageNumber}
                onChange={changePage}
                hideOnSinglePage={true}
                pageSize={5}
                showSizeChanger={false}
              />
            </div>
          </div>
          <div className='col-12 col-md-4'>
            <div className='jumbotron' style={{ height: "400px" }}>
              <h5
                style={{
                  textAlign: "center",
                  fontWeight: "bold",
                  fontSize: "24px",
                  paddingTop: "100px",
                }}
              >
                Add Your ads Here
              </h5>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  pageNumber: state.books.pageNumber,
  loading: state.books.loading,

  book: state.books.book,
  total: state.books.total,
});
const mapDispatchToProps = { fetch_book, search_book };
export default connect(mapStateToProps, mapDispatchToProps)(Books);
