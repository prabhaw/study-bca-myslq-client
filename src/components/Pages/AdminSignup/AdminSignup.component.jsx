import React, { useEffect } from "react";
import "./AdminSignup.component.less";
import { Form, Input, Button } from "antd";
import { LockOutlined, MailOutlined } from "@ant-design/icons";
import {
  check_admin,
  create_admin,
} from "./../../../Redux/actions/adminsignup.action";
import { connect } from "react-redux";
const AdminSignUp = (props) => {
  const { check_admin, user, history, create_admin } = props;

  useEffect(() => {
    document.title = "Admin-signUp";
    check_admin();
  }, [check_admin]);

  useEffect(() => {
    if (user === true) {
      history.push("/");
    }
  }, [user, history]);

  const onFinish = (value) => {
    create_admin(value, history);
  };
  console.log(user);
  return (
    <>
      {user ? (
        ""
      ) : (
        <div
          style={{
            marginTop: "20px",
          }}
          className='container'
        >
          <div className='justify-content-center row'>
            <div className='col-12 col-md-6'>
              <div className='alert alert-info '>
                <div className='row justify-content-center '>
                  <div className=' col-12   '>
                    <div className=' login-form'>
                      <Form
                        name='normal_login'
                        className=''
                        initialValues={{
                          remember: true,
                        }}
                        onFinish={onFinish}
                      >
                        <label htmlFor='first_name'>
                          <span style={{ color: "red" }}>*</span>First Name
                        </label>
                        <Form.Item
                          name='first_name'
                          rules={[
                            {
                              required: true,
                              message: "Please input your First Name!",
                            },
                          ]}
                        >
                          <Input placeholder='First Name' size='large' />
                        </Form.Item>
                        <label htmlFor='last_name'>
                          <span style={{ color: "red" }}>*</span>Last Name
                        </label>
                        <Form.Item
                          name='last_name'
                          rules={[
                            {
                              required: true,
                              message: "Please input your Last Name!",
                            },
                          ]}
                        >
                          <Input placeholder='Last Name' size='large' />
                        </Form.Item>

                        <label htmlFor='email'>
                          <span style={{ color: "red" }}>*</span>Email
                        </label>
                        <Form.Item
                          name='email'
                          rules={[
                            {
                              required: true,
                              message: "Please input your Email!",
                            },
                            { type: "email" },
                          ]}
                        >
                          <Input
                            prefix={
                              <MailOutlined className='site-form-item-icon' />
                            }
                            placeholder='Email'
                            size='large'
                          />
                        </Form.Item>

                        <label htmlFor='password'>
                          <span style={{ color: "red" }}>*</span> Password
                        </label>

                        <Form.Item
                          name='password'
                          rules={[
                            {
                              required: true,
                              message: "Please input your New Password!",
                            },
                            {
                              pattern:
                                "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z@$&!\\d]{8,32}$",

                              message:
                                "Password must be 8 character long and One Number!",
                            },
                            // { validator: validatePassword },
                          ]}
                          hasFeedback
                        >
                          <Input.Password
                            prefix={
                              <LockOutlined className='site-form-item-icon' />
                            }
                            type='password'
                            placeholder='New Password'
                            size='large'
                          />
                        </Form.Item>

                        <label htmlFor='varifypassword'>
                          <span style={{ color: "red" }}>*</span>Confirm
                          Password
                        </label>
                        <Form.Item
                          name='varifypassword'
                          rules={[
                            {
                              required: true,
                              message: "Please input your Confirm Password!",
                            },

                            ({ getFieldValue }) => ({
                              validator(rule, value) {
                                if (
                                  !value ||
                                  getFieldValue("password") === value
                                ) {
                                  return Promise.resolve();
                                }
                                return Promise.reject("Password not match!");
                              },
                            }),
                          ]}
                          hasFeedback
                        >
                          <Input.Password
                            prefix={
                              <LockOutlined className='site-form-item-icon' />
                            }
                            type='password'
                            placeholder='Confirm Password'
                            size='large'
                          />
                        </Form.Item>

                        <Form.Item>
                          <Button
                            type='primary'
                            htmlType='submit'
                            block
                            loading={props.btn}
                            size='large'
                          >
                            Sign Up
                          </Button>
                        </Form.Item>
                      </Form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};
const mapSateToProps = (state) => ({
  user: state.admin.admin_account,
  btn: state.admin.admin_btn,
});
const madDispatchToProps = { check_admin, create_admin };
export default connect(mapSateToProps, madDispatchToProps)(AdminSignUp);
