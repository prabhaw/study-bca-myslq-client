import React, { useEffect } from "react";
import { Result, Button } from "antd";
import { Link } from "react-router-dom";
const ChangePassEmail = (props) => {
  useEffect(() => {
    document.title = "Change-Password.";
  }, []);
  return (
    <>
      <Result
        status='success'
        title='Email has been sended to your Accout.'
        extra={[
          <Link to='/'>
            <Button type='primary' danger>
              Go to Home
            </Button>
          </Link>,
        ]}
      />
    </>
  );
};

export default ChangePassEmail;
