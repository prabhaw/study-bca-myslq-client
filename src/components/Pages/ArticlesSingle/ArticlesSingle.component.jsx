import React, { useEffect } from "react";
import "./ArticlesSingle.component.less";
import { Card, Avatar, Skeleton } from "antd";
import { UserOutlined } from "@ant-design/icons";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { fetch_blog } from "./../../../Redux/actions/blogpublic.action";
import { DiscussionEmbed } from "disqus-react";
import MetaTags from "react-meta-tags";
import htmltoword from "react-html-parser";
const { Meta } = Card;
const BLOG_URL = process.env.REACT_APP_BLOG_IMG_URL;
const USER_URL = process.env.REACT_APP_USER_IMG_URL;

const ArticlesSingle = (props) => {
  const { fetch_blog, blog, history, loading } = props;
  const blogid = props.match.params.id;
  useEffect(() => {
    fetch_blog(blogid);
  }, [fetch_blog, blogid]);

  useEffect(() => {
    if (!blog) {
      history.push("/articles");
    } else {
      document.title = `Article-${blog.title}`;
    }
  }, [blog, history]);
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const profile_pic =
    blog && blog.user && blog.user.profile_image ? (
      <Avatar
        size={64}
        src={`${USER_URL}/${blog.user ? blog.user.profile_image : null}`}
      />
    ) : (
      <Avatar size={64} icon={<UserOutlined />} />
    );
  return (
    <div style={{ padding: "20px 5% 20px 5%" }}>
      <MetaTags>
        <meta name='description' content={blog ? blog.title : ""} />
        <meta
          property='og:title'
          content={blog ? blog.short_description : null}
        />
        <meta
          property='og:image'
          content={`${BLOG_URL}/${blog ? blog.blog_image : ""}`}
        />
      </MetaTags>
      <div className='jumbotron' style={{ height: "170px" }}>
        <h5
          style={{
            textAlign: "center",
            fontWeight: "bold",
            fontSize: "24px",
          }}
        >
          Add Your ads Here
        </h5>
      </div>
      <div className='row'>
        <div className='col-12 col-md-8'>
          {loading ? (
            <Skeleton active paragraph={{ rows: 10 }} />
          ) : (
            <>
              <h4 className='blog-title'>{blog ? blog.title : null}</h4>
              {blog && blog.blog_image ? (
                <img
                  src={`${BLOG_URL}/${blog.blog_image}`}
                  className='img-fluid'
                  style={{ width: "100%", height: "auto" }}
                  alt='blog_image'
                />
              ) : (
                ""
              )}
              <div className='mt-5 mb-3'>
                <Meta
                  avatar={profile_pic}
                  title={
                    blog && blog.user
                      ? blog.user.first_name + " " + blog.user.last_name
                      : ""
                  }
                  description={blog ? blog.publish_date : ""}
                />
              </div>
              <h6>{blog ? blog.short_description : null}</h6>
              <br></br>
              <div className='descriptio editor-post '>
                {htmltoword(blog ? blog.description : "")}
              </div>
              <br />
              <DiscussionEmbed
                shortname='studybca-com'
                config={{
                  url: `https://studybca.com/articles/${blogid}`,
                  identifier: blogid,
                  title: blog ? blog.title : "",
                }}
              />
            </>
          )}
        </div>
        <div className='col-12 col-md-4'>
          <div className='jumbotron' style={{ height: "400px" }}>
            <h5
              style={{
                textAlign: "center",
                fontWeight: "bold",
                fontSize: "24px",
                paddingTop: "100px",
              }}
            >
              Add Your ads Here
            </h5>
          </div>
        </div>
      </div>
    </div>
  );
};
const mapStateToProps = (state) => ({
  blog: state.publicblog.blog,
  loading: state.publicblog.blog_loading,
});
const mapDispatchToProps = { fetch_blog };
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(React.memo(ArticlesSingle)));
