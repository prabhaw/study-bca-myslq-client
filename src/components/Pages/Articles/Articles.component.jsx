import React, { useEffect, useState } from "react";
import "./Articles.component.less";
import BlogPageCard from "./../../common/BlogPageCard/BlogPageCard.component";
import Titles from "./../../common/Title/Titles.component";
import { connect } from "react-redux";
import {
  fetch_public_blog,
  fetch_public_blog_search,
} from "./../../../Redux/actions/blogpublic.action";
import { Skeleton, Pagination, Divider } from "antd";
import SearchInput from "../../common/Search/Search.component";

const title = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`;
const Articles = (props) => {
  const {
    blogs,
    loading,
    blog_total,
    pageNum,
    fetch_public_blog,
    fetch_public_blog_search,
  } = props;
  const [search, setSearch] = useState("");
  useEffect(() => {
    fetch_public_blog(1, 12);
    document.title = "Recent Articles";
  }, [fetch_public_blog]);
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const changePage = (page, pageSize) => {
    if (search) {
      fetch_public_blog_search(search, page, pageSize);
    } else {
      fetch_public_blog(page, pageSize);
    }
  };
  const blog = blogs.map((item) => (
    <div key={item.id}>
      <BlogPageCard title={title} blog={item} />
      <Divider />
    </div>
  ));
  const searchData = (value) => {
    setSearch(value);
    fetch_public_blog_search(value, 1, 12);
  };
  return (
    <>
      <div style={{ padding: "20px 5% 20px 5%" }}>
        <div className='jumbotron' style={{ height: "170px" }}>
          <h5
            style={{
              textAlign: "center",
              fontWeight: "bold",
              fontSize: "24px",
            }}
          >
            Add Your ads Here
          </h5>
        </div>
        <div className='d-block d-md-none mb-5'>
          <SearchInput action={searchData} />
        </div>
        {search ? <Titles>Search Articles</Titles> : <Titles> Articles</Titles>}
        <div className='row pt-5'>
          <div className='col-12 col-md-8 '>
            {loading ? <Skeleton active paragraph={{ rows: 10 }} /> : blog}
            <div className='justify-content-end d-flex'>
              <Pagination
                total={blog_total}
                defaultCurrent={1}
                current={pageNum}
                onChange={changePage}
                hideOnSinglePage={true}
                pageSize={12}
                showSizeChanger={false}
              />
            </div>
          </div>
          <div className='col-12 col-md-4'>
            <div className='d-none d-sm-block'>
              <SearchInput action={searchData} />
            </div>
            <div className='mb-4'></div>
            <div className='jumbotron' style={{ height: "400px" }}>
              <h5
                style={{
                  textAlign: "center",
                  fontWeight: "bold",
                  fontSize: "24px",
                  paddingTop: "100px",
                }}
              >
                Add Your ads Here
              </h5>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
const mapStateToProps = (state) => ({
  blogs: state.publicblog.blogs,
  loading: state.publicblog.loading,
  blog_total: state.publicblog.blog_total,
  pageNum: state.publicblog.pageNum,
});
const mapDispatchToProps = { fetch_public_blog, fetch_public_blog_search };
export default connect(mapStateToProps, mapDispatchToProps)(Articles);
