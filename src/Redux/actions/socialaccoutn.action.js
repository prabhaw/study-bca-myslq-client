import { SOCIAL_REGISTER_MODAL, LOGIN_MODAL, SOCIAL_PASS_END } from "./types";
import httpClient from "../../utils/httpClient";
import notifications from "../../utils/notifications";

export const password_modal_close = (data) => (dispatch) => {
  dispatch({ type: SOCIAL_REGISTER_MODAL });
};
export const password_modal = (data) => (dispatch) => {
  httpClient
    .POST("/user/socialemaicheck", data, false)
    .then((data) => {
      dispatch({ type: LOGIN_MODAL });
      dispatch({ type: SOCIAL_REGISTER_MODAL });
    })
    .catch((err) => {
      notifications.handleError(err.response);
    });
};

export const signup_social = (data) => (dispatch) => {
  httpClient
    .POST("/user", data, false)
    .then(() => {
      dispatch({ type: LOGIN_MODAL });
      dispatch({ type: SOCIAL_REGISTER_MODAL });
      dispatch({ type: SOCIAL_PASS_END });
    })
    .catch((err) => {
      notifications.handleError(err.response);
      dispatch({ type: SOCIAL_PASS_END });
    });
};
