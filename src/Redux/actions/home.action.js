import { TOP_COLLEGE_END, TOP_COLLEGE_FETCH } from "./types";
import httpClient from "../../utils/httpClient";

export const college_fetch = () => (dispatch) => {
  dispatch({ type: TOP_COLLEGE_FETCH });
  httpClient.GET("/college/topcollege", false, {}).then((data) => {
    dispatch({ type: TOP_COLLEGE_END, payload: data.data });
  });
};
