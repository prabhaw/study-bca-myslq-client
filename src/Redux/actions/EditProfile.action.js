import { EDIT_PROFILE, EDIT_PROFILE_CLOSE } from "../actions/types";

export const edit_profile = () => (dispatch) => {
  dispatch({ type: EDIT_PROFILE });
};

export const edit_profile_close = () => (dispatch) => {
  dispatch({ type: EDIT_PROFILE_CLOSE });
};
