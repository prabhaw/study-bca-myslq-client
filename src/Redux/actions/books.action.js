import {
  BOOK_MODAL,
  ADD_BOOK,
  ADD_BOOK_SUCC,
  ADD_BOOK_FAIL,
  FETCH_BOOK_STR,
  FETCH_BOOK_SUCC,
  BOOK_DELETED,
} from "./types";
import httpClient from "../../utils/httpClient";
import notifications from "../../utils/notifications";

export const book_modal = () => (dispatch) => {
  dispatch({ type: BOOK_MODAL });
};

export const add_book = (data) => (dispatch) => {
  dispatch({ type: ADD_BOOK });
  httpClient
    .POST("/books", data, true)
    .then((data) => {
      dispatch({ type: ADD_BOOK_SUCC, payload: data.data });
    })
    .catch((err) => {
      notifications.handleError(err.response);
      dispatch({ type: ADD_BOOK_FAIL });
    });
};

export const fetch_book = (pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: FETCH_BOOK_STR });

  httpClient.GET("/books", false, { pageNumber, pageSize }).then((data) => {
    dispatch({
      type: FETCH_BOOK_SUCC,
      payload: data.data.data,
      total: data.data.total,
      pageNumber: pageNumber,
    });
  });
};

export const search_book = (data, pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: FETCH_BOOK_STR });
  httpClient
    .GET("/books/search", false, { pageNumber, pageSize, ...data })
    .then((data) => {
      dispatch({
        type: FETCH_BOOK_SUCC,
        payload: data.data.data,
        total: data.data.total,
        pageNumber: pageNumber,
      });
    });
};

export const on_book_delete = (id) => (dispathc) => {
  httpClient
    .REMOVE(`/books/${id}`, true)
    .then((data) => {
      dispathc({ type: BOOK_DELETED, index: id });
      notifications.showSuccess(data.data.msg);
    })
    .catch((err) => {
      notifications.handleError(err.response);
    });
};
