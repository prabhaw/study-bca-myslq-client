import {
  LOGIN_START,
  LOGIN_END,
  LOGIN_FAIL,
  LOGIN_MODAL,
  LOGOUT,
  CREATE_USER,
  SIGING_UP,
  SIGN_UP,
  SIGNUP_FAIL,
} from "./types";
import httpClient from "../../utils/httpClient";
import notifications from "./../../utils/notifications";

export const login_modal = () => (dispatch) => {
  dispatch({ type: LOGIN_MODAL });
};
export const create_user = () => (dispatch) => {
  dispatch({ type: CREATE_USER });
};
export const log_out = () => (dispatch) => {
  localStorage.clear();
  dispatch({ type: LOGOUT });
};

export const logIn_user = (user_data, history) => (dispatch) => {
  dispatch({ type: LOGIN_START });

  httpClient
    .POST("/auth", user_data, false)
    .then((data) => {
      localStorage.setItem("token", data.data.token);
      dispatch({
        type: LOGIN_END,
        payload: data.data.user,
      });
      dispatch({ type: LOGIN_MODAL });
      history.push("/user/dashboard");
      notifications.showSuccess(
        `Welcome ${data.data.user.first_name} ${data.data.user.last_name}`
      );
    })
    .catch((err) => {
      if (err.response) {
        notifications.handleError(err.response);
        dispatch({ type: LOGIN_FAIL });
      }
    });
};

export const register_user = (data) => (dispatch) => {
  dispatch({ type: SIGING_UP });
  httpClient
    .POST("/user", data, false)
    .then((data) => {
      dispatch({ type: SIGN_UP });
      notifications.showSuccess("User is Register.");
    })
    .catch((err) => {
      notifications.handleError(err.response);
      dispatch({ type: SIGNUP_FAIL });
    });
};
