import {
  SEARCH_FETCH,
  SEARCH_SEMESTER,
  SEARCH_SUBJECT,
} from "./../actions/types";
import httpClient from "../../utils/httpClient";
import notifications from "../../utils/notifications";

export const syllabus_university = (param = {}) => (dispatch) => {
  httpClient
    .GET("/syllabus/input", false, param)
    .then((data) => {
      dispatch({ type: SEARCH_FETCH, payload: data.data });
    })
    .catch((err) => {
      notifications.handleError(err.response);
    });
};

export const search_sem = (param = {}) => (dispatch) => {
  httpClient
    .GET("/syllabus/input", false, param)
    .then((data) => {
      dispatch({ type: SEARCH_SEMESTER, payload: data.data });
    })
    .catch((err) => {
      notifications.handleError(err.response);
    });
};

export const search_subj = (param = {}) => (dispatch) => {
  httpClient
    .GET("/syllabus/input", false, param)
    .then((data) => {
      dispatch({ type: SEARCH_SUBJECT, payload: data.data });
    })
    .catch((err) => {
      notifications.handleError(err.response);
    });
};
