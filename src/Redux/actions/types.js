export const LOGIN_START = "LOGIN_START";
export const LOGIN_END = "LOGIN_END";
export const LOGIN_FAIL = "LOGIN_FAIL";
export const LOGIN_MODAL = "LOGIN_MODAL";
export const CREATE_USER = "CREATE_USER";
export const LOGOUT = "LOGOUT";
export const SIGN_UP = "SIGN_UP";
export const SIGING_UP = "SIGING_UP";
export const SIGNUP_FAIL = "SIGNUP_FAIL";
export const USER_LOGIN = "USER_LOGIN";
export const EDIT_PROFILE = "EDIT_PROFILE";
export const START_USER_UPDATE = "START_USER_UPDATE";
export const UPDATED_USER = "UPDATED_USER";
export const FAIL_UPDATE_USER = "FAIL_UPDATE_USER";
export const CHANGE_PROFILE_PIC = "CHANGE_PROFILE_PIC";
export const CHANGING_PROFILE_PIC = "CHANGING_PROFILE_PIC";
export const EDIT_PROFILE_CLOSE = "EDIT_PROFILE_CLOSE";
export const CHANGE_PASSWORD = "CHANGE_PASSWORD";
export const CHANGED_PASSWORD = "CHANGED_PASSWORD";
export const SENDING_EMAIL = "SENDING_EMAIL";
export const EMAIL_SEND = "EMAIL_SEND";
export const EMAIL_SEND_FAIL = "EMAIL_SEND_FAIL";
export const EMAIL_VERIFYING = "EMAIL_VERIFYING";
export const EMAIL_VERIFYED = "EMAIL_VERIFYED";
export const RESET_PASS = "RESET_PASS";
export const RESET_PASS_END = "RESET_PASS_END";
export const RESET_ENTER_PASSWORD = "RESET_ENTER_PASSWORD";
export const RESET_ENTER_PASSWORD_END = "RESET_ENTER_PASSWORD_END";

export const SOCIAL_REGISTER_MODAL = "SOCIAL_REGISTER_MODAL";
export const SOCIAL_PASS_BTN = "SOCIAL_PASS_BTN";
export const SOCIAL_PASS_END = "SOCIAL_PASS_END";

export const ADMIN_CHECK = "ADMIN_CHECK";
export const ADMIN_CREATE = "ADMIN_CREATE";
export const ADMIN_USER_FAIL = "ADMIN_USER_FAIL";

export const USERS_FETCH = "USERS_FETCH";
export const USERS_FETCHED = "USERS_FETCHED";

export const ADD_USER = "ADD_USER";
export const ADDED_USER = "ADDED_USER";
export const ADD_USER_FAIL = "ADD_USER_FAIL";
export const ADD_USER_MODAL = "ADD_USER_MODAL";

export const ADMIN_USER_UPDATE = "ADMIN_USER";
export const ADMIN_USER_UPDATED = "ADMIN_USER_UPDATED";
export const ADMIN_USER_UPDATE_FAIL = "ADMIN_USER_UPDATE_FAIL";
export const ADMIN_USER_UPDATE_MODAL = "ADMIN_USER_UPDATE_MODAL";

export const PUBLIC_USER_START = "PUBLIC_USER_START";
export const PUBLIC_USER_END = "PUBLIC_USER_END";

export const ADD_BLOG = "ADD_BLOG";
export const BLOG_ADDED = "BLOG_ADDED";
export const BLOG_ADD_FAIL = "BLOG_ADD_FAIL";
export const BLOG_MODAL = "BLOG_MODAL";

export const FETCH_BLOG = "FETCH_BLOG";
export const FETCHED_BLOG = "FETCHED_BLOG";

export const UPDATE_BLOG = "UPDATE_BLOG";
export const UPDATED_BLOG = "UPDATED_BLOG";
export const UPDATED_BLOG_FAIL = "UPDATED_BLOG_FAIL";
export const UPDATE_BLOG_ACTIVE = "UPDATE_BLOG_ACTIVE";

export const DELETE_BLOG = "DELETE_BLOG";

// -----------public blog-------------
export const PUBLIC_BLOG_FETCHING = "PUBLIC_BLOG_FETCHING";
export const PUBLIC_BLOG_FETCH = "PUBLIC_BLOG_FETCH";
export const PUBLIC_BLOG = "PUBLIC_BLOG";
export const PUBLIC_BLOG_FETCHED = "PUBLIC_BLOG_FAIL";

// ------------------About BCA-------------------
export const BCA_ADD_MODAL = "BCA_ADD_MODAL";
export const BCA_USER_FETCH_STR = "BCA_USER_FETCH_STR";
export const BCA_USER_FETCH_END = "BCA_USER_FETCH_END";
export const BCA_ADD = "BCA_ADD";
export const BCA_ADD_SUCCESS = "BCA_ADD_SUCCESS";
export const BCA_ADD_FAIL = "BCA_ADD_FAIL";
export const BCA_ADMIN = "BCA_ADMIN";
export const BCA_ADMIN_FETCH = "BCA_ADMIN_FETCH";
export const BCA_UPDATE_STR = "BCA_UPDATE_STR";
export const BCA_UPDATE_SUCCESS = "BCA_UPDATE_SUCCESS";
export const BCA_UPDATE_FAIL = "BCA_UPDATE_FAIL";
export const BCA_UPDATE_MODAL = "BCA_UPDATE_MODAL";
export const BCA_DELETED = "BCA_DELETED";
export const BCA_STATE = "BCA_STATE";
export const PUBLIC_BCA_FETCH = "PUBLIC_BCA_FETCH";
export const PUBLIC_BCA_START = "PUBLIC_BCA_START";

// -----------------College ---------------------
export const ADD_COLLEGE_MODAL = "ADD_COLLEGE_MODAL";
export const ADD_COLLEGE = "ADD_COLLEGE";
export const ADD_COLLEGE_SUCC = "ADD_COLLEGE_SUCC";
export const ADD_COLLEGE_FAIL = "ADD_COLLEGE_FAIL";
export const ADMIN_COLLEGE = "ADMIN_COLLEGE";
export const ADMIN_COLLEGE_FETCH = "ADMIN_COLLEGE_FETCH";
export const DELETE_COLLEGE = "DELETE_COLLEGE";
export const PUBLIC_FETCH_COLLEGE = "PUBLIC_FETCH_COLLEGE";
export const PUBLIC_FETCH_COLLEGE_END = "PUBLIC_FETCH_COLLEGE_END";
export const SINGLE_COLLEGE = "SINGLE_COLLEGE";
export const SINGLE_COLLEGE_END = "SINGLE_COLLEGE_FETCH";

// ------------------------Syllabus-----------------================
export const SYLLABUS_MODAL = "SYLLABUS_MODAL";
export const ADD_SYLLABUS = "ADD_SYLLABUS";
export const ADD_SYLLABUS_FAIL = "ADD_SYLLABUS_FAIL";
export const SYLLABUS_ADDED = "SYLLABUS_ADDED";

export const FETCH_SYLLABUS_STR = "FETCH_SYLLABUS_STR";
export const FETCH_SYLLABUS_END = "FETCH_SYLLABUS_END";
export const SYLLABUS_DELETED = "SYLLABUS_DELETED";

// -------------------------books-------------------------------------
export const BOOK_MODAL = "BOOK_MODAL";
export const ADD_BOOK = "ADD_BOOK";
export const ADD_BOOK_SUCC = "ADD_BOOK_SUCC";
export const ADD_BOOK_FAIL = "ADD_BOOK_FAIL";
export const FETCH_BOOK_STR = "FETCH_BOOK_STR";
export const FETCH_BOOK_SUCC = "FETCH_BOOK_SUCC";
export const BOOK_DELETED = "BOOK_DELETED";
// --------------------------Notes --------------------------------------\

export const NOTE_MODAL = "NOTE_MODAL";
export const ADD_NOTE = "ADD_NOTE";
export const ADD_NOTE_SUCC = "ADD_NOTE_SUCC";
export const ADD_NOTE_FAIL = "ADD_NOTE_FAIL";
export const FETCH_NOTE_STR = "FETCH_NOTE_STR";
export const FETCH_NOTE_END = "FETCH_NOTE_END";
export const UPDATE_NOTE = "UPDATE_NOTE";
export const UPDATED_NOTE = "UPDATED_NOTE";
export const UPDATE_NOTE_FAIL = "UPDATE_NOTE_FAIL";
export const DELETE_NOTE = "DELETE_NOTE";
export const SINGLE_FETCH_STR = "SINGLE_FETCH_STR";
export const SINGLE_FETCH_END = "SINGLE_FETCH_END";
// ---------------------Old Question-------------------
export const QUESTION_MODAL = "QUESTION_MODAL";
export const ADD_QUESTION = "ADD_QUESTION";
export const ADD_QUESTION_SUCC = "ADD_QUESTION_SUCC";
export const ADD_QUESTION_FAIL = "ADD_QUESTION_FAIL";
export const FETCH_QUESTION_STR = "FETCH_QUESTION_STR";
export const FETCH_QUESTION_END = "FETCH_QUESTION_END";
export const DELETE_QUESTION = "DELETE_QUESTION";
export const SINGLE_QUESTION = "SINGLE_QUESTION";
export const SINGLE_QUESTION_END = "SINGLE_QUESTION_END";
// ---------------------search --------------------------------
export const SEARCH_FETCH = "SEARCH_FETCH";
export const SEARCH_SEMESTER = "SEARCH_SEMESTER";
export const SEARCH_SUBJECT = "SEARCH_SUBJECT";

//  -----------------------slider --------------------------------

export const ADD_SLIDER = "ADD_SLIDER";
export const ADD_SLIDER_FAIL = "ADD_SLIDER_FAIL";
export const ADD_SLIDER_SUCC = "ADD_SLIDER_SUCC";
export const FETCH_SLIDER_STR = "FETCH_SLIDER_STR";
export const FETCH_SLIDER_END = "FETCH_SLIDER_END";
export const DELETE_SLIDER = "DELETE_SLIDER";

// -----------------Home---------------
export const TOP_COLLEGE_FETCH = "TOP_COLLEGE_FETCH";
export const TOP_COLLEGE_END = "TOP_COLLEGE_END";
// ----------------Dashboard --------------------

export const TOTAL_NOTE = "TOTAL_NOTE";
export const TOTAL_BLOG = "TOTAL_BLOG";
export const TOTAL_QUESTIION = "TOTAL_QUESTIION";
