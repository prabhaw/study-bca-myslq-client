import {
  SENDING_EMAIL,
  EMAIL_SEND,
  EMAIL_SEND_FAIL,
  EMAIL_VERIFYING,
  EMAIL_VERIFYED,
  USER_LOGIN,
} from "./../actions/types";
import httpClient from "../../utils/httpClient";
import notifications from "../../utils/notifications";

export const send_mail = () => (dispatch) => {
  dispatch({ type: SENDING_EMAIL });

  httpClient
    .GET("/user/sendemailverify", true, {})
    .then((user) => {
      notifications.showSuccess(`Email Send To ${user.data.email}`);
      dispatch({ type: EMAIL_SEND });
    })
    .catch((err) => {
      notifications.handleError(err.response);
      dispatch({ type: EMAIL_SEND_FAIL });
    });
};

export const vefify_code = (code) => (dispatch) => {
  dispatch({ type: EMAIL_VERIFYING });
  httpClient
    .PUT("/user/setverify", code, true)
    .then((user) => {
      dispatch({ type: EMAIL_VERIFYED });
      dispatch({
        type: USER_LOGIN,
        payload: user.data,
      });
      notifications.showSuccess("User Verified Success.");
    })
    .catch((err) => {
      dispatch({ type: EMAIL_VERIFYED });
      notifications.handleError(err.response);
    });
};
