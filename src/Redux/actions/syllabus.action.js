import {
  ADD_SYLLABUS,
  ADD_SYLLABUS_FAIL,
  SYLLABUS_MODAL,
  SYLLABUS_ADDED,
  FETCH_SYLLABUS_STR,
  FETCH_SYLLABUS_END,
  SYLLABUS_DELETED,
} from "./types";
import httpClient from "../../utils/httpClient";
import notifications from "../../utils/notifications";

export const add_modal = (action) => (dispatch) => {
  dispatch({ type: SYLLABUS_MODAL, payload: action });
};

export const add_syllabus = (data) => (dispatch) => {
  dispatch({ type: ADD_SYLLABUS });
  httpClient
    .POST("/syllabus", data, true)
    .then((data) => {
      dispatch({ type: SYLLABUS_ADDED, payload: data.data });
    })
    .catch((err) => {
      dispatch({ type: ADD_SYLLABUS_FAIL });
      notifications.handleError(err.response);
    });
};

export const fetch_syllabus = (pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: FETCH_SYLLABUS_STR });
  httpClient.GET("/syllabus", false, { pageNumber, pageSize }).then((data) => {
    dispatch({
      type: FETCH_SYLLABUS_END,
      payload: data.data.data,
      total: data.data.total,
      pageNumber: pageNumber,
    });
  });
};

export const search_syllabus = (data, pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: FETCH_SYLLABUS_STR });
  httpClient
    .GET("/syllabus/search", false, { pageNumber, pageSize, ...data })
    .then((data) => {
      dispatch({
        type: FETCH_SYLLABUS_END,
        payload: data.data.data,
        total: data.data.total,
        pageNumber: pageNumber,
      });
    });
};

export const delete_syllabus = (id) => (dispatch) => {
  httpClient
    .REMOVE(`/syllabus/${id}`, true)
    .then((data) => {
      dispatch({ type: SYLLABUS_DELETED, index: id });
      notifications.showSuccess("Syllabus Deleted.");
    })
    .catch((err) => {
      notifications.handleError(err.response);
    });
};
