import {
  ADD_SLIDER,
  ADD_SLIDER_FAIL,
  ADD_SLIDER_SUCC,
  FETCH_SLIDER_STR,
  FETCH_SLIDER_END,
  DELETE_SLIDER,
} from "./types";
import httpClient from "../../utils/httpClient";
import notifications from "../../utils/notifications";

export const add_slider = (data) => (dispatch) => {
  dispatch({ type: ADD_SLIDER });
  httpClient
    .POST("/slider", data, true)
    .then((data) => {
      dispatch({
        type: ADD_SLIDER_SUCC,
        payload: data.data,
      });
      notifications.showSuccess("SLider Added.");
    })
    .catch((err) => {
      notifications.handleError(err.response);
      dispatch({ type: ADD_SLIDER_FAIL });
    });
};

export const fetch_slider = () => (dispatch) => {
  dispatch({ type: FETCH_SLIDER_STR });
  httpClient
    .GET("/slider", false, {})
    .then((data) => {
      dispatch({ type: FETCH_SLIDER_END, payload: data.data });
    })
    .catch((err) => {
      notifications.handleError(err.response);
    });
};

export const delete_slider = (id) => (dispathc) => {
  httpClient
    .REMOVE(`/slider/${id}`, true)
    .then((data) => {
      dispathc({ type: DELETE_SLIDER, index: id });
    })
    .catch((err) => {
      notifications.handleError(err.response);
    });
};
