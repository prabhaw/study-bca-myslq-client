import { TOTAL_NOTE, TOTAL_BLOG, TOTAL_QUESTIION } from "./types";
import httpClient from "./../../utils/httpClient";

export const total_blog = () => (dispathc) => {
  httpClient.GET("/total/blog", true, {}).then((total) => {
    dispathc({ type: TOTAL_BLOG, payload: total.data });
  });
};

export const total_note = () => (dispathc) => {
  httpClient.GET("/total/note", true, {}).then((total) => {
    dispathc({ type: TOTAL_NOTE, payload: total.data });
  });
};

export const total_question = () => (dispathc) => {
  httpClient.GET("/total/question", true, {}).then((total) => {
    dispathc({ type: TOTAL_QUESTIION, payload: total.data });
  });
};
