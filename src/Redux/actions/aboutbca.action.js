import {
  BCA_ADD_MODAL,
  BCA_ADD,
  BCA_ADD_FAIL,
  BCA_ADD_SUCCESS,
  BCA_USER_FETCH_STR,
  BCA_USER_FETCH_END,
  BCA_ADMIN,
  BCA_ADMIN_FETCH,
  BCA_UPDATE_STR,
  BCA_UPDATE_SUCCESS,
  BCA_UPDATE_FAIL,
  BCA_UPDATE_MODAL,
  BCA_DELETED,
  BCA_STATE,
  PUBLIC_BCA_FETCH,
  PUBLIC_BCA_START,
} from "./types";
import httpClient from "./../../utils/httpClient";
import notifications from "./../../utils/notifications";

export const add_bca_modal = () => (dispatch) => {
  dispatch({ type: BCA_ADD_MODAL });
};
export const update_bca_modal = () => (dispatch) => {
  dispatch({ type: BCA_UPDATE_MODAL });
};
export const fetch_bca_user = () => (dispatch) => {
  dispatch({ type: BCA_USER_FETCH_STR });
  httpClient
    .GET("/bca", true, {})
    .then((data) => {
      dispatch({ type: BCA_USER_FETCH_END, payload: data.data });
    })
    .catch((err) => {
      notifications.handleError(err.response);
    });
};

export const add_about_bca = (data) => (dispatch) => {
  dispatch({ type: BCA_ADD });
  httpClient
    .POST("/bca", data, true)
    .then((done) => {
      dispatch({ type: BCA_ADD_SUCCESS, payload: done.data });
    })
    .catch((err) => {
      notifications.handleError(err.response);
      dispatch({ type: BCA_ADD_FAIL });
    });
};

export const update_about_bca = (id, data) => (dispatch) => {
  dispatch({ type: BCA_UPDATE_STR });
  httpClient
    .PUT(`/bca/${id}`, data, true)
    .then((data) => {
      dispatch({ type: BCA_UPDATE_SUCCESS, payload: data.data });
    })
    .catch((err) => {
      notifications.handleError(err.response);
      dispatch({ type: BCA_UPDATE_FAIL });
    });
};

export const delete_aboutbca = (id) => (dispatch) => {
  httpClient
    .REMOVE(`/bca/${id}`, true)
    .then(() => {
      dispatch({ type: BCA_DELETED });
    })
    .catch((err) => {
      notifications.handleError(err.response);
    });
};

export const admin_fetch = (pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: BCA_ADMIN });
  httpClient
    .GET("/bca/admin", true, { pageNumber, pageSize })
    .then((data) => {
      dispatch({
        type: BCA_ADMIN_FETCH,
        payload: data.data.data,
        total: data.data.total,
        pageNumber: pageNumber,
      });
    })
    .catch((err) => {});
};

export const admin_search = (search, pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: BCA_ADMIN });
  httpClient
    .GET("/bca/search", true, { search, pageNumber, pageSize })
    .then((data) => {
      dispatch({
        type: BCA_ADMIN_FETCH,
        payload: data.data.data,
        total: data.data.total,
        pageNumber: pageNumber,
      });
    })
    .catch((err) => {});
};

export const update_aboutbca_admin = (id, data) => (dispatch) => {
  httpClient
    .PUT(`/bca/${id}`, data, true)
    .then((data) => {
      dispatch({ type: BCA_STATE, payload: data.data, id });
    })
    .catch((err) => {
      notifications.handleError(err.response);
    });
};

export const public_fetch = (pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: PUBLIC_BCA_START });
  httpClient
    .GET("/bca/public", false, { pageNumber, pageSize })
    .then((data) => {
      dispatch({
        type: PUBLIC_BCA_FETCH,
        payload: data.data.data,
        total: data.data.total,
        pageNumber: pageNumber,
      });
    })
    .catch((err) => {});
};
