import {
  USER_LOGIN,
  START_USER_UPDATE,
  UPDATED_USER,
  FAIL_UPDATE_USER,
  CHANGE_PROFILE_PIC,
  CHANGING_PROFILE_PIC,
  CHANGE_PASSWORD,
  CHANGED_PASSWORD,
} from "./types";
import httpClient from "../../utils/httpClient";
import notifications from "../../utils/notifications";

export const fetch_user = (history) => (dispatch) => {
  httpClient
    .GET("/user/id", true, {})
    .then((user) => {
      dispatch({
        type: USER_LOGIN,
        payload: user.data,
      });
    })
    .catch((err) => {
      notifications.handleError(err.response);
      localStorage.clear();
      history.push("/");
    });
};

export const update_user = (data) => (dispatch) => {
  dispatch({ type: START_USER_UPDATE });

  httpClient
    .PUT("/user/id", data, true)
    .then((user) => {
      dispatch({ type: UPDATED_USER });

      dispatch({
        type: USER_LOGIN,
        payload: user.data,
      });
      notifications.showSuccess("Information Has Been Updated.");
    })
    .catch((err) => {
      notifications.handleError(err.response);
      dispatch({ type: FAIL_UPDATE_USER });
    });
};

export const change_profile_pic = (data) => (dispatch) => {
  dispatch({ type: CHANGING_PROFILE_PIC });
  httpClient
    .PUT("/user/profilepic", data, true)
    .then((user) => {
      dispatch({
        type: USER_LOGIN,
        payload: user.data,
      });
      dispatch({ type: CHANGE_PROFILE_PIC });
    })
    .catch((err) => {
      dispatch({ type: CHANGE_PROFILE_PIC });
      notifications.handleError(err.response);
    });
};

export const update_password = (data) => (dispatch) => {
  dispatch({ type: CHANGE_PASSWORD });

  httpClient
    .PUT("/user/id", data, true)
    .then((user) => {
      dispatch({ type: CHANGED_PASSWORD });
      notifications.showSuccess("Password Changed.");
    })
    .catch((err) => {
      notifications.handleError(err.response);
      dispatch({ type: CHANGED_PASSWORD });
    });
};
