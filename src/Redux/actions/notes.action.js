import {
  NOTE_MODAL,
  ADD_NOTE,
  ADD_NOTE_SUCC,
  ADD_NOTE_FAIL,
  FETCH_NOTE_STR,
  FETCH_NOTE_END,
  UPDATE_NOTE,
  UPDATED_NOTE,
  UPDATE_NOTE_FAIL,
  DELETE_NOTE,
  SINGLE_FETCH_STR,
  SINGLE_FETCH_END,
} from "./types";
import httpClient from "../../utils/httpClient";
import notifications from "../../utils/notifications";

export const note_modal = () => (dispatch) => {
  dispatch({ type: NOTE_MODAL });
};
export const add_note = (data) => (dispatch) => {
  dispatch({ type: ADD_NOTE });
  httpClient
    .POST("/notes", data, true)
    .then((data) => {
      dispatch({ type: ADD_NOTE_SUCC, payload: data.data });

      notifications.showSuccess("Note Successfully Added.");
    })
    .catch((err) => {
      dispatch({ type: ADD_NOTE_FAIL });
      notifications.handleError(err.response);
    });
};

export const fetch_admin = (pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: FETCH_NOTE_STR });
  httpClient
    .GET("/notes/userfetch", true, { pageNumber, pageSize })
    .then((data) => {
      dispatch({
        type: FETCH_NOTE_END,
        payload: data.data.data,
        total: data.data.total,
        pageNumber,
      });
    });
};

export const update_note = (id, data) => (dispatch) => {
  dispatch({ type: UPDATE_NOTE });
  httpClient
    .PUT(`/notes/${id}`, data, true)
    .then((data) => {
      dispatch({ type: UPDATED_NOTE, payload: data.data, id });
      notifications.showSuccess("Your Note is Updated.");
    })
    .catch((err) => {
      dispatch({ type: UPDATE_NOTE_FAIL });
      notifications.handleError(err.response);
    });
};

export const delete_note = (id) => (dispatch) => {
  httpClient
    .REMOVE(`/notes/${id}`, true)
    .then((data) => {
      dispatch({ type: DELETE_NOTE, index: id });
      notifications.showSuccess("Note Deleted.");
    })
    .catch((err) => {
      notifications.handleError(err.response);
    });
};

export const public_fetch = (pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: FETCH_NOTE_STR });
  httpClient.GET("/notes", false, { pageNumber, pageSize }).then((data) => {
    dispatch({
      type: FETCH_NOTE_END,
      payload: data.data.data,
      total: data.data.total,
      pageNumber,
    });
  });
};

export const search_note = (data, pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: FETCH_NOTE_STR });
  httpClient
    .GET("/notes/search", false, { pageNumber, pageSize, ...data })
    .then((data) => {
      dispatch({
        type: FETCH_NOTE_END,
        payload: data.data.data,
        total: data.data.total,
        pageNumber,
      });
    });
};

export const fetch_singel_note = (id) => (dispatch) => {
  dispatch({ type: SINGLE_FETCH_STR });
  httpClient
    .GET(`/notes/${id}`, false, {})
    .then((data) => {
      dispatch({ type: SINGLE_FETCH_END, payload: data.data });
    })
    .catch((err) => {
      dispatch({ type: SINGLE_FETCH_END, payload: false });
    });
};
