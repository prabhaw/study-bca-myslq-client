import {
  FETCHED_BLOG,
  FETCH_BLOG,
  ADD_BLOG,
  BLOG_ADDED,
  BLOG_ADD_FAIL,
  BLOG_MODAL,
  UPDATE_BLOG,
  UPDATED_BLOG,
  UPDATED_BLOG_FAIL,
  DELETE_BLOG,
  UPDATE_BLOG_ACTIVE,
} from "./types";
import httpClient from "../../utils/httpClient";
import notifications from "./../../utils/notifications";
export const add_blog_modal = () => (dispatch) => {
  dispatch({ type: BLOG_MODAL });
};

export const fetch_dash_blog = (pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: FETCH_BLOG });
  httpClient.GET("/blog", true, { pageNumber, pageSize }).then((data) => {
    dispatch({
      type: FETCHED_BLOG,
      payload: data.data.data,
      total: data.data.total,
      pageNumber: pageNumber,
    });
  });
};

export const search_blog_admin = (search, pageNumber, pageSize) => (
  dispatch
) => {
  dispatch({ type: FETCH_BLOG });
  httpClient
    .GET("/blog/adminsearch", true, { pageNumber, pageSize, search })
    .then((data) => {
      dispatch({
        type: FETCHED_BLOG,
        payload: data.data.data,
        total: data.data.total,
        pageNumber: pageNumber,
      });
    })
    .catch((err) => {});
};

export const add_blog = (data) => (dispatch) => {
  dispatch({ type: ADD_BLOG });
  httpClient
    .POST("/blog", data, true)
    .then((data) => {
      dispatch({ type: BLOG_ADDED, payload: data.data });
    })
    .catch((err) => {
      notifications.handleError(err.response);
      dispatch({ type: BLOG_ADD_FAIL });
    });
};

export const update_blog = (id, data) => (dispatch) => {
  dispatch({ type: UPDATE_BLOG });
  httpClient
    .PUT(`/blog/${id}`, data, true)
    .then((data) => {
      dispatch({ type: UPDATED_BLOG, payload: data.data, id });
    })
    .catch((err) => {
      dispatch({ type: UPDATED_BLOG_FAIL });
      notifications.handleError(err.response);
    });
};
export const update_blog_active = (id, data) => (dispatch) => {
  httpClient
    .PUT(`/blog/${id}`, data, true)
    .then((data) => {
      dispatch({ type: UPDATE_BLOG_ACTIVE, payload: data.data, id });
    })
    .catch((err) => {
      notifications.handleError(err.response);
    });
};
export const delete_blog = (id) => (dispatch) => {
  httpClient
    .REMOVE(`/blog/${id}`, true)
    .then((data) => {
      dispatch({ type: DELETE_BLOG, index: id });
      notifications.showSuccess("Blog Deleted Success");
    })
    .catch((err) => {
      notifications.handleError(err.response);
    });
};
