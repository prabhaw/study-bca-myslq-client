import { ADMIN_CHECK, ADMIN_CREATE, ADMIN_USER_FAIL } from "./types";
import httpClient from "../../utils/httpClient";
import notifications from "../../utils/notifications";

export const check_admin = () => (dispatch) => {
  httpClient
    .GET("user/checkadmin", false)
    .then((data) => {
      dispatch({ type: ADMIN_CHECK, payload: false });
    })
    .catch((err) => {
      dispatch({ type: ADMIN_CHECK, payload: true });
      notifications.handleError(err.response);
    });
};

export const create_admin = (data, history) => (dispatch) => {
  httpClient
    .POST("user/checkadmin", data, false)
    .then((data) => {
      dispatch({ type: ADMIN_CREATE });
      notifications.showSuccess("Admin is Created.");
      history.push("/");
    })
    .catch((err) => {
      notifications.handleError(err.response);
      dispatch({ type: ADMIN_USER_FAIL });
    });
};
