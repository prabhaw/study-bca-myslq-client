import {
  QUESTION_MODAL,
  ADD_QUESTION,
  ADD_QUESTION_SUCC,
  ADD_QUESTION_FAIL,
  FETCH_QUESTION_STR,
  FETCH_QUESTION_END,
  DELETE_QUESTION,
  SINGLE_QUESTION,
  SINGLE_QUESTION_END,
} from "./../actions/types";
import httpClient from "../../utils/httpClient";
import notifications from "../../utils/notifications";

export const add_question_modal = () => (dispatch) => {
  dispatch({ type: QUESTION_MODAL });
};

export const add_question = (data) => (dispatch) => {
  dispatch({ type: ADD_QUESTION });
  httpClient
    .POST("/questions", data, true)
    .then((data) => {
      dispatch({ type: ADD_QUESTION_SUCC, payload: data.data });
      notifications.showSuccess("Question Added Successfully.");
    })
    .catch((err) => {
      dispatch({ type: ADD_QUESTION_FAIL });
      //   notifications.handleError(err.response);
      console.log(err);
    });
};

export const fetch_admin = (pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: FETCH_QUESTION_STR });
  httpClient
    .GET("/questions/userfetch", true, { pageNumber, pageSize })
    .then((data) => {
      dispatch({
        type: FETCH_QUESTION_END,
        payload: data.data.data,
        total: data.data.total,
        pageNumber,
      });
    });
};

export const delete_question = (id) => (dispatch) => {
  httpClient
    .REMOVE(`/questions/${id}`, true)
    .then((data) => {
      dispatch({ type: DELETE_QUESTION, index: id });
      notifications.showSuccess("Question Deleted.");
    })
    .catch((err) => {
      notifications.handleError(err.response);
    });
};

export const public_fetch = (pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: FETCH_QUESTION_STR });
  httpClient.GET("/questions", false, { pageNumber, pageSize }).then((data) => {
    dispatch({
      type: FETCH_QUESTION_END,
      payload: data.data.data,
      total: data.data.total,
      pageNumber,
    });
  });
};

export const public_fetch_home = (pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: FETCH_QUESTION_STR });
  httpClient.GET("/questions", false, { pageNumber, pageSize }).then((data) => {
    dispatch({
      type: FETCH_QUESTION_END,
      payload: data.data.data,
      total: data.data.total,
      pageNumber,
    });
  });
};

export const search_question = (data, pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: FETCH_QUESTION_STR });
  httpClient
    .GET("/questions/search", false, { pageNumber, pageSize, ...data })
    .then((data) => {
      dispatch({
        type: FETCH_QUESTION_END,
        payload: data.data.data,
        total: data.data.total,
        pageNumber,
      });
    });
};

export const fetch_singel_question = (id) => (dispatch) => {
  dispatch({ type: SINGLE_QUESTION });
  httpClient
    .GET(`/questions/${id}`, false, {})
    .then((data) => {
      dispatch({ type: SINGLE_QUESTION_END, payload: data.data });
    })
    .catch((err) => {
      dispatch({ type: SINGLE_QUESTION_END, payload: false });
    });
};
