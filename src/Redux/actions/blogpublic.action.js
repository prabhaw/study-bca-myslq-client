import {
  PUBLIC_BLOG_FETCHING,
  PUBLIC_BLOG_FETCH,
  PUBLIC_BLOG,
  PUBLIC_BLOG_FETCHED,
} from "./types";
import httpClient from "../../utils/httpClient";

export const fetch_public_blog = (pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: PUBLIC_BLOG_FETCHING });
  httpClient
    .GET("/blog/publicfetch", false, { pageNumber, pageSize })
    .then((data) => {
      dispatch({
        type: PUBLIC_BLOG_FETCH,
        payload: data.data.data,
        total: data.data.total,
        pageNumber: pageNumber,
      });
    });
};

export const fetch_public_blog_search = (search, pageNumber, pageSize) => (
  dispatch
) => {
  dispatch({ type: PUBLIC_BLOG_FETCHING });
  httpClient
    .GET("/blog/publicsearch", false, { search, pageNumber, pageSize })
    .then((data) => {
      dispatch({
        type: PUBLIC_BLOG_FETCH,
        payload: data.data.data,
        total: data.data.total,
        pageNumber: pageNumber,
      });
    });
};

export const fetch_blog = (id) => (dispatch) => {
  dispatch({ type: PUBLIC_BLOG });
  httpClient
    .GET(`/blog/publicblog/${id}`)
    .then((data) => {
      dispatch({
        type: PUBLIC_BLOG_FETCHED,
        payload: data.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: PUBLIC_BLOG_FETCHED,
        payload: false,
      });
    });
};
