import {
  ADD_COLLEGE_MODAL,
  ADD_COLLEGE,
  ADD_COLLEGE_SUCC,
  ADD_COLLEGE_FAIL,
  ADMIN_COLLEGE,
  ADMIN_COLLEGE_FETCH,
  DELETE_COLLEGE,
  PUBLIC_FETCH_COLLEGE,
  PUBLIC_FETCH_COLLEGE_END,
  SINGLE_COLLEGE,
  SINGLE_COLLEGE_END,
} from "./types";
import httpClient from "../../utils/httpClient";
import notifications from "../../utils/notifications";

export const college_modal = () => (dispathc) => {
  dispathc({ type: ADD_COLLEGE_MODAL });
};

export const add_college = (data) => (dispatch) => {
  dispatch({ type: ADD_COLLEGE });

  httpClient
    .POST("/college", data, false)
    .then((data) => {
      dispatch({ type: ADD_COLLEGE_SUCC });
      notifications.showSuccess("College Has Been Added.");
    })
    .catch((err) => {
      dispatch({ type: ADD_COLLEGE_FAIL });
    });
};

export const admin_college_fetch = (pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: ADMIN_COLLEGE });
  httpClient
    .GET("/college/adminfetch", true, { pageNumber, pageSize })
    .then((data) => {
      dispatch({
        type: ADMIN_COLLEGE_FETCH,
        payload: data.data.data,
        total: data.data.total,
        pageNumber: pageNumber,
      });
    });
};

export const search_admin_college = (search, pageNumber, pageSize) => (
  dispatch
) => {
  dispatch({ type: ADMIN_COLLEGE });
  httpClient
    .GET("/college/adminsearch", true, { pageNumber, pageSize, search })
    .then((data) => {
      dispatch({
        type: ADMIN_COLLEGE_FETCH,
        payload: data.data.data,
        total: data.data.total,
        pageNumber: pageNumber,
      });
    });
};

export const on_college_active = (id, data) => (dispatch) => {
  httpClient
    .PUT(`/college/${id}`, data, true)
    .then((data) => {
      notifications.showSuccess(data.data.msg);
    })
    .catch((err) => {
      notifications.handleError(err.response);
    });
};

export const on_college_delete = (id) => (dispathc) => {
  httpClient
    .REMOVE(`/college/${id}`, true)
    .then((data) => {
      dispathc({ type: DELETE_COLLEGE, index: id });
      notifications.showSuccess(data.data.msg);
    })
    .catch((err) => {
      notifications.handleError(err.response);
    });
};

export const public_college_fetch = (pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: PUBLIC_FETCH_COLLEGE });
  httpClient.GET("/college", false, { pageNumber, pageSize }).then((data) => {
    dispatch({
      type: PUBLIC_FETCH_COLLEGE_END,
      payload: data.data.data,
      total: data.data.total,
      pageNumber: pageNumber,
    });
  });
};
export const public_search_college = (search, pageNumber, pageSize) => (
  dispatch
) => {
  dispatch({ type: PUBLIC_FETCH_COLLEGE });
  httpClient
    .GET("/college/publicsearch", false, { pageNumber, pageSize, search })
    .then((data) => {
      dispatch({
        type: PUBLIC_FETCH_COLLEGE_END,
        payload: data.data.data,
        total: data.data.total,
        pageNumber: pageNumber,
      });
    });
};

export const singel_fetch = (id) => (dispatch) => {
  dispatch({ type: SINGLE_COLLEGE });
  httpClient
    .GET(`/college/${id}`, false, {})
    .then((data) => {
      dispatch({ type: SINGLE_COLLEGE_END, payload: data.data });
    })
    .catch((err) => {
      dispatch({ type: SINGLE_COLLEGE_END, payload: false });
    });
};
