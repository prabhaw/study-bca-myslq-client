import {
  USERS_FETCH,
  USERS_FETCHED,
  ADD_USER,
  ADDED_USER,
  ADD_USER_FAIL,
  ADD_USER_MODAL,
  ADMIN_USER_UPDATE,
  ADMIN_USER_UPDATED,
  ADMIN_USER_UPDATE_FAIL,
  ADMIN_USER_UPDATE_MODAL,
  PUBLIC_USER_START,
  PUBLIC_USER_END,
} from "./types";
import httpClient from "./../../utils/httpClient";
import notifications from "../../utils/notifications";

export const add_modal_fun = () => (dispatch) => {
  dispatch({ type: ADD_USER_MODAL });
};
export const update_model_fun = () => (dispatch) => {
  dispatch({ type: ADMIN_USER_UPDATE_MODAL });
};
export const fetch_users = (pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: USERS_FETCH });
  httpClient
    .GET("/user", true, { pageNumber, pageSize })
    .then((data) => {
      dispatch({
        type: USERS_FETCHED,
        payload: data.data.data,
        total: data.data.total,
        pageNumber: pageNumber,
      });
    })
    .catch((err) => {});
};

export const add_user = (data) => (dispatch) => {
  dispatch({ type: ADD_USER });
  httpClient
    .POST("/user", data, false)
    .then((data) => {
      dispatch({ type: ADDED_USER, payload: data.data });
    })
    .catch((err) => {
      notifications.handleError(err.response);
      dispatch({ type: ADD_USER_FAIL });
    });
};

export const user_udate = (id, data) => (dispatch) => {
  dispatch({ type: ADMIN_USER_UPDATE });
  httpClient
    .PUT(`/user/${id}`, data, true)
    .then((data) => {
      dispatch({ type: ADMIN_USER_UPDATED, payload: data.data, id });
    })
    .catch((err) => {
      dispatch({ type: ADMIN_USER_UPDATE_FAIL });
      notifications.handleError(err.response);
    });
};

export const search_users = (search, pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: USERS_FETCH });
  httpClient
    .GET("/user/search", true, { pageNumber, pageSize, search })
    .then((data) => {
      dispatch({
        type: USERS_FETCHED,
        payload: data.data.data,
        total: data.data.total,
        pageNumber: pageNumber,
      });
    })
    .catch((err) => {});
};

export const fetch_public_users = (pageNumber, pageSize) => (dispatch) => {
  dispatch({ type: PUBLIC_USER_START });
  httpClient
    .GET("/user/publicuser", false, { pageNumber, pageSize })
    .then((data) => {
      dispatch({
        type: PUBLIC_USER_END,
        payload: data.data.data,
        total: data.data.total,
        pageNumber: pageNumber,
      });
    })
    .catch((err) => {});
};
