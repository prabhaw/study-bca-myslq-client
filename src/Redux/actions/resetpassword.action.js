import {
  RESET_PASS,
  RESET_PASS_END,
  RESET_ENTER_PASSWORD,
  RESET_ENTER_PASSWORD_END,
} from "./types";
import httpClient from "../../utils/httpClient";
import notifications from "../../utils/notifications";

export const send_reset_email = (email, history) => (dispatch) => {
  dispatch({ type: RESET_PASS });

  httpClient
    .PUT("/user/reset-password", email, false)
    .then(() => {
      dispatch({ type: RESET_PASS_END });
      history.push("/reset-password-end");
    })
    .catch((err) => {
      dispatch({ type: RESET_PASS_END });
      notifications.handleError(err.response);
    });
};

export const reset_password = (password, token, history) => (dispatch) => {
  dispatch({ type: RESET_ENTER_PASSWORD });
  httpClient
    .PUT(`/user/reset-pass-with-email/${token}`, password, false)
    .then(() => {
      dispatch({ type: RESET_ENTER_PASSWORD_END });
      history.push("/");
      notifications.showSuccess("Password Reset Success.");
    })
    .catch((err) => {
      dispatch({ type: RESET_ENTER_PASSWORD_END });
      notifications.handleError(err.response);
    });
};
