import {
  ADD_SYLLABUS,
  ADD_SYLLABUS_FAIL,
  SYLLABUS_MODAL,
  SYLLABUS_ADDED,
  FETCH_SYLLABUS_STR,
  FETCH_SYLLABUS_END,
  SYLLABUS_DELETED,
} from "./../actions/types";

const initialState = {
  syllabus_modal: false,
  add_btn: false,
  syllabus: [],
  resetform: false,
  syllabus_total: 0,
  loading: false,
  pageNumber: 1,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_SYLLABUS_STR:
      return {
        ...state,
        loading: true,
      };
    case FETCH_SYLLABUS_END:
      return {
        ...state,
        loading: false,
        syllabus: action.payload,
        syllabus_total: action.total,
        pageNumber: action.pageNumber,
      };

    case SYLLABUS_MODAL:
      return {
        ...state,
        syllabus_modal: action.payload,
      };
    case ADD_SYLLABUS:
      return {
        ...state,
        add_btn: true,
        resetform: false,
      };
    case ADD_SYLLABUS_FAIL:
      return {
        ...state,
        add_btn: false,
      };

    case SYLLABUS_ADDED:
      return {
        ...state,
        resetform: true,
        syllabus_modal: false,
        add_btn: false,
        syllabus: [action.payload, ...state.syllabus],
        syllabus_total: state.syllabus_total + 1,
      };

    case SYLLABUS_DELETED:
      const itemDeleted = state.syllabus.filter(
        (item) => item.id !== action.index
      );
      return {
        ...state,
        syllabus_total: state.syllabus_total - 1,
        syllabus: itemDeleted,
      };

    default:
      return {
        ...state,
      };
  }
}

export default reducer;
