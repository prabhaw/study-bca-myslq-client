import { TOTAL_NOTE, TOTAL_BLOG, TOTAL_QUESTIION } from "./../actions/types";

const initialState = {
  totalnote: 0,
  totalpost: 0,
  totalquestion: 0,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case TOTAL_NOTE:
      return {
        ...state,
        totalnote: action.payload,
      };
    case TOTAL_BLOG:
      return {
        ...state,
        totalpost: action.payload,
      };
    case TOTAL_QUESTIION:
      return {
        ...state,
        totalquestion: action.payload,
      };
    default:
      return {
        ...state,
      };
  }
}

export default reducer;
