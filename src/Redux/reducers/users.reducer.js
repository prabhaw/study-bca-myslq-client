import {
  USERS_FETCH,
  USERS_FETCHED,
  ADD_USER,
  ADDED_USER,
  ADD_USER_FAIL,
  ADD_USER_MODAL,
  ADMIN_USER_UPDATE,
  ADMIN_USER_UPDATED,
  ADMIN_USER_UPDATE_FAIL,
  ADMIN_USER_UPDATE_MODAL,
  PUBLIC_USER_START,
  PUBLIC_USER_END,
} from "./../actions/types";

const initialState = {
  fetching_user: false,
  users: [],
  total: 0,
  pageNumber: 1,
  add_btn: false,
  add_modal: false,
  formreset: false,
  update_modal: false,
  update_btn: false,
  public_user: [],
  public_total: 0,
  public_pageNum: 1,
  public_fetch: false,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case PUBLIC_USER_START:
      return {
        ...state,
        public_fetch: true,
      };
    case PUBLIC_USER_END:
      return {
        ...state,
        public_fetch: false,
        public_user: action.payload,
        public_total: action.total,
        public_pageNum: action.pageNumber,
      };
    case ADMIN_USER_UPDATE_MODAL:
      return {
        ...state,
        update_modal: false,
      };

    case ADMIN_USER_UPDATE:
      return {
        ...state,
        update_btn: true,
      };
    case ADMIN_USER_UPDATED:
      const data = state.users.map((item, index) => {
        if (item.id !== action.id) {
          return item;
        } else {
          return {
            ...item,
            ...action.payload,
          };
        }
      });

      return {
        ...state,
        update_modal: true,
        update_btn: false,
        users: data,
      };

    case ADMIN_USER_UPDATE_FAIL:
      return {
        ...state,
        update_btn: false,
      };
    case ADD_USER_MODAL:
      return {
        ...state,
        add_modal: !state.add_modal,
      };
    case ADD_USER:
      return {
        ...state,
        add_btn: true,
      };
    case ADDED_USER:
      return {
        ...state,
        add_btn: false,
        add_modal: false,
        formreset: true,
        public_total: state.public_total + 1,
        users: [action.payload, ...state.users],
      };
    case ADD_USER_FAIL:
      return {
        ...state,
        add_btn: false,
      };
    case USERS_FETCH:
      return {
        ...state,
        fetching_user: true,
      };
    case USERS_FETCHED:
      return {
        ...state,
        fetching_user: false,
        users: action.payload,
        total: action.total,
        pageNumber: action.pageNumber,
      };
    default:
      return {
        ...state,
      };
  }
}

export default reducer;
