import {
  EDIT_PROFILE,
  EDIT_PROFILE_CLOSE,
  START_USER_UPDATE,
  UPDATED_USER,
  FAIL_UPDATE_USER,
  CHANGE_PROFILE_PIC,
  CHANGING_PROFILE_PIC,
  CHANGE_PASSWORD,
  CHANGED_PASSWORD,
} from "../actions/types";

const initialState = {
  edit_profile: false,
  update_btn: false,
  disable_profile_input: false,
  password_btn: false,
};

function editprofilereducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_PASSWORD:
      return {
        ...state,
        password_btn: true,
      };

    case CHANGED_PASSWORD:
      return {
        ...state,
        password_btn: false,
      };

    case CHANGING_PROFILE_PIC:
      return {
        ...state,
        disable_profile_input: true,
      };
    case CHANGE_PROFILE_PIC:
      return {
        ...state,
        disable_profile_input: false,
      };
    case START_USER_UPDATE:
      return {
        ...state,
        update_btn: true,
      };
    case UPDATED_USER:
      return {
        ...state,
        update_btn: false,
        edit_profile: false,
      };

    case FAIL_UPDATE_USER:
      return {
        ...state,
        update_btn: false,
      };
    case EDIT_PROFILE:
      return {
        ...state,
        edit_profile: !state.edit_profile,
      };
    case EDIT_PROFILE_CLOSE:
      return {
        ...state,
        edit_profile: false,
      };
    default: {
      return { ...state };
    }
  }
}

export default editprofilereducer;
