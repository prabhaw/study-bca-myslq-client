import {
  QUESTION_MODAL,
  ADD_QUESTION,
  ADD_QUESTION_SUCC,
  ADD_QUESTION_FAIL,
  FETCH_QUESTION_STR,
  FETCH_QUESTION_END,
  DELETE_QUESTION,
  SINGLE_QUESTION,
  SINGLE_QUESTION_END,
} from "./../actions/types";

const initialState = {
  modal: false,
  questions: [],
  add_btn: false,
  loading: false,
  formreset: false,
  total: 0,
  pageNumber: 1,
  single_load: false,
  question: {},
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case QUESTION_MODAL:
      return {
        ...state,
        modal: !state.modal,
      };
    case ADD_QUESTION:
      return {
        ...state,
        add_btn: true,
        formreset: false,
      };
    case ADD_QUESTION_FAIL:
      return {
        ...state,
        add_btn: false,
      };
    case ADD_QUESTION_SUCC:
      return {
        ...state,
        add_btn: false,
        formreset: true,
        modal: false,
        total: state.total + 1,
        questions: [action.payload, ...state.questions],
      };

    case FETCH_QUESTION_STR:
      return {
        ...state,
        loading: true,
      };
    case FETCH_QUESTION_END:
      return {
        ...state,
        loading: false,
        questions: action.payload,
        total: action.total,
        pageNumber: action.pageNumber,
      };

    case DELETE_QUESTION:
      const itemDeleted = state.questions.filter(
        (item) => item.id !== action.index
      );
      return {
        ...state,
        questions: itemDeleted,
        total: state.total - 1,
      };
    case SINGLE_QUESTION:
      return {
        ...state,
        single_load: true,
      };
    case SINGLE_QUESTION_END:
      return {
        ...state,
        single_load: false,
        question: action.payload,
      };
    default:
      return {
        ...state,
      };
  }
}

export default reducer;
