import { combineReducers } from "redux";
import loginreducer from "./login.reducer";
import editprofile from "./Edituser.reducers";
import email from "./verifyemail.reducer";
import resetpass from "./resetpassword.reducer";
import socialreg from "./socialaccoutn.reducer";
import adminSignup from "./adminsignup.reducer";
import users from "./users.reducer";
import blogs from "./blogs.reducer";
import publicblog from "./blogpublic.reducer";
import bca from "./aboutbca.reducer";
import college from "./college.reducer";
import syllabus from "./syllabus.reducer";
import home from "./home.reducer";
import search from "./search.reducer";
import books from "./books.reducer";
import notes from "./notes.reducer";
import question from "./question.reducer";
import slider from "./slider.reducer";
import total from "./total.reducer";
export default combineReducers({
  auth: loginreducer,
  editprofile: editprofile,
  verifyemail: email,
  resetpass,
  socialreg,
  admin: adminSignup,
  users,
  blogs,
  publicblog,
  bca,
  college,
  home,
  syllabus,
  search,
  books,
  notes,
  question,
  slider,
  total,
});
