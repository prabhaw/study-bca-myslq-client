import {
  BCA_ADD_MODAL,
  BCA_ADD,
  BCA_ADD_FAIL,
  BCA_ADD_SUCCESS,
  BCA_USER_FETCH_STR,
  BCA_USER_FETCH_END,
  BCA_ADMIN,
  BCA_ADMIN_FETCH,
  BCA_UPDATE_STR,
  BCA_UPDATE_SUCCESS,
  BCA_UPDATE_FAIL,
  BCA_UPDATE_MODAL,
  BCA_DELETED,
  BCA_STATE,
  PUBLIC_BCA_FETCH,
  PUBLIC_BCA_START,
} from "./../actions/types";

const initialState = {
  add_model: false,
  loading: false,
  add_btn: false,
  update_btn: false,
  update_modal: false,
  aboutbca: {},
  all_aboutbca: [],
  admin_loading: false,
  total: 0,
  pageNumber: 1,
  public_bca: [],
  public_loading: false,
  public_total: 0,
  public_pageNumber: 1,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case BCA_ADMIN:
      return {
        ...state,
        admin_loading: true,
      };
    case PUBLIC_BCA_START:
      return {
        ...state,
        public_loading: true,
      };

    case PUBLIC_BCA_FETCH:
      return {
        ...state,
        public_loading: false,
        public_bca: action.payload,
        public_total: action.total,
        public_pageNumber: action.pageNumber,
      };
    case BCA_ADMIN_FETCH:
      return {
        ...state,
        admin_loading: false,
        all_aboutbca: action.payload,
        total: action.total,
        pageNumber: action.pageNumber,
      };
    case BCA_DELETED:
      return {
        ...state,
        aboutbca: null,
      };
    case BCA_UPDATE_MODAL:
      return {
        ...state,
        update_modal: !state.update_modal,
      };

    case BCA_ADD:
      return {
        ...state,
        add_btn: true,
        resetform: false,
      };
    case BCA_ADD_SUCCESS:
      return {
        ...state,
        add_btn: false,
        add_model: false,
        aboutbca: action.payload,
      };
    case BCA_ADD_FAIL:
      return {
        ...state,
        add_btn: false,
      };
    case BCA_ADD_MODAL:
      return {
        ...state,
        add_model: !state.add_model,
      };
    case BCA_USER_FETCH_STR:
      return {
        ...state,
        loading: true,
      };
    case BCA_USER_FETCH_END:
      return {
        ...state,
        loading: false,
        aboutbca: action.payload,
      };

    case BCA_UPDATE_STR:
      return {
        ...state,
        update_btn: true,
      };
    case BCA_UPDATE_SUCCESS:
      return {
        ...state,
        update_btn: false,
        update_modal: false,
        aboutbca: action.payload,
      };
    case BCA_UPDATE_FAIL:
      return {
        ...state,
        update_btn: false,
      };
    case BCA_STATE:
      const data_active = state.all_aboutbca.map((item) => {
        if (item.id !== action.id) {
          return item;
        } else {
          return {
            ...item,
            ...action.payload,
          };
        }
      });

      return {
        ...state,
        all_aboutbca: data_active,
      };
    default:
      return {
        ...state,
      };
  }
}

export default reducer;
