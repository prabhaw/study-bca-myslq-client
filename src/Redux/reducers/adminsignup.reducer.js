import { ADMIN_CHECK, ADMIN_CREATE, ADMIN_USER_FAIL } from "./../actions/types";

const initialState = {
  admin_account: false,
  admin_btn: false,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case ADMIN_CHECK:
      return {
        ...state,
        admin_account: action.payload,
      };
    case ADMIN_CREATE:
      return {
        ...state,
        admin_btn: true,
      };

    case ADMIN_USER_FAIL:
      return {
        ...state,
        admin_btn: false,
      };
    default:
      return {
        ...state,
      };
  }
}

export default reducer;
