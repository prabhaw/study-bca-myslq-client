import {
  LOGIN_START,
  LOGIN_END,
  LOGIN_FAIL,
  LOGIN_MODAL,
  LOGOUT,
  SIGING_UP,
  SIGN_UP,
  SIGNUP_FAIL,
  CREATE_USER,
  USER_LOGIN,
} from "../actions/types";

const initialState = {
  loggingIn: false,
  loggedIn: false,
  logInmodal: false,
  signup_btn: false,
  sign_in: true,
  user: {},
};

function loginreducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_MODAL:
      return {
        ...state,
        sign_in: true,
        logInmodal: !state.logInmodal,
      };
    case CREATE_USER:
      return {
        ...state,
        sign_in: !state.sign_in,
      };
    case LOGIN_START:
      return {
        ...state,
        loggingIn: true,
      };
    case LOGIN_END:
      return {
        ...state,
        loggingIn: false,
        loggedIn: true,
        user: action.payload,
      };
    case LOGIN_FAIL:
      return {
        ...state,
        loggingIn: false,
      };
    case LOGOUT:
      return {
        ...state,
        user: {},
        loggedIn: false,
      };
    case SIGN_UP:
      return {
        ...state,
        signup_btn: false,
        sign_in: true,
      };
    case SIGING_UP:
      return {
        ...state,
        signup_btn: true,
      };
    case SIGNUP_FAIL:
      return {
        ...state,
        signup_btn: false,
      };
    case USER_LOGIN:
      return {
        ...state,
        loggedIn: true,
        user: action.payload,
      };
    default: {
      return { ...state };
    }
  }
}

export default loginreducer;
