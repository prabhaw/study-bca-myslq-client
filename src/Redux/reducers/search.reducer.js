import {
  SEARCH_FETCH,
  SEARCH_SEMESTER,
  SEARCH_SUBJECT,
} from "./../actions/types";

const initialState = {
  search: [],
  semester: [],
  subject: [],
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case SEARCH_FETCH:
      let university = [];
      action.payload.forEach((item) => {
        if (university.indexOf(item.university) === -1) {
          university.push(item.university);
        }
      });

      return {
        ...state,
        search: university,
      };

    case SEARCH_SEMESTER:
      let semesters = [];
      action.payload.forEach((item) => {
        if (semesters.indexOf(item.semester) === -1) {
          semesters.push(item.semester);
        }
      });
      return {
        ...state,
        semester: semesters,
      };
    case SEARCH_SUBJECT:
      let subj = [];
      action.payload.forEach((item) => {
        if (subj.indexOf(item.subject) === -1) {
          subj.push(item.subject);
        }
      });
      return {
        ...state,
        subject: subj,
      };
    default:
      return {
        ...state,
      };
  }
}

export default reducer;
