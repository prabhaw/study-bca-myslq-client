import {
  ADD_COLLEGE_MODAL,
  ADD_COLLEGE,
  ADD_COLLEGE_SUCC,
  ADD_COLLEGE_FAIL,
  ADMIN_COLLEGE,
  ADMIN_COLLEGE_FETCH,
  DELETE_COLLEGE,
  PUBLIC_FETCH_COLLEGE,
  PUBLIC_FETCH_COLLEGE_END,
  SINGLE_COLLEGE,
  SINGLE_COLLEGE_END,
} from "./../actions/types";

const initialState = {
  add_modal: false,
  add_btn: false,
  resetform: false,
  admin_colleges: [],
  loading: false,
  admin_total: 0,
  admin_pageNumber: 1,
  public_loading: false,
  public_college: [],
  public_total: 0,
  public_pageNumber: 1,
  single_loading: false,
  college: {},
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case SINGLE_COLLEGE:
      return {
        ...state,
        single_loading: true,
      };
    case SINGLE_COLLEGE_END:
      return {
        ...state,
        single_loading: false,
        college: action.payload,
      };
    case PUBLIC_FETCH_COLLEGE:
      return {
        ...state,
        public_loading: true,
      };
    case PUBLIC_FETCH_COLLEGE_END:
      return {
        ...state,
        public_college: action.payload,
        public_total: action.total,
        public_pageNumber: action.pageNumber,
        public_loading: false,
      };
    case ADMIN_COLLEGE:
      return {
        ...state,
        loading: true,
      };
    case ADMIN_COLLEGE_FETCH:
      return {
        ...state,
        admin_colleges: action.payload,
        admin_total: action.total,
        admin_pageNumber: action.pageNumber,
        loading: false,
      };

    case DELETE_COLLEGE:
      const itemDeleted = state.admin_colleges.filter(
        (item) => item.id !== action.index
      );
      return {
        ...state,
        admin_colleges: itemDeleted,
        admin_total: state.admin_total - 1,
      };
    case ADD_COLLEGE_MODAL:
      return {
        ...state,
        add_modal: !state.add_modal,
      };
    case ADD_COLLEGE_SUCC:
      return {
        ...state,
        add_modal: false,
        add_btn: false,
        resetform: true,
      };
    case ADD_COLLEGE:
      return {
        ...state,
        add_btn: true,
        resetform: false,
      };
    case ADD_COLLEGE_FAIL:
      return {
        ...state,
        add_btn: false,
      };
    default:
      return {
        ...state,
      };
  }
}

export default reducer;
