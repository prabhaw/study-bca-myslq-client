import {
  FETCHED_BLOG,
  FETCH_BLOG,
  ADD_BLOG,
  BLOG_ADDED,
  BLOG_ADD_FAIL,
  BLOG_MODAL,
  UPDATE_BLOG,
  UPDATED_BLOG,
  UPDATED_BLOG_FAIL,
  DELETE_BLOG,
  UPDATE_BLOG_ACTIVE,
} from "../actions/types";

const initialState = {
  blog_modal: false,
  blogs: [],
  add_blog_btn: false,
  loading: false,
  formreset: false,
  blog_total: 0,
  pageNum: 1,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case UPDATE_BLOG:
      return {
        ...state,
        add_blog_btn: true,
        formreset: false,
      };
    case UPDATED_BLOG:
      const data = state.blogs.map((item) => {
        if (item.id !== action.id) {
          return item;
        } else {
          return {
            ...item,
            ...action.payload,
          };
        }
      });
      return {
        ...state,

        add_blog_btn: false,
        blogs: data,
        formreset: true,
      };
    case UPDATE_BLOG_ACTIVE:
      const data_active = state.blogs.map((item) => {
        if (item.id !== action.id) {
          return item;
        } else {
          return {
            ...item,
            ...action.payload,
          };
        }
      });
      return {
        ...state,
        blogs: data_active,
      };
    case UPDATED_BLOG_FAIL:
      return {
        ...state,
        add_blog_btn: false,
      };
    case BLOG_MODAL:
      return {
        ...state,
        blog_modal: !state.blog_modal,
      };
    case ADD_BLOG:
      return {
        ...state,
        add_blog_btn: true,
        formreset: false,
      };
    case BLOG_ADDED:
      return {
        ...state,
        add_blog_btn: false,
        blog_modal: false,
        formreset: true,
        blog_total: state.blog_total + 1,
        blogs: [action.payload, ...state.blogs],
      };
    case BLOG_ADD_FAIL:
      return {
        ...state,
        add_blog_btn: false,
      };
    case FETCH_BLOG:
      return {
        ...state,
        loading: true,
      };
    case FETCHED_BLOG:
      return {
        ...state,
        loading: false,
        blogs: action.payload,
        blog_total: action.total,
        pageNum: action.pageNumber,
      };

    case DELETE_BLOG:
      const itemDeleted = state.blogs.filter(
        (item) => item.id !== action.index
      );
      return {
        ...state,
        blogs: itemDeleted,
        blog_total: state.blog_total - 1,
      };
    default:
      return {
        ...state,
      };
  }
}

export default reducer;
