import {
  SENDING_EMAIL,
  EMAIL_SEND,
  EMAIL_SEND_FAIL,
  EMAIL_VERIFYING,
  EMAIL_VERIFYED,
} from "./../actions/types";

const initialState = {
  sendign_btn: false,
  verify_code: false,
  verify_btn: false,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case EMAIL_VERIFYING:
      return {
        ...state,
        verify_btn: true,
      };
    case EMAIL_VERIFYED:
      return {
        ...state,
        verify_btn: false,
      };
    case SENDING_EMAIL:
      return {
        ...state,
        sendign_btn: true,
      };
    case EMAIL_SEND:
      return {
        ...state,
        sendign_btn: false,
        verify_code: true,
      };

    case EMAIL_SEND_FAIL:
      return {
        ...state,
        sendign_btn: false,
      };

    default:
      return {
        ...state,
      };
  }
}

export default reducer;
