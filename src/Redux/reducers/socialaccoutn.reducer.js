import {
  SOCIAL_REGISTER_MODAL,
  SOCIAL_PASS_BTN,
  SOCIAL_PASS_END,
} from "./../actions/types";

const initialState = {
  password_btn: false,
  password_modal: false,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case SOCIAL_REGISTER_MODAL:
      return {
        ...state,
        password_modal: !state.password_modal,
      };
    case SOCIAL_PASS_BTN:
      return {
        ...state,
        password_btn: true,
      };
    case SOCIAL_PASS_END:
      return {
        ...state,
        password_btn: false,
      };
    default:
      return {
        ...state,
      };
  }
}

export default reducer;
