import {
  PUBLIC_BLOG_FETCHING,
  PUBLIC_BLOG_FETCH,
  PUBLIC_BLOG,
  PUBLIC_BLOG_FETCHED,
} from "./../actions/types";

const initialState = {
  blogs: [],
  loading: false,
  blog_total: 0,
  pageNum: 1,
  blog: {},
  blog_loading: false,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case PUBLIC_BLOG:
      return {
        ...state,
        blog_loading: true,
      };
    case PUBLIC_BLOG_FETCHED:
      return {
        ...state,
        blog_loading: false,
        blog: action.payload,
      };
    case PUBLIC_BLOG_FETCHING:
      return {
        ...state,
        loading: true,
      };
    case PUBLIC_BLOG_FETCH:
      return {
        ...state,
        loading: false,
        blogs: action.payload,
        blog_total: action.total,
        pageNum: action.pageNumber,
      };

    default:
      return {
        ...state,
      };
  }
}

export default reducer;
