import {
  RESET_PASS,
  RESET_PASS_END,
  RESET_ENTER_PASSWORD,
  RESET_ENTER_PASSWORD_END,
} from "./../actions/types";

const initialStart = {
  sendemail_btn: false,
  reset_btn: false,
};

function resetpassword(state = initialStart, action) {
  switch (action.type) {
    case RESET_ENTER_PASSWORD:
      return {
        ...state,
        reset_btn: true,
      };
    case RESET_ENTER_PASSWORD_END:
      return {
        ...state,
        reset_btn: false,
      };
    case RESET_PASS:
      return {
        ...state,
        sendemail_btn: true,
      };
    case RESET_PASS_END:
      return {
        ...state,
        sendemail_btn: false,
      };

    default:
      return {
        ...state,
      };
  }
}

export default resetpassword;
