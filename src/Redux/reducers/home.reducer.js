import { TOP_COLLEGE_END, TOP_COLLEGE_FETCH } from "./../actions/types";

const initialState = {
  colleges: [],
  loading: false,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case TOP_COLLEGE_FETCH:
      return {
        ...state,
        loading: true,
      };
    case TOP_COLLEGE_END:
      return {
        ...state,
        loading: false,
        colleges: action.payload,
      };
    default:
      return {
        ...state,
      };
  }
}

export default reducer;
