import {
  ADD_SLIDER,
  ADD_SLIDER_FAIL,
  ADD_SLIDER_SUCC,
  FETCH_SLIDER_STR,
  FETCH_SLIDER_END,
  DELETE_SLIDER,
} from "./../actions/types";

const initialState = {
  loading: false,
  add_btn: false,
  sliders: [],
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case ADD_SLIDER:
      return {
        ...state,
        add_btn: true,
      };

    case ADD_SLIDER_FAIL:
      return {
        ...state,
        add_btn: false,
      };

    case ADD_SLIDER_SUCC:
      return {
        ...state,
        add_btn: false,
        sliders: [action.payload, ...state.sliders],
      };

    case FETCH_SLIDER_STR:
      return {
        ...state,
        loading: true,
      };

    case FETCH_SLIDER_END:
      return {
        ...state,
        loading: false,
        sliders: action.payload,
      };

    case DELETE_SLIDER:
      const itemDeleted = state.sliders.filter(
        (item) => item.id !== action.index
      );
      return {
        ...state,
        sliders: itemDeleted,
      };
    default:
      return {
        ...state,
      };
  }
}

export default reducer;
