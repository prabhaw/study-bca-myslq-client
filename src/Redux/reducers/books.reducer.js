import {
  BOOK_MODAL,
  ADD_BOOK,
  ADD_BOOK_SUCC,
  ADD_BOOK_FAIL,
  FETCH_BOOK_STR,
  FETCH_BOOK_SUCC,
  BOOK_DELETED,
} from "./../actions/types";

const initialState = {
  pageNumber: 1,
  loading: false,
  add_btn: false,
  formreset: false,
  book_modal: false,
  total: 0,
  book: [],
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case BOOK_MODAL:
      return {
        ...state,
        book_modal: !state.book_modal,
      };
    case ADD_BOOK:
      return {
        ...state,
        add_btn: true,
        formreset: false,
      };
    case ADD_BOOK_FAIL:
      return {
        ...state,
        add_btn: false,
      };
    case ADD_BOOK_SUCC:
      return {
        ...state,
        add_btn: false,
        formreset: true,
        book_modal: false,
        total: state.total + 1,
        book: [action.payload, ...state.book],
      };

    case FETCH_BOOK_STR:
      return {
        ...state,
        loading: true,
      };
    case FETCH_BOOK_SUCC:
      return {
        ...state,
        loading: false,
        book: action.payload,
        pageNumber: action.pageNumber,
        total: action.total,
      };
    case BOOK_DELETED:
      const itemDeleted = state.book.filter((item) => item.id !== action.index);
      return {
        ...state,
        total: state.total - 1,
        book: itemDeleted,
      };
    default:
      return {
        ...state,
      };
  }
}

export default reducer;
