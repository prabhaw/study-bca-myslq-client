import {
  NOTE_MODAL,
  ADD_NOTE,
  ADD_NOTE_SUCC,
  ADD_NOTE_FAIL,
  FETCH_NOTE_STR,
  FETCH_NOTE_END,
  UPDATE_NOTE,
  UPDATE_NOTE_FAIL,
  UPDATED_NOTE,
  DELETE_NOTE,
  SINGLE_FETCH_STR,
  SINGLE_FETCH_END,
} from "./../actions/types";

const initialState = {
  note_modal: false,
  notes: [],
  note_btn: false,
  loading: false,
  formreset: false,
  note_total: 0,
  pageNumber: 1,
  single_load: false,
  note: {},
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case NOTE_MODAL:
      return {
        ...state,
        note_modal: !state.note_modal,
      };

    case ADD_NOTE:
      return {
        ...state,
        note_btn: true,
        formreset: false,
      };
    case ADD_NOTE_FAIL:
      return {
        ...state,
        note_btn: false,
      };

    case ADD_NOTE_SUCC:
      return {
        ...state,
        note_btn: false,
        formreset: true,
        note_modal: false,
        note_total: state.note_total + 1,
        notes: [action.payload, ...state.notes],
      };
    case FETCH_NOTE_STR:
      return {
        ...state,
        loading: true,
      };
    case FETCH_NOTE_END:
      return {
        ...state,
        loading: false,
        notes: action.payload,
        note_total: action.total,
        pageNumber: action.pageNumber,
      };

    case UPDATE_NOTE:
      return {
        ...state,
        note_btn: true,
        formreset: false,
      };
    case UPDATE_NOTE_FAIL:
      return {
        ...state,
        note_btn: false,
      };
    case UPDATED_NOTE:
      const data = state.notes.map((item) => {
        if (item.id !== action.id) {
          return item;
        } else {
          return {
            ...item,
            ...action.payload,
          };
        }
      });
      return {
        ...state,
        note_btn: false,
        notes: data,
        formreset: true,
      };
    case DELETE_NOTE:
      const itemDeleted = state.notes.filter(
        (item) => item.id !== action.index
      );
      return {
        ...state,
        notes: itemDeleted,
        note_total: state.note_total - 1,
      };

    case SINGLE_FETCH_STR:
      return {
        ...state,
        single_load: true,
      };
    case SINGLE_FETCH_END:
      return {
        ...state,
        single_load: false,
        note: action.payload,
      };
    default:
      return {
        ...state,
      };
  }
}

export default reducer;
